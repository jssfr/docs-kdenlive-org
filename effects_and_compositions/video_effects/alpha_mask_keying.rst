.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)
             - TheMickyRosen-Left (https://userbase.kde.org/User:TheMickyRosen-Left)
             - Carl Schwan <carl@carlschwan.eu>
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-alpha_mask_keying:

======================
Alpha, Mask and Keying
======================

The *Alpha, Mask and Keying* video effects category provides effects and filters to work with the alpha channel of source files or create masks and cut-outs if needed. It forms the prerequisite for alpha compositing [1]_ or alpha blending or what is more commonly referred to as Chroma Keying. At the end, the results are used to combine one image or video with a background to create the appearance of varying degrees of transparency.

The following filters are available:

.. toctree::
   :maxdepth: 1
   :glob:

   alpha_mask_keying/*


**Notes**

.. |wiki-alpha_compositing| raw:: html

   <a href="https://en.wikipedia.org/wiki/Alpha_compositing" target="_blank">alpha compositing</a>

.. |porterduff| raw:: html

   <a href="https://keithp.com/~keithp/porterduff/p253-porter.pdf" target="_blank">p253-porter.pdf</a>

.. [1] For some background, the Wikipedia article about |wiki-alpha_compositing| is useful. See also Porter, Thomas; Tom Duff (1984). "Compositing Digital Images". Computer Graphics 18 (3): p253–259 1984 |porterduff|
