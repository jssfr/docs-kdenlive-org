.. meta::

   :description: Do your first steps with Kdenlive video editor, generate effect category
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, generate

.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-generate:

========
Generate
========

The *Generate* video effects category provides effects and filter for generating specific overlays, like :ref:`effects-dynamic_text`, :ref:`scanlines <effects-scanline0r>`, or generating a :ref:`effects-video_grid`.

The following effects and filters are available:

.. toctree::
   :maxdepth: 1
   :glob:

   generate/*
   
