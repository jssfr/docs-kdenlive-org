.. meta::

   :description: Do your first steps with Kdenlive video editor, using scratchlines effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, grain and noise, scratchlines

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-scratchlines:

Scratchlines
============

This effect/filter generates scratch lines over the image source. Can be used in conjunction with :ref:`effects-oldfilm` and :ref:`effects-dust` to simulate vintage film footage.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-scratchlines.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-scratchlines

   Scratchlines effect

* **Width of lines** - Sets the width of the scratch lines

* **Max number of lines** - Defines the maximum number of lines

* **Max darker** - Defines how much darker the image will be behind the line

* **Max lighter** - Defines how much lighter the image will be behind the line
