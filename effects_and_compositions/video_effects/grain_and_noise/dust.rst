.. meta::

   :description: Do your first steps with Kdenlive video editor, using dust effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, grain and noise, dust

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-dust:

Dust
====

This effect/filter makes random dust particles appear across the image source. This gives the video an old film look. See also the :ref:`effects-oldfilm` and :ref:`effects-scratchlines` effects.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-dust.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-dust

   Dust effect

* **Maximal Diameter** - Sets the maximum size of (the underlying .png file for) the dust particles

* **Maximal number of dust** - Sets the maximum number of dust particles
