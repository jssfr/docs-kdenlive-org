.. meta::

   :description: Do your first steps with Kdenlive video editor, using gradfun effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, grain and noise, gradfun

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-gradfun:

Gradfun
=======

This effect/filter de-bands video quickly by using gradients.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-gradfun.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-gradfun

   Gradfun effect

* **av.strength** - The maximum amount by which the filter will change any one pixel

* **av.radius** - The neighborhood to fit the gradient to
