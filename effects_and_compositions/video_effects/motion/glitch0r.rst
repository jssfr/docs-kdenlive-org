.. meta::

   :description: Do your first steps with Kdenlive video editor, using glitch0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, misc, miscellaneous, glitch0r

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-glitch0r:

Glitch0r
========

This effect/filter adds glitches and block shifting.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-glitch0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-glitch0r

   Glitch0r effect

* **Glitch frequency** - How frequently the glitch should be applied

* **Block height** - Height range of the block that will be shifted/glitched

* **Shift intensity** - How much blocks should be moved when glitching

* **Color glitching intensity** - How intensive color distortion should be

.. rst-class:: clear-both


.. hint:: The smaller the :guilabel:`Block height`, the more visible the effect even with lower values for :guilabel:`Glitch frequency`.
