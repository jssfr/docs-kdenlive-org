.. meta::

   :description: Do your first steps with Kdenlive video editor, using nervous effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, motion, nervous

.. metadata-placeholder

   :authors: - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-nervous:

Nervous
=======

This effect/filter flushes frames from internal cache into a random, "nervous" order. No frame is discarded.

The effect works like a switch, does not have keyframes and no parameters to set.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-nervous.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-nervous

   Nervous effect

..
