.. meta::

   :description: Do your first steps with Kdenlive video editor, grain and noise effect category
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, grain and noise, grain, noise

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-grain_and_noise:

===============
Grain and Noise
===============

The *Grain and Noise* video effects category provides effects and filter for adding or removing noise. A special effect is :ref:`effects-scratchlines` which is most often used with :ref:`effects-oldfilm` and :ref:`effects-dust`.

The following effects and filters are available:

.. toctree::
   :maxdepth: 1
   :glob:

   grain_and_noise/*
