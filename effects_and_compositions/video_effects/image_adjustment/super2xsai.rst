.. meta::

   :description: Do your first steps with Kdenlive video editor, using super2xsai effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, image adjustment, super2xsai

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |2xsai| raw:: html

   <a href="https://en.wikipedia.org/wiki/Pixel-art_scaling_algorithms#2%C3%97SaI" target="_blank">2xSaI</a>


.. _effects-super2xsai:

Super2xSaI
==========

This effect/filter scales the input by 2x and smooths it using the Super2xSaI (Scale and Interpolate) pixel art scaling algorithm\ [1]_.

Useful for enlarging pixel art images without reducing sharpness.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-super2xsai.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-super2xsai

   Super2xSaI effect

..

.. rst-class:: clear-both

**Notes**

.. [1] For more details refer to this article about |2xsai| on Wikipedia.
