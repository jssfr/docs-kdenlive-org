.. meta::

   :description: Do your first steps with Kdenlive video editor, using epx scaler effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, image adjustment, epx scaler

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |epx| raw:: html

   <a href="https://en.wikipedia.org/wiki/Pixel-art_scaling_algorithms#EPX/Scale2%C3%97/AdvMAME2%C3%97" target="_blank">Eric's Pixel Expansion (EPX)</a>


.. _effects-epx_scaler:

EPX Scaler
==========

This effect/filter applies the EPX\ [1]_ magnification filter which is designed for pixel art.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-epx_scaler.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-epx_scaler

   EPX Scaler effect

* **Scale factor** - Sets the scaling dimension: **2x** or **3x**. Default is **3x**.

.. rst-class:: clear-both


**Notes**

.. [1] See the article in Wikipedia about |epx| algorithm.
