.. meta::

   :description: Do your first steps with Kdenlive video editor, transform distort perspective video effects category
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, category, transform, distort, perspective

.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Smolyaninov (https://userbase.kde.org/User:Smolyaninov)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-transform_distort_perspective:

==================================
Transform, Distort and Perspective
==================================

The *Transform, Distort and Perspective* video effects category provides effects and filters for manipulating the shape, size and position of the source clip(s), such as :ref:`effects-transform` and :ref:`effects-crop_scale_and_tilt` effects.

The available effects and filters are:

.. toctree::
   :maxdepth: 1
   :glob:
   
   transform_distort_perspective/*
