.. meta::

   :description: Do your first steps with Kdenlive video editor, using planes blur effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, planes blur


   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-planes_blur:

Planes Blur
===========

This effect applies just to three :term:`Planes<plane>`: :term:`Luma`, :term:`Chroma` and Alpha. You define the :guilabel:`Radius` and :guilabel:`Power` to determine the strength of the effect.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-planes_blur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-planes_blur

   Planes Blur effect

..
