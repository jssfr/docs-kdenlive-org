.. meta::

   :description: Do your first steps with Kdenlive video editor, using gaussian blur effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, gaussian blur

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _effects-gaussian_blur:

Gaussian Blur
=============

This effect blurs the clip by applying a Gaussian function (known from statistics as an expression for the normal distribution). By default, all :term:`planes<plane>` will be affected but by setting the blur effect to different planes (e.g. red, green or blue) interesting artistic effects can be achieved. :guilabel:`Sigma` determines the strength of the blur.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-gaussian_blur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-gaussian_blur

   Gaussian Blur effect

..
