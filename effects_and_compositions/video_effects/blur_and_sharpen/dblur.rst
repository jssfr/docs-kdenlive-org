.. meta::

   :description: Do your first steps with Kdenlive video editor, using directional blur (dblur) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, dblur, directional blur

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _effects-dblur:

Directional Blur (Dblur)
========================

This effect applies a directional blur to the clip.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-dblur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-dblur

   Directional Blur effect

..
