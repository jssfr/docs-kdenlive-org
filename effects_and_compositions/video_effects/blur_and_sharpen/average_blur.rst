.. meta::

   :description: Do your first steps with Kdenlive video editor, using average blur effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, average blur

   :authors: - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-average_blur:

Average Blur
============

Blurs the clip based on the settings for :guilabel:`X size` and :guilabel:`Y size`. By default, all :term:`planes<plane>` will be affected but by setting the blur effect to different planes (e.g. red, green or blue) interesting artistic effects can be achieved.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-average_blur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-average_blur

   Average Blur effect

..
