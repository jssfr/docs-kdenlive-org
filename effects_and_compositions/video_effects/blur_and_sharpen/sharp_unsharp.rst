.. meta::

   :description: Do your first steps with Kdenlive video editor, using sharp/unsharp effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, sharp, unsharp

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-sharp_unsharp:

Sharp/Unsharp
=============

This effect is used to blur or sharpen a clip.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sharp_unsharp.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-sharp_unsharp

   Sharp/Unsharp effect

* **Luma/Chroma horizontal matrix** - Odd integer between 3 and 23, default is 5

* **Luma/Chroma vertical matrix** - Odd integer between 3 and 23, default is 5

* **Luma/Chroma strength** - Floating point number between -2.00 (blurry) and 5.00 (sharp). Reasonable values are between -1.5 and 1.5
