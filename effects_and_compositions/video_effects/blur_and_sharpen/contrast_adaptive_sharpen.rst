.. meta::

   :description: Do your first steps with Kdenlive video editor, using contrast adaptive sharpen (CAS) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, contrast adaptive sharpen, CAS

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-contrast_adaptive_sharpen:

Contrast Adaptive Sharpen (CAS)
===============================

Helps increase visual quality by providing natural sharpness without artifacts.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-CAS.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-CAS

   Contrast Adaptive Sharpening (CAS) effect

* **Planes** - Set the :term:`plane` the effect is to be applied to. Options are: **Alpha**, **Y** (default), **U**, **V**, **Red**, **Green**, **Blue** and **All**.

* **Strength** - Determine the strength of the effect. Allowed values are from 0.000 to 1.000, default is 0.000.
