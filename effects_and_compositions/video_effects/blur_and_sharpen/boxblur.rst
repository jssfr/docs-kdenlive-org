.. meta::

   :description: Do your first steps with Kdenlive video editor, using boxblur effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, boxblur

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _boxblur:

Boxblur
========

This effect blurs the image based on the horizontal and vertical multiplicator allowing uneven blurring or blurring in only one direction. Not be confused with the deprecated Box Blur effect.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-boxblur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-boxblur

   Boxblur effect

..
