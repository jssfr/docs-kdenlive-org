.. meta::

   :description: Do your first steps with Kdenlive video editor, using square blur effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, square blur


   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _effects-square_blur:

Square Blur
===========

This effect blurs the entire clip. **Kernel size** determines the blurriness.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-square_blur.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-square_blur

   Squareblur effect
