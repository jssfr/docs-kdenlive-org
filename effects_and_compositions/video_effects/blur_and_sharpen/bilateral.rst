.. meta::

   :description: Do your first steps with Kdenlive video editor, using bilateral effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, blur and sharpen, bilateral

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |here| raw:: html

   <a href="https://en.wikipedia.org/wiki/Bilateral_filter" target="_blank">here</a>


.. _effects_bilateral:

Bilateral
=========

This effect applies a kind of blur filter with spatial smoothing while preserving edges. A more in-depth explanation including the mathematical definition is available |here| on Wikipedia.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-bilateral.webp
   :align: left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-bilateral

   Bilateral blur effect
