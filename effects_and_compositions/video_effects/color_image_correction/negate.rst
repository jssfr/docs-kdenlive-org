.. meta::

   :description: Do your first steps with Kdenlive video editor, using negate effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, negate

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-effect_negate:

Negate
======

This effect/filter negates (inverts) the input video or its alpha channel.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-negate.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-negate

   Negate effect

* **Negate alpha** - If checked, it negates/inverts the alpha component (if present). Default is off.
