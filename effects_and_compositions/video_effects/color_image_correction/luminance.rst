.. meta::

   :description: Do your first steps with Kdenlive video editor, using luminance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, luminance

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-luminance:

Luminance
=========

This effect/filter creates a grayscale :term:`luminance` map of the source image.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-luminance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-luminance

   Luminance effect

..
