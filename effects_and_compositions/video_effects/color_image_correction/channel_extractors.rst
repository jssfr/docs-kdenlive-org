.. meta::

   :description: Do your first steps with Kdenlive video editor, using the channel extractor effects
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, channel extractor red, channel extractor green, channel extractor blue

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0



.. _effects-channel_extractors:

Channel Extractor BLUE / GREEN / RED
====================================

These effects extract the respective color channels from the clip. There are no further parameters or keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-channel_extractor.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-channel_extractor

   Original clip before

..

.. container:: clear-both

   .. figure:: /images/effects_and_compositions/kdenlive2304_effects-channel_extractor_RED.webp
      :width: 400px
      :figwidth: 400px
      :align: left
      :alt: kdenlive2304_effects-channel_extractor_RED

      Result of Channel Extractor RED



   .. figure:: /images/effects_and_compositions/kdenlive2304_effects-channel_extractor_GREEN.webp
      :width: 400px
      :figwidth: 400px
      :align: left
      :alt: kdenlive2304_effects-channel_extractor_GREEN

      Result of Channel Extractor GREEN



   .. figure:: /images/effects_and_compositions/kdenlive2304_effects-channel_extractor_BLUE.webp
      :width: 400px
      :figwidth: 400px
      :align: left
      :alt: kdenlive2304_effects-channel_extractor_BLUE

      Result of Channel Extractor BLUE

..
