.. meta::

   :description: Do your first steps with Kdenlive video editor, using exposure effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, exposure

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |wikipedia_exposure| raw:: html

   <a href="https://en.wikipedia.org/wiki/Exposure_value" target="_blank">Exposure Value</a>


.. _effects-exposure:

Exposure
========

This effect/filter adjusts the exposure of the image or video stream.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-exposure.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-exposure

   Exposure effect

* **Exposure** - Set the exposure correction in EV\ [1]_. Allowed values are from -3.0 to 3.0 EV, default is 0 EV.

* **Radius** - Set the black level correction. Allowed values are from -1.0 to 1.0, default is 0.

.. rst-class:: clear-both

**Notes**

.. [1] Exposure Value. A number that represents a combination of a camera's shutter speed and f-number. For more details refer to the |wikipedia_exposure| article on Wikipedia.
