.. meta::

   :description: Do your first steps with Kdenlive video editor, using gamma effect with keyframes
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, gamma with keyframes

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-gamma_keyframe:

Gamma (keyframable)
===================

This effect/filter adjusts the :term:`gamma` value of a source image.

The effect has keyframes. Use :ref:`effects-gamma` if you do not need keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-gamma_keyframe.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-gamma_keyframe

   Gamma (keyframable) effect

..
