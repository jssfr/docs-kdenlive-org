.. meta::

   :description: Do your first steps with Kdenlive video editor, using lumaliftgammagain effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, lumaliftgammagain, luma, lift, gamma, gain

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |color_grading_terminology| raw:: html

   <a href="http://www.digital-intermediate.co.uk/colour/colourterminology.htm" target="_blank">color grading terminology</a>


.. _effects-lumaliftgammagain:

LumaLiftGammaGain
=================

This effect/filter can be used to apply lift, gain and :term:`gamma` correction to :term:`luma` values of the source image.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-lumaliftgammagain.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-lumaliftgammagain

   LumaLiftGammaGain effect

* **Lift** - Adjusts the darker areas

* **Gain** - Adjusts the brighter areas

* **Gamma** - Adjusts the midtone areas

.. rst-class:: clear-both


**Notes**

There is good section of |color_grading_terminology| available at the digital-intermediate.co.uk site.
