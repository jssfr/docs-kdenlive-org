.. meta::

   :description: Do your first steps with Kdenlive video editor, using the color balance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, color balance

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-color_balance:

Color Balance
=============

This effect modifies the intensity of primary colors (red, green and blue). It allows adjustment in the shadow, mid-tones or highlights regions for the red-cyan, green-magenta or blue-yellow balance.

A positive value shifts the balance towards the primary color, a negative value towards the complementary color.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_balance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_balance

   Color Balance effect

* **Red / Green / Blue Shadow** - Adjust the red, green and blue shadows (darkest pixels)

* **Red / Green / Blue Midtones** - Adjust the red, green and blue mid-tones (medium pixels)

* **Red / Green / Blue Highlights** - Adjust the red, green and blue highlights (brightest pixels)

* **Preserve lightness** - Preserve lightness when changing color balance. Default is disabled.
