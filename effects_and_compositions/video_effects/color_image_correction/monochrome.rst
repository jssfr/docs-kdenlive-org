.. meta::

   :description: Do your first steps with Kdenlive video editor, using monochrome effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, monochrome

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-monochrome:

Monochrome
==========

This effect/filter converts video to gray using custom color filter.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-monochrome.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-monochrome

   Monochrome effect

* **Chroma blue spot** - Set the :term:`chroma` blue spot. Allowed range is from -1 to 1. Default value is 0.

* **Chroma red spot** - Set the :term:`chroma` red spot. Allowed range is from -1 to 1. Default value is 0.

* **Color filter size** - Set the color filter size. Allowed range is from .1 to 10. Default value is 1.

* **Highlights strength** - Set the highlights strength. Allowed range is from 0 to 1. Default value is 0.
