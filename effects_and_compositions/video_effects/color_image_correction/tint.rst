.. meta::

   :description: Do your first steps with Kdenlive video editor, using tint effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, tint

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-tint:

Tint
====

This effect/filter maps the source image :term:`luminance` between two specified colors.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-tint.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-tint

   Tint effect

* **Map black to** - The color to map source color with zero luminance

* **Map white to** - The color to map source color with full luminance

* **Tint amount** - Amount of color. Allowed values are from 0 to 1000, default is 250.
