.. meta::

   :description: Do your first steps with Kdenlive video editor, using color temperature effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, color temperature

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-color_temperature:

Color Temperature
=================

This effect/filter adjusts the color :term:`temperature` in video to simulate an ambient color temperature.

This effect has keyframes

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_temperature.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_temperature

   Color Temperature effect

* **Temperature** - Set the temperature in :term:`Kelvin<kelvin>`. Allowed values are from 1,000 to 40,000, default is 6,500K.

* **Mix** - Set the mix with filtered output. Allowed values are from 0.0 to 1.0, default value is 1.0.

* **Saturation** - Set the color :term:`saturation`. Allowed values are from 0 to 1, default value is 0.5.

* **Preserve lightness** - Set the amount of preserving :term:`lightness`. Allowed values are from 0 to 1, default value is 0.

.. rst-class:: clear-both

Color temperature is measured in degrees Kelvin. Lower values correct for "warmer" lighting, higher values correct for "cool" lighting. the default value of +6,500K is unity.
