.. meta::

   :description: Do your first steps with Kdenlive video editor, using color_levels effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, color_levels

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-color_levels:

Color Levels
============

This effect/filter adjusts video input frames using levels.

*Input levels* are used to lighten highlights (bright tones), darken shadows (dark tones), change the balance of bright and dark tones. *Output levels* allows manual selection of a constrained output level range.

This effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_levels.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_levels

   Color Levels effect

* **Red / Green / Blue black input** - Adjust red, green and blue input black point. Allowed values are 0 to 1.0, default is 0.

* **Red / Green / Blue white input** - Adjust red, green and blue input white point. Allowed values are 0 to 1.0, default is 1.

* **Red / Green / Blue black output** - Adjust red, green and blue output black point. Allowed values are 0 to 1.0, default is 0.

* **Red / Green / Blue white output** - Adjust red, green and blue output white point. Allowed values are 0 to 1.0, default is 1.
