.. meta::

   :description: Do your first steps with Kdenlive video editor, using invert or invert0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, invert, invert0r

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-invert:
.. _effects-invert0r:

Invert / Invert0r
=================

The invert and invert0r effects/filters invert all the colors of source image. They do exactly the same.

The effects work like switches and do not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-invert0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-invert0r

   Invert and Invert0r effect

..
