.. meta::

   :description: Do your first steps with Kdenlive video editor, using vibrance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, vibrance

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |vib_sat| raw:: html

   <a href="https://www.wix.com/blog/photography/vibrance-vs-saturation" target="_blank">Vibrance vs. Saturation</a>


.. _effects-vibrance:

Vibrance
========

This effect/filter boosts or alters :term:`saturation`. The results from using the vibrance effect are different compared to the :ref:`effects-saturation` effect\ [1]_.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-vibrance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-vibrance

   Vibrance effect

* **Intensity** - Set the strength of the boost with positive values, strength of :guilabel:`Alternate` with negative values. Allowed range is from -2.0 to 2.0, default is 0.

* **Red /Green / Blue balance** - Set the red, green and blue balance. Allowed range is from -10.0 to 10.0, default is 1.0.

* **Red / Green / Blue luma** - Set the red, green and blue :term:`luma` coefficient.

* **Alternate** - If :guilabel:`Intensity` is negative and this is checked, colors will change, otherwise colors will be less saturated more towards grey.

.. rst-class:: clear-both


**Notes**

.. [1] For more details about vibrance and saturation see the article about |vib_sat|.
