.. meta::

   :description: Do your first steps with Kdenlive video editor, using the color contrast effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, color contrast

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-color_contrast:

Color Contrast
==============

This effect/filter adjusts the color contrast between RGB components.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_contrast.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_contrast

   Color Contrast effect

* **Red-Cyan Contrast** - Set the red-cyan contrast. Default is 0.0. Allowed range is from -1.0 to 1.0.

* **Green-Magenta Contrast** - Set the green-magenta contrast. Default is 0.0. Allowed range is from -1.0 to 1.0.

* **Blue-Yellow Contrast** - Set the blue-yellow contrast. Default is 0.0. Allowed range is from -1.0 to 1.0.

* **R-C / G-M / B-Y Weight** - Set the weight for each red-cyan, green-magenta and blue-yellow option value. Default value is 0.0. Allowed range is from 0.0 to 1.0. If all weights are 0.0 filtering is disabled.

* **Preserving lightness** - Set the amount of preserving lightness. Default value is 0.0. Allowed range is from 0.0 to 1.0.
