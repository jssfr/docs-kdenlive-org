.. meta::

   :description: Do your first steps with Kdenlive video editor, using the bw0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, bw0r, black and white

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. |bw0r| raw:: html

   <a href="https://www.mltframework.org/plugins/FilterFrei0r-bw0r/" target="_blank">frei0r.bw0r</a>


.. _effects-bw0r:

Bw0r
====

This effect simply turns the clip to black & white. There are no further parameters or keyframes.

This is the |bw0r| MLT filter.
