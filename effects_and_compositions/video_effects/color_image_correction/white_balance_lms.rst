.. meta::

   :description: Do your first steps with Kdenlive video editor, using white balance (LMS) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, white balance (LMS)

   :authors: - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-white_balance_lms:

White Balance (LMS)
===================

This effect/filter does simple color correction, in a physically meaningful way. For more detailed about white balance see :ref:`tutorial-white_balance_lms` in the :ref:`useful_information` section of the documentation.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-white_balance_lms.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-white_balance_lms

   White Balance (LMS space) effect

* **Neutral Color** - Choose the color from the source image that should be white

* **Color Temperature** - Choose an output color temperature, if different from 6,500K

.. rst-class:: clear-both


Color temperature is measured in degrees Kelvin. Lower values correct for "warmer" lighting, higher values correct for "cool" lighting. the default value of +6,500K is unity.
