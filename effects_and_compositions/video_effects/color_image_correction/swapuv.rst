.. meta::

   :description: Do your first steps with Kdenlive video editor, using swapuv effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, swapuv

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-swapuv:

Swapuv
======

This effect/filter swaps the U (blue projection) and V (red projection) components.

The effect works like a switch and does not have keyframes.
