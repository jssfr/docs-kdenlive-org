.. meta::

   :description: Do your first steps with Kdenlive video editor, using gamma effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, gamma

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-gamma:

Gamma
=====

This effect/filter adjusts the :term:`gamma` value of a source image.

The effect does not have keyframes. Use :ref:`effects-gamma_keyframe` if you need keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-gamma.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-gamma

   Gamma effect

..
