.. meta::

   :description: Do your first steps with Kdenlive video editor, using the 3-point balance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, 3-point balance, three point balance

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. |pipette| replace:: pipette


.. _effects-3_point_balance:

3 Point Balance
===============

This effect is a simple implementation of the Bezier Curves effect. With the simplified interface you select the shades of grey using the |pipette| color picker for Black Level, Grey Level and White Level, or use the color selection buttons. See also :ref:`effects-bezier_curves`.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-3_point_balance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-3_point_balance

   3-point Balance effect

* **Split screen preview** - Select this to have a split screen in the Project Monitor where you can compare the results of the effect/filter with the original clip.

* **Source image on left side** - If *Split screen preview* is enabled, the original clip is on the left side of the split screen. Uncheck this to have the original on the right-hand side of the split screen.

* **Black / Gray / White color** - Represent the Black, Grey and White levels for the clip.
