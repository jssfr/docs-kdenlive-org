.. meta::

   :description: Do your first steps with Kdenlive video editor, using colorize effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, colorize

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-colorize:

Colorize
========

This effect/filter colorizes an image to the selected :term:`hue`, :term:`saturation` and :term:`lightness`.

This effect has keyframes

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-colorize.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-colorize

   Colorize effect

* **Hue** - Set the color :term:`hue`. Allowed values are from 0 to 360, default value is 0.

* **Saturation** - Set the color :term:`saturation`. Allowed values are from 0 to 1, default value is 0.5.

* **Lightness** - Set the color :term:`lightness`. Allowed values are from 0 to 1, default value is 0.5.

.. note:: This effect works similar to the :ref:`effects-color_overlay` effect but produces slightly different results for crispiness and detail.
