.. meta::

   :description: Do your first steps with Kdenlive video editor, using the brightness effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, brightness

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. |frei0r_brightness| raw:: html

   <a href="https://www.mltframework.org/plugins/FilterFrei0r-brightness/" target="_blank">frei0r.brightness</a>


.. _effects-brightness:

Brightness
==========

This effect adjusts the brightness of the clip. Values range from 0 (completely dark) to 1000 (glaringly bright).

This effect has keyframes.

This is the |frei0r_brightness| MLT filter.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-brightness.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-brightness

   Brightness effect

..

.. rst-class:: clear-both

.. note:: This brightness effect works differently than the :ref:`Brightness (keyframable) <effects-brightness_2>` effect.
