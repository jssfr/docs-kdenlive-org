.. meta::

   :description: Do your first steps with Kdenlive video editor, using technicolor effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, technicolor

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |gentlemen_prefer_blondes| raw:: html

   <a href="https://en.wikipedia.org/wiki/Technicolor#/media/File:Gentlemen_Prefer_Blondes_Movie_Trailer_Screenshot_(34).jpg" target="_blank">Gentlemen Prefer Blondes</a>


.. _effects-technicolor:

Technicolor
===========

This effect/filter over-saturates the color in the video source and gives it the old Technicolor film appearance\ [1]_.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-technicolor.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-technicolor

   Technicolor effect

* **Blue/Yellow axis** - Adjust the factor for the Blue/Yellow axis. Allowed values are from -400 to 400, default is 190.

* **Red/Green axis** - Adjust the factor for the Red/Green axis. Allowed values are from -400 to 400, default is 190.

.. rst-class:: clear-both


**Notes**

.. [1] See this still from |gentlemen_prefer_blondes| in Wikipedia as an example of Technicolor filming.
