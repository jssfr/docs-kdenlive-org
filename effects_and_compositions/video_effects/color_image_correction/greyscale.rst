.. meta::

   :description: Do your first steps with Kdenlive video editor, using greyscale effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, greyscale

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-greyscale:

Greyscale
=========

This effect/filter discards color information and turns the source image to greyscale.

The effect works like a switch and does not have keyframes.
