.. meta::

   :description: Do your first steps with Kdenlive video editor, using white balance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, white balance

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-white_balance:

White Balance
=============

This effect/filter adjusts the white balance of the source image.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-white_balance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-white_balance

   White Balance effect

* **Neutral Color** - Choose the color from the source image that should be white

* **Green Tint** - Adjust the level of green
