.. meta::

   :description: Do your first steps with Kdenlive video editor, using saturation effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, saturation

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-saturation:

Saturation
==========

This effect/filter adjusts the :term:`saturation` of a source image.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-saturation.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-saturation

   Saturation effect

* **Saturation** - Define the level of saturation. Allowed values are between 1 and 1000 (default is 125) where 1 turns the image into greyscale.

.. .. rst-class:: clear-both


.. **Notes**

.. See :ref:`grading of GoPro Hero 3 footage <tutorial-grading_gopro>` in the :ref:`useful_information` section of this documentation.


.. See `TheDiveo's blog <https://thediveo-e.blogspot.com/2013/10/grading-of-hero-3-above-waterline.html>`_ for an example of the usage of the Saturation effect.
