.. meta::

   :description: Do your first steps with Kdenlive video editor, using lift/gamma/gain effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, lift/gamma/gain, lift gamma gain, lift, gamma, gain

   :authors: - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-lift_gamma_gain:

Lift/Gamma/Gain
===============

This effect/filter allows you to adjust the lift (impacting mainly shadows), gain (impacting mainly highlights) and :term:`gamma` (impacting mainly midtones). The color wheel inputs allow to control the degree to which these effects apply to the R, G & B color channels. By default, the white color at the centre of the colour wheel is selected, meaning the effect applies equally to all three color channels. By choosing another color on the color wheel, the effect will be applied on the R, G & B channels in proportion to the RGB components that make up that color.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-liftgammagain.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-liftgammagain

   Lift/Gamma/Gain effect

..
