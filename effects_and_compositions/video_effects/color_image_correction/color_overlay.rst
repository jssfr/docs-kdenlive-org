.. meta::

   :description: Do your first steps with Kdenlive video editor, using color overlay effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, color overlay, colorize

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-color_overlay:

Color Overlay
=============

This effect/filter overlays a solid color on the video stream.

This effect has keyframes

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_overlay.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_overlay

   Color Overlay effect

* **Hue** - Set the color :term:`hue`. Allowed values are from 0 to 360, default value is 0.

* **Saturation** - Set the color :term:`saturation`. Allowed values are from 0 to 1, default value is 0.5.

* **Lightness** - Set the color :term:`lightness`. Allowed values are from 0 to 1, default value is 0.5.

* **Mix** - Set the mix of source lightness. Allowed values are from 0.0 to 1.0, default value is 1.0.
