.. meta::

   :description: Do your first steps with Kdenlive video editor, using the brightness (keyframable) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, brightness (keyframable)

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Mmaguire (https://userbase.kde.org/User:Mmaguire)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. |brightness| raw:: html

   <a href="https://www.mltframework.org/plugins/FilterBrightness/" target="_blank">brightness</a>


.. _effects-brightness_2:

Brightness (keyframable)
========================

This effect adjusts the brightness of the clip. Intensity values range from 0 (completely dark) to 400 (bright colors)

This effect has keyframes.

This is the |brightness| MLT filter.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-brightness_2.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-brightness_2

   Brightness (keyframable) effect

..

.. rst-class:: clear-both

.. note:: This brightness effect works differently than the :ref:`effects-brightness` effect. At full intensity (400) the clip is not glaringly bright (or white) but just has bright colors.
