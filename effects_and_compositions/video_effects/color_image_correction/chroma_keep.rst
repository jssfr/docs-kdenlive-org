.. meta::

   :description: Do your first steps with Kdenlive video editor, using the chroma keep effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, chroma keep

   :authors: - Bernd Jordan

   :license: Creative Commons License SA 4.0



.. https://youtu.be/dXnFsOjS734


.. _effects-chroma_keep:

Chroma Keep
===========

This effect turns the clip into grey scale except for the selected color.

This effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-chroma_keep.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-chroma_keep

   Chroma Keep effect

* **Variance** - Percentage of deviation from the selected color value. Lower values keep the colors closer to the selected one; higher values include more colors.

.. rst-class:: clear-both


.. hint:: This effect is much more effective and easier to apply than the :ref:`effects-chroma_hold` effect.
