.. meta::

   :description: Do your first steps with Kdenlive video editor, using sepia effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, sepia

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |wikipedia_sepia| raw:: html

   <a href="https://en.wikipedia.org/wiki/Sepia_(color)" target="_blank">Sepia</a>


.. _effects_sepia:

Sepia
=====

This effect/filter turns clip colors to sepia\ [1]_.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sepia.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-sepia

   Sepia effect

* **Chrominance U** - Changes the U :term:`plane` (blue projection). Allowed values are from 0 to 255, default is 75.

* **Chrominance V** - Changes the V :term:`plane` (red projection). Allowed values are from 0 to 255, default is 75.

.. rst-class:: clear-both

By using the :guilabel:`Chrominance` sliders you can adjust the level of brownishness to the look you want.


**Notes**

.. [1] See this article about |wikipedia_sepia| in Wikipedia.
