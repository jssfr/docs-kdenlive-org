.. meta::

   :description: Do your first steps with Kdenlive video editor, using equaliz0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, equaliz0r

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |wikipedia_histogram_equalization| raw:: html

   <a href="https://en.wikipedia.org/wiki/Histogram_equalization" target="_blank">histogram equalization</a>


.. _effects-equaliz0r:

Equaliz0r
=========

This effect/filter equalizes the intensity histograms. Essentially, it tries to give each color the same intensity thus increasing the global contrast.\ [1]_

The effect is a simple switch, and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-equaliz0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-equaliz0r

   Equaliz0r effect

..

.. rst-class:: clear-both

**Notes**

.. [1] See the article in Wikipedia about |wikipedia_histogram_equalization|.
