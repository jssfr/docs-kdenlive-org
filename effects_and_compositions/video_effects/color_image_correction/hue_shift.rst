.. meta::

   :description: Do your first steps with Kdenlive video editor, using hue shift effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, color and image correction, hue shift

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-hue_shift:

Hue Shift
=========

This effect/filter shifts the :term:`hue` of a source image.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-hue_shift.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-hue_shift

   Hue Shift effect

* **Hue** - Sets the hue of the entire image
