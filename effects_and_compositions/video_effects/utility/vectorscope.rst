.. meta::

   :description: Do your first steps with Kdenlive video editor, using vectorscope effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, utility, vectorscope

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-vectorscope:

Vectorscope
===========

This effect/filter draws and overlays a vectorscope of the video data.

This is different from the :ref:`view-vectorscope` from the :ref:`view_menu` because the Effect version writes the vectorscope into the ouput video, whereas the View Menu version displays the vectorscope in a separate widget while you still can preview your project.

The effect works like a switch and does not have any parameters nor keyframes. Compare with the :ref:`effects-vectorscope_advanced` effect.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-vectorscope.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-vectorscope

   Vectorscope effect

..
