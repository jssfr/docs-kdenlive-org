.. meta::

   :description: Do your first steps with Kdenlive video editor, using nikon d90 stairstepping fix effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, utility, nikon d90 stairstepping fix

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-nikon_d90_stairstepping_fix:

Nikon D90 Stairstepping Fix
===========================

This effect/filter removes stairstepping artifacts from Nikon D90's 720p videos by interpolation.

Sharp lines in videos from the Nikon D90 show steps each 8th or 9th line, assumedly due to poor downsampling. These can be smoothed out with this filter if they become too annoying.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-nikon_d90_stairstepping_fix.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-nikon_d90_stairstepping_fix

   Nikon D90 Stairstepping Fix effect

..
