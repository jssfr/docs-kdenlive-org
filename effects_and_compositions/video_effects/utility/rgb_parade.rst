.. meta::

   :description: Do your first steps with Kdenlive video editor, using oscilloscope (advanced) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, utility, oscilloscope (advanced)

.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-rgb_parade:

RGB Parade
==========

This effect/filter displays a histogram of R, G and B components of the video data.

This is different from the :ref:`view-rgb_parade` from the :ref:`view_menu` because the Effect version writes the histogram into the output video whereas the View Menu version just displays the histogram widget in the application while you preview your project.

The effect works like a switch, does not have any parameters nor keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-rgb_parade.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-rgb_parade

   RGB Parade effect

..
