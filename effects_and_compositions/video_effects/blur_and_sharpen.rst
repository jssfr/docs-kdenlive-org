.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)
             - Smolyaninov (https://userbase.kde.org/User:Smolyaninov)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-blur_and_sharpen:

================
Blur and Sharpen
================

The *Blur and Sharpen* video effects category provides effects and filters to blur or sharpen the clip. Combined with masks from the *Alpha, Mask and Keying* category it is possible to apply them only to portions of the clip. For more details about this technique see the :ref:`effects-mask_apply` section of the documentation.

The following filters are available:

.. toctree::
   :maxdepth: 1
   :glob:

   blur_and_sharpen/*

