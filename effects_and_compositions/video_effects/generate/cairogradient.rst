.. meta::

   :description: Do your first steps with Kdenlive video editor, using cairogradient effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, generate, cairogradient, gradient

.. metadata-placeholders

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-cairogradient:

Cairogradient
=============

This effect/filter draws a gradient on top of the image source.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-cairogradient.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-cairogradient

   Cairogradient effect

* **Pattern** - Set to **Linear** or **Radial**

* **Blend Mode** - Determines the mode used to composite  gradient and image source. Options are **normal**, **add**, **saturate**, **multiply**, **screen**, **overlay**, **darken**, **lighten**, **colordodge**, **colorburn**, **hardlight**, **softlight**, **difference**, **exclusion**, **hslhue**, **hslsaturation**, **hslcolor** and **hslluminosity**. Default is **normal**.

* **Start Color** - First color of the gradient

* **Start Opacity** - Opacity of the first color of the gradient

* **End Color** - Second color of the gradient

* **End Opacity** - Opacity of the second color of the gradient

* **Start / End X** - X position of the start and end point of the gradient

* **Start / End Y** - Y position of the start and end point of the gradient

* **Offset** - Position of the first color in the line connecting gradient ends. Really useful only for **radial** :guilabel:`Pattern`.
