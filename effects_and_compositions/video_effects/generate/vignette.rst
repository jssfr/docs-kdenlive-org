.. meta::

   :description: Do your first steps with Kdenlive video editor, using vignette effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, generate, vignette

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-vignette:

Vignette
========

This effect/filter creates a natural lens vignetting effect. It is similar to the :ref:`effects-vignette_effect` effect but lacks the built-in ability to move the center around.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-vignette.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-vignette

   Vignette effect

* **Aspect ratio** - Select the aspect ratio of the vignette to make it more circular or more elliptical. Default is 500 (circular). Values below 500 "squash" the vignette vertically making it look elongated horizontally; values above 500 "squash" the vignette horizontally making it look elongated vertically.

* **Clear center size** - Set the size of the unaffected center

* **Softness** - Set the softness of the vignette
