.. meta::

   :description: Do your first steps with Kdenlive video editor, using draw box effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, generate, draw box

.. metadata-placeholders

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-draw_box:

Draw Box
========

This effect/filter draws a colored box on the input video.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-draw_box.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-draw_box

   Draw Box effect

* **X / Y** - Defines the coordinates for the top left-hand corner of the box (coordinates are based on the project dimension settings)

* **Width** - Defines the width in pixels

* **Height** - Defines the height in pixels

* **Thickness** - Defines the thickness of the line in pixels. If set high enough the box is filled.

* **Color** - Defines the color for the box

.. rst-class:: clear-both


.. note:: As of this writing and in version 23.04.2 the color parameter is being ignored and black is used.
