.. meta::

   :description: Do your first steps with Kdenlive video editor, using scanline0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, generate, scanline0r, scanline

.. metadata-placeholders

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-scanline0r:

Scanline0r
==========

This effect/filter creates interlaced black lines imitating an old monitor.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-scanline0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-scanline0r

   Scanline0r effect

..
