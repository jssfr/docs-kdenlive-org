.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-despill:

Despill
-------

Remove unwanted contamination of foreground colors caused by reflected color of greenscreen or bluescreen (avfilter.despill)

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-despill.webp
   :alt: kdenlive2304_effects-despill

   Despill effect panel
