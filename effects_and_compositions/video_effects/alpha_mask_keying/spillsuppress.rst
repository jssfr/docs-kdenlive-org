.. metadata-placeholder

   :authors: - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. |spillsupress| raw:: html

   <a href="https://gstreamer.freedesktop.org/documentation/frei0r/frei0r-filter-spillsupress.html?gi-language=c" target="_blank">frei0r.spillsupress</a>


.. _effects-spillsupress:

Spill Supress
==============

Remove green or blue spill light from subjects shot in front of green or blue screen. This is the |spillsupress| filter.
