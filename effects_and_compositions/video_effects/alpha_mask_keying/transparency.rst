.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. |frei0r.transparency| raw:: html

   <a href="https://www.mltframework.org/plugins/FilterFrei0r-transparency/" target="_blank">frei0r.transparency</a>


.. _effects-transparency:

Transparency
------------

Tunes the alpha channel. (|frei0r.transparency|)

Can also be used to simply change the transparency of the clip. Keyframable.
