.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-alpha_gradient:

Alpha Gradient
==============

Fill the alpha channel with a specified gradient (frei0r.alphagrad)\ [1]_. Its purpose is to enable, together with alpha controlled color manipulation, the use of graduated neutral-density filters similar to what one uses in photography\ [2]_.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-alpha_gradient_before.webp
   :align: left
   :width: 90%
   :alt: kdenlive2304_effects-alpha_gradient_before

   Alpha Gradient effect - before


.. figure:: /images/effects_and_compositions/kdenlive2304_effects-alpha_gradient_after.webp
   :align: left
   :width: 90%
   :alt: kdenlive2304_effects-alpha_gradient_after

   Alpha Gradient effect - after


**Notes**

.. |wiki_graduated_nd_filter| raw:: html

   <a href="https://en.wikipedia.org/wiki/Graduated_neutral-density_filter" target="_blank">graduated neutral-density filter</a>

.. |alphaops| raw:: html

   <a href="https://github.com/dyne/frei0r/blob/master/src/filter/alpha0ps/readme" target="_blank">frei0r alpha0ps plugins</a>

.. [1] The description of this effect has been taken in parts from the readme file for the |alphaops|. You find much more detailed information there.

.. [2] For more details about this topic refer to the Wikipedia entry about the |wiki_graduated_nd_filter|.
