.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-lumakey:

Lumakey
-------

This filter modifies image's alpha channel as a function of its luma value. This is used together with a compositor to combine two images so that bright or dark areas of source image are overwritten on top of the destination image. (lumakey)

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-lumakey.webp
   :alt: kdenlive2304_effects-lumakey

   Lumakey effect panel
