.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-bluescreen0r:

Bluescreen0r
------------

Color to alpha (blit SRCALPHA) (frei0r.bluescreen0r)

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-bluescreen0r.webp
   :alt: kdenlive2304_effects-bluescreen0r

   Bluescreen0r effect panel
