.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. |frei0r_mask0mate| raw:: html

   <a href="https://www.mltframework.org/plugins/FilterFrei0r-mask0mate/" target="_blank">frei0r.mask0mate</a>


.. _effects-rectangular_alpha_mask:

Rectangular Alpha Mask
----------------------

This effect was previously called *Mask0Mate* and is the |frei0r_mask0mate| MLT filter.

Creates a rectangular alpha-channel mask.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-rect_alpha_mask.webp
   :width: 90%
   :alt: kdenlive2304_effects-rect_alpha_mask

   Rectangular Alpha Mask effect panel and example
