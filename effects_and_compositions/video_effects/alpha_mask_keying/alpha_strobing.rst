.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0


.. _effects-alpha_strobing:

Alpha Strobing
--------------

Strobes the alpha channel to 0. Many other filters overwrite the alpha channel, in that case this needs to be last. (strobe)
