.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-mask_apply:

Mask Apply
----------

Apply the previous effects starting with an effect with "(Mask)" in its name. (mask_apply)

This is a very powerful effect function as it does not do anything itself but instruct Kdenlive to apply the effects beginning with the (Mask) effect only to the area defined by that (Mask) effect.

See the :ref:`Masking Effects <effects-masking_effects>` section for more details.

In the *Template* effects category there is a pre-setup *Secondary Color Correction* that uses a (Mask) effect and the Mask Apply effect.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sec_color_corr_area_select.webp
   :width: 80%
   :alt: kdenlive2304_effects-sec_color_corr_area_select

   Secondary Color Correction Area Selection (Mask) effect panel
