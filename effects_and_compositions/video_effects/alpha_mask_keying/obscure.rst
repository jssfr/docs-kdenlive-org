.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan

   :license: Creative Commons License SA 4.0

.. _effects-obscure:

Obscure
-------

Hide a region of the clip by pixelizing it (obscure). The effect's pixelization cannot be adjusted and may therefore not work satisfactorily with all source material. Compare the :ref:`effects-pixelize` effect.

.. In that case try the :ref:`Pixelate <effects-pixelize>` effect.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-obscure.webp
   :align:  left
   :width: 400px
   :figwidth: 400px
   :alt: kdenlive2304_effects-obscure

   Obscure effect panel and results (default settings)

Use the handles on the red rectangle\ [1]_ in the Project Monitor to change the size: use the center handle to move the rectangle around.

.. rst-class:: clear-both


**Notes**

.. [1] If you do not see a red rectangle in the Project Monitor, enable Edit Mode by clicking on the |edit-mode| icon in the Project Monitor toolbar
