.. meta::

   :description: Do your first steps with Kdenlive video editor, using binarize dynamically effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, binarize dynamically

.. metadata-placeholder

   :authors: - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-binarize_dynamically:

Binarize Dynamically
====================

This effect/filter creates a black and white image through dynamic thresholding.

This effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-binarize_dynamically.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-binarize_dynamically

   Binarize Dynamically effect

..

