.. meta::

   :description: Do your first steps with Kdenlive video editor, using threshold effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, threshold

.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |threshold0r| raw:: html

   <a href="https://mltframework.org/plugins/FilterFrei0r-threshold0r/" target="_blank">frei0r.threshold0r</a>


.. _effects-threshold:

Threshold
=========

This effect/filter thresholds a source image.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-threshold.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-threshold>

   Threshold effect

* **Threshold** - Set the threshold in a range from 0 (white) to 1000 (black)

.. rst-class:: clear-both


**Note**

This is the |threshold0r| filter that used to be called *threshold0r* in the Kdenlive video effects list.
