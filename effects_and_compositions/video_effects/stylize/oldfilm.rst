.. meta::

   :description: Do your first steps with Kdenlive video editor, using oldfilm effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, oldfilm

.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-oldfilm:

Old Film
========

This effect/filter moves the video up and down and adds random brightness changes, making it look like old film footage. See also :ref:`effects-dust` and :ref:`effects-scratchlines` effects.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-oldfilm.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-oldfilm

   Old Film effect

* **Y-Delta** - Maximum delta value of up/down move (jitter)

* **% of picture have delta** - Set the %age of frames that have a Y-Delta (jitter)

* **Brightness up** - Sets the value by which the image will be made lighter

* **Brightness down** - Sets the value by which the image will be made darker

* **Brightness every** - Set the %age of frames that will have the brightness changed

* **Unevendevelop up** - Sets the value by which the image will be made lighter

* **Unevendevelop down** - Sets the value by which the image will be made darker

* **Unevendevelop Duration** - Sets the time in frames of an up/down cycle for uneven development



.. https://youtu.be/0g1xDo-pwm0

   https://youtu.be/PuQTd6D2Y2Y
