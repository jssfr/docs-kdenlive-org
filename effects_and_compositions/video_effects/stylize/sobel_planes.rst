.. meta::

   :description: Do your first steps with Kdenlive video editor, using sobel with planes effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, sobel with planes

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-sobel_planes:

Sobel with Planes
=================

This effect/filter is the same as the :ref:`effects-sobel` effect but has parameters for more control. Compare :ref:`effects-kirsch`, :ref:`effects-prewitt` and :ref:`effects-roberts` effects.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sobel_planes.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-sobel_planes

   Sobel with Planes effect

* **Planes** - Set which :term:`planes<plane>` will be processed, unprocessed planes will be copied. Default is **YUV**.

* **Scale** - Set value which will be multiplied with filtered result

* **Delta** - Set value which will be added to filtered result
