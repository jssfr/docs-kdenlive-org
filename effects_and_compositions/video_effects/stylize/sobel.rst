.. meta::

   :description: Do your first steps with Kdenlive video editor, using sobel effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, sobel

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |sobel| raw:: html

   <a href="https://en.wikipedia.org/wiki/Sobel_operator" target="_blank">Sobel operator</a>


.. _effects-sobel:

Sobel
=====

This effect/filter is an edge detection filter using the Sobel operator\ [1]_. Compare :ref:`effects-kirsch`, :ref:`effects-prewitt` and :ref:`effects-roberts` effects.

The effect works like a switch, and does not have any parameters or keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sobel.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-sobel

   Sobel effect

..

.. rst-class:: clear-both


**Notes**

.. [1] For more details refer to the |sobel| article in Wikipedia


.. https://youtu.be/sSlJovKEZJk
