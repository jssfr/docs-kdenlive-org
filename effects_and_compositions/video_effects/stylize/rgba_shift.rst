.. meta::

   :description: Do your first steps with Kdenlive video editor, using rgba shift effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, rgba shift

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-rgba_shift:

RGBA Shift
==========

This effect/filter shifts R/G/B/A pixels horizontally and/or vertically creating an appearance similar to the Tik Tok logo. Compare the :ref:`effects-rgbsplit0r` and :ref:`effects-chroma_shift` effects which essentially do the same but in the case of :ref:`effects-rgbsplit0r` lack the ability to determine the edge operation.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-rgba_shift.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-rgba_shift

   RGBA Shift effect

* **Red Horizontal shift** - Set amount to shift red horizontally. Positive values shift right, negative values shift left.

* **Red Vertical shift** - Set amount to shift red vertically. Positive values shift up, negative values shift down.

* **Green Horizontal shift** - Set amount to shift green horizontally. Positive values shift right, negative values shift left.

* **Green Vertical shift** - Set amount to shift green vertically. Positive values shift up, negative values shift down.

* **Blue Horizontal shift** - Set amount to shift blue horizontally. Positive values shift right, negative values shift left.

* **Blue Vertical shift** - Set amount to shift blue vertically. Positive values shift up, negative values shift down.

* **Alpha Horizontal shift** - Set amount to shift red horizontally. Positive values shift right, negative values shift left.

* **Alpha Vertical shift** - Set amount to shift alpha vertically. Positive values shift up, negative values shift down.

* **Edge operation** - Set edge mode. Options are **Smear** (default; shifted pixel leave a trail), and **Wrap** (shifted pixels wrap around the image and appear on the opposite side)
