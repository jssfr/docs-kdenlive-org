.. meta::

   :description: Do your first steps with Kdenlive video editor, using glow effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, glow

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-glow:

Glow
====

This effect/filter creates a glamorous glow by lightening the image and adding some blur around the edges.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-glow.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-glow

   Glow effect

* **Blur** - Set the amount of blurriness on the edges. The higher the number the more blur is applied increasing the glow effect.


.. https://youtu.be/vh4lrkFaVWc

   https://youtu.be/UtBWFrYN9kA
