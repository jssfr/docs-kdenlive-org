.. meta::

   :description: Do your first steps with Kdenlive video editor, using primaries effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, primaries

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-primaries:

Primaries
=========

This effect/filter reduces the image colors to the primary colors.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-primaries.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-primaries

   Primaries effect

* **Factor** - Influence of mean px value ( > 32 = 0 )


.. https://youtu.be/gjgQphzQZrQ
