.. meta::

   :description: Do your first steps with Kdenlive video editor, using emboss effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, emboss

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-emboss:

Emboss
======

This effect/filter creates a greyish embossed relief appearance of the source image.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-emboss.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-emboss

   Emboss effect

* **Light direction** - The direction of the embossing (practically simulating a light source). Value is an angle between 0 and 360 (degrees).

* **Background lightness** - Defines the brightness/lightness of the areas not embossed

* **Bump height** - Defines the thickness of the edges increasing/decreasing the perceived depth of the embossed areas
