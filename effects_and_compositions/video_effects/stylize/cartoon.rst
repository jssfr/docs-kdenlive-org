.. meta::

   :description: Do your first steps with Kdenlive video editor, using cartoon effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, cartoon

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-cartoon:

Cartoon
=======

This effect/filter does a form of edge detect giving the video a cartoonish appearance.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-cartoon.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-cartoon

   Cartoon effect

* **Level of trip** - Changes the intensity of the effect. The lower the number, the coarser the edge detect is.

* **Difference space** - Changes the intensity of the effect. The higher the number, the thicker the edge will be drawn.


.. https://youtu.be/92fI4znypEo

