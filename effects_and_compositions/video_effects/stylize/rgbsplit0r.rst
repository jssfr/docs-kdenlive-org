.. meta::

   :description: Do your first steps with Kdenlive video editor, using rgbsplit0r effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, rgbsplit0r

.. metadata-placeholder

   :authors: - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-rgbsplit0r:

Rgbsplit0r
==========

This effect/filter takes the red, green and blue channels in the video and separates them by a given x and y amount to produce a rainbowish effect similar to the Tik Tok logo. Compare the :ref:`effects-rgba_shift` effect which essentially does the same but allows individual shifting of the RGB and Alpha channels and has the ability to determine the edge operation. Also compare the :ref:`effects-chroma_shift` effect.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-rgbsplit0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-rgbsplit0r

   Rgbsplit0r effect

* **Vertical split distance** - How far should layers be moved vertically from each other

* **Horizontal split distance** - How far should layers be moved horizontally from each other
