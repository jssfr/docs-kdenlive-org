.. meta::

   :description: Do your first steps with Kdenlive video editor, using 3-level threshold effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, 3-level threshold

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-3-level_threshold:

3-level Threshold
=================

This effect/filter performs a dynamic 3-level thresholding action turning the source into black, grey and white areas.

The effect does not have keyframes or any parameters to set

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-3-level_threshold.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-3-level_threshold

   3-level Threshold effect

..
