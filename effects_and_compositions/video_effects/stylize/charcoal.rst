.. meta::

   :description: Do your first steps with Kdenlive video editor, using charcoal effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, charcoal

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-charcoal:

Charcoal
========

This effect/filter gives the clip a charcoal drawing effect.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-charcoal.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-charcoal

   Charcoal effect

* **Invert** - Inverts the clip

* **Horizontal scatter** - Defines the line width. Range is 1 to 10, default is 2.

* **Vertical scatter** - Defines the line height. Range is 1 to 10, default is 2.

* **Scale** - Defines the contrast. Range is 1 to 10, default is 1.

* **Mix** - Defines the color :term:`planes<plane>`. Range is 1 to 10, default is 0 (black & white)



.. https://youtu.be/fI1YrZcT26k

   https://youtu.be/1XpJCI6tHSc

   https://youtu.be/m-_MFMSb75w
