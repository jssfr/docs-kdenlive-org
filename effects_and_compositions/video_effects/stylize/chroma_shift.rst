.. meta::

   :description: Do your first steps with Kdenlive video editor, using chroma shift effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, chroma shift

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-chroma_shift:

Chroma Shift
============

This effect/filter shifts chroma pixels horizontally and/or vertically similar to the Tik Tok logo effect. Compare :ref:`effects-rgba_shift` and :ref:`effects-rgbsplit0r` effects.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-chroma_shift.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-chroma_shift

   Chroma Shift effect

* **Chroma-blue H shift** - Set amount to shift chroma-blue horizontally

* **Chroma-blue V shift** - Set amount to shift chroma-blue vertically

* **Chroma-red H shift** - Set amount to shift chroma-red horizontally

* **Chroma-red V shift** - Set amount to shift chroma-red vertically

* **Edge mode** - Set edge mode determining what happens to pixels when shifted outside the frame. Options are **Smear** (default) or **Warp**.
