.. meta::

   :description: Do your first steps with Kdenlive video editor, using posterize effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, posterize

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |posterization| raw:: html

   <a href="https://en.wikipedia.org/wiki/Posterization" target="_blank">Posterization</a>


.. _effects-posterize:

Posterize
=========

This effect/filter posterizes\ [1]_ the image by reducing the number of colors used in the image.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-posterize.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-posterize

   Posterize effect

* **Levels** - Number of values per channel

.. rst-class:: clear-both


**Notes**

.. [1] For more details about posterization refer to the |posterization| article in Wikipedia
