.. meta::

   :description: Do your first steps with Kdenlive video editor, using color distance effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, color distance

.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-color_distance:

Color Distance
==============

This effect/filter calculates the distance between the selected color and the current pixel and uses that value as new pixel value.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_distance.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_distance

   Color Distance effect

* **Source Color** - Select the color either by using the color picker or by clicking on the color button.



.. https://youtu.be/eL8cFUJrUo0

   https://youtu.be/4Ta9UE2nflU

   https://youtu.be/7VRQyCUxYUQ
