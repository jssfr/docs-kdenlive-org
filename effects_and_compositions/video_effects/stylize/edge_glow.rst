.. meta::

   :description: Do your first steps with Kdenlive video editor, using edge glow effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, edge glow

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-edge_glow:

Edge Glow
=========

This effect/filter creates an edge glow.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-edge_glow.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-edge_glow

   Edge Glow effect

* **Edge lightening threshold** - Defines the threshold for edge detection. If set to 0 (default), all edges will be detected.

* **Edge brightness upscaling multiplier** - Defines the level of brightness in which edges are drawn

* **Non-edge downscaling multiplier** - Defines the darkening of areas between edges

.. rst-class:: clear-both


For sources with large contiguous areas like a sky or a wall the :guilabel:`Edge lightening threshold` can be used to ignore the small and subtle changes in color or luminance that would otherwise be detected as edges.



.. https://youtu.be/d0MvA_7VuJk

   https://youtu.be/Cl0Z8FXULbQ
