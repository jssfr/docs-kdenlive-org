.. meta::

   :description: Do your first steps with Kdenlive video editor, using color effect effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, color effect

.. metadata-placeholder

   :authors: - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-color_effect:

Color Effect
============

This effect/filter applies a pre-made color effect to the image.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-color_effect.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-color_effect

   Color Effect effect

* **Type** - Select the pre-made effect type

.. rst-class:: clear-both


Possible effect types are:

xpro (default), sepia, heat, red_green, old_photo, xraym, esses and yellow_blue.
