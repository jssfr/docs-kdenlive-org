.. meta::

   :description: Do your first steps with Kdenlive video editor, using soft glow effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, soft glow

.. metadata-placeholder

   :authors: - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-soft_glow:

Soft Glow
=========

This effect/filter creates a soft glow effect on highlights.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-soft_glow.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-soft_glow

   Soft Glow effect

* **Blend mode** - Defines how to blend highlight blur with input image. Options are **Screen** (default), **Overlay**, and **Add**.

* **Blur of the glow** - Set the blurriness of the glow

* **Brightness** - Set the brightness of highlight areas

* **Sharpness** - Set the sharpness of highlight areas
