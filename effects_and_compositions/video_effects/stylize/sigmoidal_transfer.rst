.. meta::

   :description: Do your first steps with Kdenlive video editor, using sigmoidal transfer effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize, sigmoidal transfer

.. metadata-placeholder

   :authors: - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-sigmoidal_transfer:

Sigmoidal Transfer
==================

This effect/filter desaturates the image and creates a particular look that could be called Stamp, Newspaper or Photocopy.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-sigmoidal_transfer.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-sigmoidal_transfer

   Sigmoidal Transfer effect

* **Brightness** - Controls brightness of image. Range 0 to 100.

* **Sharpness** - Controls sharpness of transfer. Range 0 to 100.
