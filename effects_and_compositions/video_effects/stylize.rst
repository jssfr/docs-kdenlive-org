.. meta::

   :description: Do your first steps with Kdenlive video editor, stylize video effects category
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, stylize

.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-stylize:

=======
Stylize
=======

The *Stylize* video effects category contains effects and filters to give images and videos an artistic appearance (e.g. :ref:`effects-charcoal` or :ref:`effects-cartoon`).

The following effects and filters are available:

.. toctree::
   :maxdepth: 1
   :glob:

   stylize/*
