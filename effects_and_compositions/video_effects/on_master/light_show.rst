.. meta::

   :description: Do your first steps with Kdenlive video editor, using light show effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, misc, miscellaneous, light show

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-light_show:

Light Show
==========

This effect/filter is an audio visualization effect that colors the image proportional to the magnitude of the audio spectrum.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-light_show.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-light_show

   Light Show effect

* **Low Frequency** - The low end of the frequency range to be used to influence the image changes

* **High Frequency** - The high end of the frequency range to be used to influence the image changes

* **Threshold** - The minimum amplitude of sound that must occur within the frequency range to cause the image to be changed

* **Oscillation** - Oscillation can be useful to make the image move back and forth during long periods of sound. A value of 0 specifies no oscillation.

* **1st / 2nd Color** - The color of the gradient used for lightening or darkening the image
