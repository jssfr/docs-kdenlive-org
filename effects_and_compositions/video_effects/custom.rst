.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)

   :license: Creative Commons License SA 4.0

.. _effects-custom:

==============
Custom Effects
==============

The **Custom Group** in the **Effect List** is where effects appear when you choose :menuselection:`Save Effect` from an effect in the  :ref:`effects_and_compositions`.

.. image:: /images/Save_Effect.png
   :align: left
   :alt: Save_Effect

.. image:: /images/Name_effect.png
   :align: left
   :alt: Name_effect

.. image:: /images/Custom_effect.png
   :align: left
   :alt: Custom_effect
