.. meta::

   :description: Do your first steps with Kdenlive video editor, using flippo effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, flippo

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-flippo:

Flippo
======

This effect/filter flips the input video along the X and/or Y axis. It basically combines the :ref:`effects-flip_horizontally` and :ref:`effects-flip_vertically` effects. Also compare :ref:`effects-mirror`.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-flippo.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-flippo

   Flippo effect

* **X axis** - Flip along the X axis (flip vertically)

* **Y axis** - Flip along the Y axis (flip horizontally)
