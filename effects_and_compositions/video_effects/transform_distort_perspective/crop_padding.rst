.. meta::

   :description: Do your first steps with Kdenlive video editor, using crop by padding effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, crop by padding

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-crop_padding:

Crop by Padding
===============

This effect/filter crops the image to a rounded rectangle or circle by padding the edges with a specified color.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-crop_by_padding.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-crop_by_padding

   Crop by Padding effect

* **Circle** - If switched on, the :guilabel:`Radius` parameter creates a circular crop. Otherwise a rectangle is used. Default is **off**.

* **Radius** - Amount of circular or rectangle rounding

* **Padding Color** - The color to be used for padding. Can be alpha.

.. rst-class:: clear-both


The parameters **X**, **Y**, **W**, **H**, and **Size** can be used to move and/or scale the rectangle or circle within the frame in order to crop a specific portion of the image.
