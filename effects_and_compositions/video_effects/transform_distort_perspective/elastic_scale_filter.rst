.. meta::

   :description: Do your first steps with Kdenlive video editor, using elastic scale filter effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, elastic scale filter

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-elastic_scale_filter:

Elastic Scale Filter
====================

This effect/filter allows to scale the video sources non-linearly.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-elastic_scale_filter.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-elastic_scale_filter

   Elastic Scale Filter effect

* **Center** - Horizontal center position of the linear area

* **Linear Width** - Width of the linear area

* **Linear Scale Factor** - Amount how much the linear area is scaled

* **Non-Linear Scale Factor** - Amount how much the outer left and outer right areas are scaled non linearly
