.. meta::

   :description: Do your first steps with Kdenlive video editor, using distort effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, distort

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-distort:

Distort
=======

This effect/filter creates an overlapping wave distortion with adjustable amplitude and frequency giving the video a plasma appearance.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-distort.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-distort

   Distort effect

* **Amplitude** - Set the amplitude of the plasma signal. Range is -1000 to 1000, default is 0.

* **Frequency** - Set the frequency of the plasma signal. Range is -2000 to 2000, default is 0.
