.. meta::

   :description: Do your first steps with Kdenlive video editor, using pillar echo effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, pillar echo

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-pillar_echo:

Pillar Echo
===========

This effect/filter creates an echo effect (blur) outside of an area of interest. It is very useful for using vertical video sources (like recorded on a smartphone) in a horizontal project.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-pillar_echo.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-pillar_echo

   Pillar Echo effect

* **Blur** - Set the blur amount in %

* **X / Y / W / H / Size** - Adjusts the area where the blur is applied and visible.
