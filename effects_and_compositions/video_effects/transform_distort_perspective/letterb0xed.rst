.. meta::

   :description: Do your first steps with Kdenlive video editor, using letterb0xed effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, letterb0xed

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-letterb0xed:

LetterB0xed
===========

This filter/effect adds black borders at the top and bottom for this Cinema look.

The effect has keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-letterb0xed.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-letterb0xed

   LetterB0xed effect

* **Transparency** - Makes the black borders transparent

* **Border Width** - Set the border width in pixel



.. https://youtu.be/9Ldjt0QZPzs

.. https://youtu.be/JBp8wQW-_Qw
