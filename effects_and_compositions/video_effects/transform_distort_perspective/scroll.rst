.. meta::

   :description: Do your first steps with Kdenlive video editor, using scroll effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, scroll

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-scroll:

Scroll
======

This effect/filter scrolls the clip horizontally and/or vertically.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-scroll.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-scroll

   Scroll effect

* **Horizontal scrolling speed** - Set the horizontal scrolling speed

* **Vertical scrolling speed** - Set the vertical scrolling speed

* **Initial horizontal position** - Set the initial horizontal position

* **Initial vertical position** - Set the initial vertical position

.. rst-class:: clear-both


.. note:: Negative values make the scrolling go from left to right. The higher the value the faster the scrolling. A value of 1.000 renders the effect useless.
