.. meta::

   :description: Do your first steps with Kdenlive video editor, using mirror effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, mirror

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _effects-mirror:

Mirror
======

This effect/filter mirrors your image in any direction. It can also flip it horizontally and vertically.  Compare :ref:`effects-flip_vertically`, :ref:`effects-flip_horizontally` and :ref:`effects-flippo` effects.

The effect does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-mirror.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-mirror

   Mirror effect

* **Mirroring direction** - Set the mirroring axis. Options are **Horizontal** (default), **Vertical**, **Diagonal**, and **X Diagonal**. By default, the right and bottom half are mirrored. Use :guilabel:`Invert` to change this.

* **Invert** - Invert the direction (not effective on **Flip** and **Flop**)


.. https://youtu.be/ao32j0dSVII

.. https://youtu.be/3-hcMZu52Vk
