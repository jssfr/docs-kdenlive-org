.. meta::

   :description: Do your first steps with Kdenlive video editor, using flip horizontally effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, flip horizontally

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-flip_horizontally:

Flip Horizontally
=================

This effect/filter flips the input video horizontally (along the X axis, mirroring on the center vertical axis). Compare :ref:`effects-flip_vertically`, :ref:`effects-flippo` and :ref:`effects-mirror` effects.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-flip_horizontally.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-flip_horizontally

   Flip Horizontally effect

..

