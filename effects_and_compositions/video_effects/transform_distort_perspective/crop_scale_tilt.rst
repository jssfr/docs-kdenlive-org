.. meta::

   :description: Do your first steps with Kdenlive video editor, using crop scale tilt effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, crop scale tilt

.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Roger (https://userbase.kde.org/User:Roger)
             - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0

.. _effects-crop_scale_and_tilt:

Crop, Scale and Tilt
====================

This effect/filter crops, scales and tilts an image.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-crop_scale_tilt.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-crop_scale_tilt

   Crop, Scale and Tilt effect

* **Crop left / right** - Number of pixels to crop from the left / right

* **Crop top / bottom** - Number of pixels to crop from the top / bottom

* **Scale X / Y** - Set the scale factor in % along X / Y axis

* **Tilt X / Y** - Position to move the image along the X / Y axis. Defaults to the center based on the project resolution settings (e.g. X=960, Y=540 for a 1920x1080 project).

.. rst-class:: clear-both


**Notes**

This effect was previously called *Scale and Tilt* and *Crop, Scale and Position*.


.. https://youtu.be/WV4bocj7ygw
