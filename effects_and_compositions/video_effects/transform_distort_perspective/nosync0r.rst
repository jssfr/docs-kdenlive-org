.. meta::

   :description: Do your first steps with Kdenlive video editor, using nosync0r (broken TV) effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, nosync0r, broken tv

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. |link| raw:: html

   <a href="link_URI" target="_blank">link_text</a>


.. _effects-nosync0r:

Nosync0r (Broken TV)
====================

This effect/filter shifts the input frame by a set amount (1/1000\ :sup:`th` of the frame height) and wraps it around the bottom edge. This simulates a broken TV set where the horizontal synchronization signal (HSync) is off or faulty.

The effect has keyframes. This allows you to simulate a continuous scrolling video.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-nosync0r.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-nosync0r

   Nosync0r (Broken TV) effect

* **HSync** - Offset amount (1/1000\ :sup:`th` of the frame height)
