.. meta::

   :description: Do your first steps with Kdenlive video editor, using flip vertically effect
   :keywords: KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video effects, transform, distort, perspective, flip vertically

.. metadata-placeholder

   :authors: - Bernd Jordan (https://discuss.kde.org/u/berndmj)

   :license: Creative Commons License SA 4.0


.. _effects-flip_vertically:

Flip Vertically
===============

This effect/filter flips the input video vertically (along the Y axis, mirroring on the center horizontal axis). Compare :ref:`effects-flip_horizontally`, :ref:`effects-flippo` and :ref:`effects-mirror` effects.

The effect works like a switch and does not have keyframes.

.. figure:: /images/effects_and_compositions/kdenlive2304_effects-flip_vertically.webp
   :width: 400px
   :figwidth: 400px
   :align: left
   :alt: kdenlive2304_effects-flip_vertically

   Flip Vertically effect

..
