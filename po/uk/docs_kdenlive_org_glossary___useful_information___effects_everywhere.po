# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-27 00:37+0000\n"
"PO-Revision-Date: 2021-12-27 08:58+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/effects_everywhere.rst:13
msgid "Effects everywhere"
msgstr "Ефекти повсюду"

#: ../../glossary/useful_information/effects_everywhere.rst:15
msgid ""
"Did you know that you can **apply effects** not only to clips in the "
"timeline, but also to **project bin clips** and even to specific **tracks in "
"the timeline**?"
msgstr ""
"Чи знали ви, що ви можете **застосовувати ефекти** не лише до кліпів на "
"монтажному столі, але і до **кліпів контейнера проєкту** і навіть до певних "
"**доріжок на монтажному столі**?"

#: ../../glossary/useful_information/effects_everywhere.rst:19
msgid "Clip Effects"
msgstr "Ефекти кліпу"

#: ../../glossary/useful_information/effects_everywhere.rst:23
msgid ""
"Probably most of the time, many Kdenlive users will simply slap **individual "
"effects on individual timeline clips**. For instance, as lighting conditions "
"vary *within* the same source clip, multiple scenes taken from it might be "
"in need of individual grading. Or you need to crop and place an individual "
"clip, separately from others. So we’re all used to it, and we do it almost "
"unconsciously."
msgstr ""
"Ймовірно, багато користувачів Kdenlive просто застосовують **окремі ефекти "
"до окремих кліпів на монтажному столі**. Наприклад, оскільки умови "
"освітлення змінювалися *у межах* того самого початкового кліпу, його сцени "
"можуть потребувати окремої обробки. Або вам потрібно обрізати і розташувати "
"певний кліп, окремо від інших. Отже, усі звикли до цього, і роблять це майже "
"підсвідомо."

#: ../../glossary/useful_information/effects_everywhere.rst:25
msgid ""
"But in some situations, we would like to ease and speed up our timeline "
"work. Instead of laboriously setting up effect after effect on individual "
"timeline clips, we want to add effects to either a specific bin clip or a "
"timeline track once and for all…"
msgstr ""
"Але іноді нам би було б бажаним скористатися простішим і швидшим способом "
"роботи з монтажним столом. Замість трудомісткого налаштовування ефекту за "
"ефектом для окремих кліпів на монтажному столі, нам хочеться додавати ефекти "
"або до окремого кліпу контейнера, або до цілої доріжки на монтажному столі "
"раз і назавжди…"

#: ../../glossary/useful_information/effects_everywhere.rst:28
msgid ""
"Did you know that you can temporarily :ref:`disable_all_timeline_effects`? "
"Use :menuselection:`Timeline --> Disable timeline effects`."
msgstr ""
"Чи знали ви, що ви можете тимчасово здійснити :ref:"
"`disable_all_timeline_effects`? Скористайтеся пунктом меню :menuselection:"
"`Монтажний стіл --> Вимкнути ефекти монтажного столу`."

#: ../../glossary/useful_information/effects_everywhere.rst:31
msgid "Effects on Project Bin Clips"
msgstr "Ефекти у кліпах контейнера проєкту"

#: ../../glossary/useful_information/effects_everywhere.rst:35
msgid ""
"Effects on bin clips allow you to, for instance, color grade a clip itself. "
"All copies of it that you use in the timeline then will automatically use "
"these effects. Also, all changes you make to the bin clip will immediately "
"become effective on all copies in the timeline."
msgstr ""
"Додавання ефектів до кліпів на панелі контейнера надає вам змогу, наприклад, "
"змінити кольори самого кліпу. Усі його копії, якими ви скористалися на "
"монтажному столі, автоматично використовуватимуть визначені вами ефекти. "
"Крім того, усі зміни, які було внесено до кліпу контейнера, буде негайно "
"застосовано до усіх копій на монтажному столі."

#: ../../glossary/useful_information/effects_everywhere.rst:39
msgid ""
"Please note that bin clip effects are applied first, before any timeline "
"clip effects."
msgstr ""
"Будь ласка, зауважте, що ефекти кліпу контейнера застосовуються першими, до "
"будь-яких ефектів кліпу на монтажному столі."

#: ../../glossary/useful_information/effects_everywhere.rst:45
msgid "Apply Effects to Bin Clips"
msgstr "Застосування ефектів до кліпів контейнера"

#: ../../glossary/useful_information/effects_everywhere.rst:47
msgid ""
"To apply effects to a clip in the project bin in general, simply drag and "
"drop an effect from the effects pane **(1)** into your clip in the project "
"bin **(2)**. The parameters pane **(3)** then will switch to the effects "
"applied to this particular bin clip. Adjust as you like."
msgstr ""
"Щоб застосувати ефекти до кліпу на панелі контейнера проєкту загалом, просто "
"перетягніть ефект з панелі ефектів **(1)** і скиньте його на кліп на панелі "
"контейнера проєкту **(2)**. Після цього панель параметрів **(3)** "
"перемкнеться у режим ефекту, який застосовано до відповідного кліпу на "
"панелі контейнера проєкту. Скоригуйте параметри бажаним чином."

#: ../../glossary/useful_information/effects_everywhere.rst:49
msgid ""
"If you later need to return to the bin clip effects in order to edit them "
"again, simply select the clip in the project bin. The parameters pane "
"**(3)** then will automatically switch to your bin clip’s effect stack."
msgstr ""
"Якщо пізніше ви захочете повернутися до ефектів кліпу на панелі контейнера "
"проєкту, щоб внести зміни до параметрів, просто позначте пункт кліпу на "
"панелі контейнера проєкту. Після цього панель параметрів **(3)** буде "
"автоматично перемкнуто у режим стосу ефектів кліпу на панелі контейнера "
"проєкту."

#: ../../glossary/useful_information/effects_everywhere.rst:52
msgid "Compare Before/After Effects"
msgstr "Порівняння «До/Після ефекту»"

#: ../../glossary/useful_information/effects_everywhere.rst:54
msgid ""
"**Please note** the split compare button at the top of the parameters pane "
"**(3)**: when active, the **clip monitor (4)** shows your clip in a before/"
"after fashion:"
msgstr ""
"**Будь ласка, зверніть увагу** на кнопку порівняння з поділом зображення, "
"яку розташовано у верхній частині панелі параметрів **(3)**: якщо кнопку "
"активовано, на **моніторі кліпу (4)** буде показано кліп у режимі «до/після»:"

#: ../../glossary/useful_information/effects_everywhere.rst:56
msgid ""
"**left half** of clip monitor **(4)**: your bin clip with all effects "
"applied; the text «effect» to the left of the red divider **(4)** is a "
"reminder of which side is showing effects, and which side is without effects."
msgstr ""
"**ліва половина** монітора кліпу **(4)**: ваш кліп контейнера із "
"застосуванням усіх ефектів; напис «ефект» ліворуч від червоної лінії-"
"роздільника **(4)** є нагадуванням про те, до якої частини застосовано "
"ефекти, а яка є первинним зображенням."

#: ../../glossary/useful_information/effects_everywhere.rst:57
msgid "**right half**: your bin clip **without any effects applied**."
msgstr "**права половина**: ваш кліп контейнера **без застосування ефектів**."

#: ../../glossary/useful_information/effects_everywhere.rst:59
msgid ""
"While hovering your mouse cursor over the clip monitor, you should notice a "
"red vertical divider line appearing. Drag it to dynamically change the split "
"between the clip parts with/without effects."
msgstr ""
"Якщо ви наведете вказівник миші на монітор кліпу, ви помітите появу червоної "
"вертикальної лінії-роздільника. Перетягніть її для динамічної зміни поділу "
"кадру на області із застосуванням ефектів та без їхнього застосування."

#: ../../glossary/useful_information/effects_everywhere.rst:62
msgid "Temporarily Disable Bin Clip Effects"
msgstr "Тимчасове вимикання ефектів кліпів контейнера"

#: ../../glossary/useful_information/effects_everywhere.rst:64
msgid ""
"You can (temporarily) **disable all effects on a single bin clip**, by "
"selecting it and then **un-checking** the **Bin effects for…** box at the "
"top of the parameters pane **(3)**. This works exactly the same as with "
"effects applied to timeline clips."
msgstr ""
"Ви можете (тимчасово) **вимкнути усі ефекти для окремого кліпу контейнера**: "
"позначте кліп на панелі контейнера, а потім **зніміть позначку** з пункту "
"**Ефекти контейнера для…** у верхній частині панелі параметрів **(3)**. Це "
"працює точно так само, як і з ефектами, які застосовано до кліпів на "
"монтажному столі."

#: ../../glossary/useful_information/effects_everywhere.rst:73
msgid ""
"Bin clips that have effects directly applied on them show the effects "
"signet: a star. It is overlaid on the clip thumbnail, as you can see to the "
"right."
msgstr ""
"Пункти кліпів контейнера, до яких безпосередньо застосовано ефекти, матимуть "
"позначку — зірку. Вона накладається на мініатюру кліпу, як ви можете бачити "
"праворуч."

#: ../../glossary/useful_information/effects_everywhere.rst:78
msgid "Temporarily disable ALL bin effects"
msgstr "Тимчасове вимикання усіх ефектів контейнера"

#: ../../glossary/useful_information/effects_everywhere.rst:80
msgid ""
"You can also temporarily disable all bin effects at once, using either a "
"keyboard shortcut or a toolbar button. Unfortunately, this function is "
"slightly hidden, as there is (yet) no menu item for it. You’ll need to "
"either configure a shortcut for this action, or add the action to a toolbar:"
msgstr ""
"Ви також можете тимчасово вимкнути усі ефекти контейнера одразу. Для цього "
"можна скористатися або клавіатурним скороченням, або кнопкою на панелі "
"інструментів. На жаль, цю можливість дещо приховано, оскільки (ще) немає "
"відповідного пункту меню. Вам доведеться або налаштувати клавіатурне "
"скорочення для цієї дії, або додати кнопку дії на панель інструментів:"

#: ../../glossary/useful_information/effects_everywhere.rst:82
msgid ""
"To configure a keyboard shortcut, go to :menuselection:`Settings --> "
"Configure Shortcuts…`, then search for :guilabel:`Disable Bin Effects`. Now "
"set your desired shortcut, click :guilabel:`OK`. Done."
msgstr ""
"Щоб налаштувати клавіатурне скорочення, скористайтеся пунктом меню :"
"menuselection:`Параметри --> Налаштувати скорочення…`, потім виконайте пошук "
"пункту :guilabel:`Вимкнути ефекти контейнера`. Далі, вкажіть бажану для вас "
"комбінацію клавіш, натисніть кнопку :guilabel:`Гаразд`. Усе."

#: ../../glossary/useful_information/effects_everywhere.rst:83
msgid ""
"Alternatively, go to :menuselection:`Settings --> Configure Toolbars…`, then "
"search for the available action :guilabel:`Disable Bin Effects`. Add it to "
"whatever toolbar you like, such as the **Timeline Toolbar** by clicking the :"
"guilabel:`>` button. Click :guilabel:`OK`. Done."
msgstr ""
"Крім того, ви можете скористатися пунктом меню :menuselection:`Параметри --> "
"Налаштувати пенали…`, потім виконайте пошук доступної дії :guilabel:"
"`Вимкнути ефекти контейнера`. Додайте кнопку дії на будь-яку бажану панель "
"інструментів, зокрема **панель інструментів «Монтажний стіл»**, натисканням "
"кнопки :guilabel:`>`. Натисніть кнопку :guilabel:`Гаразд`. Усе."

#: ../../glossary/useful_information/effects_everywhere.rst:85
msgid ""
"You can now quickly disable and enable all bin effects at once using either "
"the shortcut or toolbar button you’ve configured above."
msgstr ""
"Після додавання скорочення або кнопки ви зможете швидко вмикати або вимикати "
"усі ефекти контейнера одночасно."

#: ../../glossary/useful_information/effects_everywhere.rst:89
msgid "Effects on Tracks"
msgstr "Ефекти на доріжках"

#: ../../glossary/useful_information/effects_everywhere.rst:91
msgid ""
"Similar to effects on bin clips, you can also add effects to a specific "
"timeline track. For instance, you can set the crop and placement of clips on "
"a specific track, so you don’t need to copy these settings over and over "
"again onto all clips in this track. When you change a track effects, it "
"immediately applies to all clips on this track. Sweet."
msgstr ""
"Подібно до ефектів для кліпів контейнера, ви можете також додавати ефекти до "
"певної доріжки монтажного столу. Наприклад, ви можете встановити обрізання і "
"розташування кліпів на певній доріжці, щоб не копіювати ці параметри для "
"кожного з кліпів на цій доріжці. Коли ви змінюватимете ефекти доріжки, зміни "
"негайно буде застосовано до усіх кліпів на цій доріжці. Чудово."

#: ../../glossary/useful_information/effects_everywhere.rst:97
msgid "Apply Effects to Tracks"
msgstr "Застосування ефектів до доріжок"

#: ../../glossary/useful_information/effects_everywhere.rst:99
msgid ""
"To apply effects to a track in the timeline, simply drag and drop an effect "
"from the effects pane **(1)** into the desired track in your timeline "
"**(2)**. The parameters pane **(3)** then will switch to the effects applied "
"to this track. Adjust effects as you like."
msgstr ""
"Щоб застосувати ефекти до доріжки на монтажному столі, просто перетягніть "
"пункт ефекту з панелі ефектів **(1)** і скиньте його на бажану доріжку на "
"монтажному столі **(2)**. Після цього панель параметрів **(3)** буде "
"перемкнуто у режим ефектів, які застосовано до цієї доріжки. Скоригуйте "
"параметри ефекту бажаним чином."

#: ../../glossary/useful_information/effects_everywhere.rst:101
msgid ""
"There’s one minor catch here: the split compare button unfortunately doesn’t "
"work here, as it applies to individual clips only. It doesn’t work for "
"timeline tracks."
msgstr ""
"Тут є один невеличкий недолік: на жаль, кнопка режиму розділеного порівняння "
"тут не працює, оскільки вона стосується лише окремих кліпів. Кнопка не "
"працює для доріжок на монтажному столі."

#: ../../glossary/useful_information/effects_everywhere.rst:103
msgid ""
"If you later need to return to track effects in order to edit them again, "
"simply click into the header of the desired track. The parameters pane "
"**(3)** then will automatically switch to your track effect stack."
msgstr ""
"Якщо пізніше ви захочете повернутися до ефектів доріжки, щоб внести зміни до "
"параметрів, просто клацніть на заголовку бажаної доріжки. Після цього панель "
"параметрів **(3)** буде автоматично перемкнуто у режим стосу ефектів доріжки."

#: ../../glossary/useful_information/effects_everywhere.rst:106
msgid "Temporarily Disable Track Effects"
msgstr "Тимчасове вимикання ефектів доріжки"

#: ../../glossary/useful_information/effects_everywhere.rst:108
msgid ""
"You can (temporarily) **disable all effects for a track**, by clicking into "
"the track header and then **un-checking** the **Bin effects for…** box at "
"the top of the parameters pane **(3)**. This works exactly the same as with "
"effects applied to timeline clips."
msgstr ""
"Ви можете (тимчасово) **вимкнути усі ефекти для доріжки**: клацніть на "
"заголовку доріжки, а потім **зніміть позначку** з пункту **Ефекти контейнера "
"для…** у верхній частині панелі параметрів **(3)**. Це працює точно так "
"само, як і з ефектами, які застосовано до кліпів на монтажному столі."

#: ../../glossary/useful_information/effects_everywhere.rst:117
msgid ""
"Timeline tracks that have effects directly applied on them show the (usual) "
"effects signet: a star. It shows up after the clip title. In single-line "
"layout, the effects signet shows up in between the clip title and the track "
"controls."
msgstr ""
"Пункти доріжок, до яких безпосередньо застосовано ефекти, матимуть "
"(звичайну) позначку ефектів — зірку. Позначку буде показано після заголовка "
"кліпу. В однорядковому компонуванні позначку ефектів буде показано між "
"заголовком кліпу і кнопками керування доріжкою."
