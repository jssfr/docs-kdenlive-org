# Translation of docs_kdenlive_org_glossary___introducing_scopes___histogram_working.po to Catalan
# Copyright (C) 2022-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-04 00:39+0000\n"
"PO-Revision-Date: 2023-05-15 19:17+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/introducing_scopes/histogram_working.rst:13
msgid "How the Histogram works"
msgstr "Com funcionen els histogrames"

#: ../../glossary/introducing_scopes/histogram_working.rst:15
msgid "*Submitted by Granjow on Mon, 08/30/2010 - 23:10*"
msgstr "*Enviat per Granjow el dilluns, 30/08/2010 - 23:10*"

#: ../../glossary/introducing_scopes/histogram_working.rst:17
msgid ""
"When the Histogram receives an updated image from one of the monitors, each "
"of these pixels consist of a Red, Green, and Blue component. Each of these "
"values lies within a range of 0 and 255, which are the numbers you can "
"represent with one Byte. 0 means that the component is not shining at all (i."
"e. it is black), 255 means that it is shining as bright as possible."
msgstr ""
"Quan l'histograma rep una imatge actualitzada d'un dels monitors, cadascun "
"d'aquests píxels consisteix en un component vermell, verd i blau. Cada un "
"d'aquests valors es troba dins d'un interval entre 0 i 255, que són els "
"nombres que es poden representar amb un byte. 0 significa que el component "
"no està brillant en absolut (és a dir, és negre), 255 significa que està "
"brillant el més potent possible."

#: ../../glossary/introducing_scopes/histogram_working.rst:19
msgid ""
"The Histogram is merely statistics; it shows how often a component of a "
"certain brightness occurs. So what the Histogram then does is actually quite "
"simple:"
msgstr ""
"L'histograma només és estadística; mostra amb quina freqüència es produeix "
"un component d'una certa brillantor. Així que el que fa l'histograma llavors "
"és bastant senzill:"

#: ../../glossary/introducing_scopes/histogram_working.rst:21
msgid "Take the first pixel"
msgstr "Pren el primer píxel"

#: ../../glossary/introducing_scopes/histogram_working.rst:22
msgid ""
"Look at the Red value (= x) of the pixel. Increase the height of the bar at "
"position x of the histogram by 1. Example: If the red value is 0, increase "
"the height of the bar at position 0 (that is at the very left) of the "
"histogram by 1. If it is 42, increase bar 42 by 1. And so on."
msgstr ""
"Mira el valor vermell (= x) del píxel. Augmenta l'alçada de la barra a la "
"posició x de l'histograma per 1. Exemple: si el valor vermell és 0, augmenta "
"l'alçada de la barra a la posició 0 (és a dir, a l'extrem esquerre) de "
"l'histograma en 1. Si és 42, incrementa la barra 42 en 1. I així "
"successivament."

#: ../../glossary/introducing_scopes/histogram_working.rst:23
msgid "Repeat the previous step with Green and Blue."
msgstr "Repeteix el pas anterior amb el verd i el blau."

#: ../../glossary/introducing_scopes/histogram_working.rst:24
msgid ""
"Look at R, G, and B together and calculate the Luma value. Luma is the "
"perceived Luminance of this pixel. See further below how it is calculated "
"(if you are interested)."
msgstr ""
"Mira el vermell (R), el verd (G) i el blau (B) junts i calcula el valor de "
"la luma. La luma és la luminància percebuda d'aquest píxel. Vegeu més avall "
"com es calcula (si hi ha interès)."

#: ../../glossary/introducing_scopes/histogram_working.rst:25
msgid "Repeat these steps for all other pixels on the image."
msgstr "Repeteix aquests passos per a tots els altres píxels de la imatge."

#: ../../glossary/introducing_scopes/histogram_working.rst:28
msgid "What the Histogram shows"
msgstr "Què mostra l'histograma"

#: ../../glossary/introducing_scopes/histogram_working.rst:30
msgid ""
"The Histogram only shows the distribution of the luminance of the selected "
"components – nothing more, nothing less. Also when looking at the RGB "
"channels separately, instead of at the calculated Luma component only, you "
"cannot really guess the colors in the image."
msgstr ""
"L'histograma només mostra la distribució de la luminància dels components "
"seleccionats, ni més ni menys. També quan es miren els canals RGB per "
"separat, en comptes de només el component calculat de la luma, no es poden "
"endevinar els colors de la imatge."

#: ../../glossary/introducing_scopes/histogram_working.rst:32
msgid "Really? Yes. Take a look at these two images."
msgstr "De debò? Sí. Mireu aquestes dues imatges."

#: ../../glossary/introducing_scopes/histogram_working.rst:40
msgid ""
"Exactly the same Histogram. Totally different colors. (What you can do is "
"guessing the color tone; see below.) But what is the histogram good for now?"
msgstr ""
"Exactament el mateix histograma. Colors totalment diferents. El que podeu "
"fer és endevinar el to del color; vegeu més endavant. Però, quin és "
"l'histograma bo per ara?"

# skip-rule: t-acc_obe
#: ../../glossary/introducing_scopes/histogram_working.rst:42
msgid ""
"To answer this question, I would like to point an article from the "
"«Cambridge in Colour»: `Understanding Digital Camera Histograms: Tones and "
"Contrast <https://www.cambridgeincolour.com/tutorials/histograms1.htm>`_ and "
"the second part `Luminance & Color <https://www.cambridgeincolour.com/"
"tutorials/histograms2.htm>`_. Although written for digital photo cameras, "
"exactly the same applies for digital video cameras. Both articles are easy "
"to read and understand (and may also be of interest for experienced users)."
msgstr ""
"Per a respondre a aquesta pregunta, m'agradaria assenyalar un article de "
"«Cambridge in Colour»: `Entenent els histogrames de les càmeres digitals: "
"tons i contrast <https://www.cambridgeincolour.com/tutorials/histograms1."
"htm>`_ i la segona part `Luminància i color <https://www.cambridgeincolour."
"com/tutorials/histograms2.htm>`_. Encara que està escrit per a càmeres "
"fotogràfiques digitals, el mateix s'aplica a les càmeres de vídeo digitals. "
"Ambdós articles són fàcils de llegir i entendre (i també poden ser d'interès "
"per a usuaris experimentats)."

#: ../../glossary/introducing_scopes/histogram_working.rst:45
msgid "Histogram example: Candlelight"
msgstr "Exemple d'histograma: llum d'espelma"

#: ../../glossary/introducing_scopes/histogram_working.rst:50
msgid "Two special things about this histogram."
msgstr "Dues coses especials sobre aquest histograma."

#: ../../glossary/introducing_scopes/histogram_working.rst:52
msgid ""
"Most pixels are dark, according to the Luma component (white). Though there "
"is no total black: Notice that the Luma component shows «min: 8». "
"Nevertheless, the blue component does reach 0. This means that the darkest "
"pixels are still slightly orange and didn't lose all color information yet."
msgstr ""
"La majoria dels píxels són foscos, segons el component de la luma (blanc). "
"Tot i que no hi ha cap negre total: tingueu en compte que el component de la "
"luma mostra «min: 8». No obstant això, el component blau arriba a 0. Això "
"significa que els píxels més foscos encara són lleugerament taronja i encara "
"no han perdut tota la informació de color."

#: ../../glossary/introducing_scopes/histogram_working.rst:54
msgid ""
"There is quite some clipping. A lot of R values are sticking at the very "
"right, at 255. Having a peak at 255 usually means that we lost information "
"because some regions were too bright for the camera sensor with the current "
"sensitivity settings. This could have been solved by lowering the "
"sensitivity, but then the book and nearly everything else would be black. In "
"this case the candles cause the clipping. (Not too bad here, because the "
"lost detail isn't important for the image.)"
msgstr ""
"Hi ha força retallats. Molts valors del vermell es mantenen a la dreta, a "
"255. Tenir un pic a 255 normalment significa que hem perdut informació "
"perquè algunes regions eren massa brillants per al sensor de la càmera amb "
"la configuració de sensibilitat actual. Això podria haver-se resolt baixant "
"la sensibilitat, però llavors el llibre i gairebé tota la resta seria negre. "
"En aquest cas, les espelmes causen la retallada. No és gaire dolent aquí, "
"perquè el detall perdut no és important per a la imatge."

#: ../../glossary/introducing_scopes/histogram_working.rst:56
msgid ""
"The RGB components also show very well that the shadows are not neutral grey "
"but orange, otherwise the color heaps on the left would, as in the gradient "
"histogram above, have their center at the same position. There isn’t a lot "
"to correct here, what could be done is raising the shadows with a Curves "
"effect, but this is a matter of taste and the intended mood for the final "
"movie."
msgstr ""
"Els components RGB també mostren molt bé que les ombres no són de color gris "
"neutre sinó de color taronja, en cas contrari els munts de color de "
"l'esquerra, com en l'histograma de degradat de dalt, tindrien el seu centre "
"a la mateixa posició. Aquí no hi ha gaire a corregir, el que es podria fer "
"és elevar les ombres amb un efecte de corbes, però és una qüestió de gust i "
"l'estat d'ànim de la pel·lícula final."

#: ../../glossary/introducing_scopes/histogram_working.rst:62
msgid "Histogram example: Underexposed ABC"
msgstr "Exemple d'histograma: ABC subexposat"

#: ../../glossary/introducing_scopes/histogram_working.rst:67
msgid "We immediately notice two things:"
msgstr "Observem immediatament dues coses:"

#: ../../glossary/introducing_scopes/histogram_working.rst:69
msgid ""
"The RGB peaks are at the same position, near the middle. The white wall is "
"the brightest part, so this peaks are from the white wall. As they are not "
"shifted, the white balance should be okay (the image confirms that). Note "
"that the Histogram is not very accurate for white balance. Later I will "
"introduce a much more accurate scope."
msgstr ""
"Els pics RGB estan a la mateixa posició, prop del mig. La paret blanca és la "
"part més brillant, de manera que aquests pics són de la paret blanca. Com "
"que no es desplacen, el balanç de blancs hauria d'estar bé (la imatge ho "
"confirma). Tingueu en compte que l'histograma no és molt precís per al "
"balanç de blancs. Més endavant presentaré un electroscopi molt més precís."

#: ../../glossary/introducing_scopes/histogram_working.rst:71
msgid ""
"The image is too dark. The brightest component, red, only reaches a value of "
"170. The white wall is actually grey."
msgstr ""
"La imatge és massa fosca. El component més brillant, el vermell, només "
"arriba a un valor de 170. La paret blanca de fet és grisa."

#: ../../glossary/introducing_scopes/histogram_working.rst:73
msgid ""
"Monitoring correct exposure is the Histogram's strength! The exposure can be "
"corrected with curves as well, but this time I will use the *Levels effect*."
msgstr ""
"Vigilar l'exposició correcta és la força de l'histograma! L'exposició també "
"es pot corregir amb corbes, però aquesta vegada utilitzarà l'efecte "
"*Nivells*."

#: ../../glossary/introducing_scopes/histogram_working.rst:78
msgid ""
"I've lowered the Input white level of the Luma channel until one of the RGB "
"components reached 255. Lowering the input white level further would cause "
"clipping on the wall and lost image information. (Which may be desired in "
"certain circumstances!)"
msgstr ""
"He baixat el balanç de blancs d'entrada del canal de Luma fins que un dels "
"components RGB ha arribat a 255. Abaixar encara més el balanç de blancs "
"d'entrada provocaria la retallada a la paret i a la pèrdua d'informació de "
"la imatge. Que podria ser desitjat en determinades circumstàncies!"

#: ../../glossary/introducing_scopes/histogram_working.rst:80
msgid "This process is called *Stretching* of the tonal range."
msgstr "Aquest procés s'anomena *estirament* de l'interval tonal."

#: ../../glossary/introducing_scopes/histogram_working.rst:83
msgid "Histogram options"
msgstr "Opcions dels histogrames"

#: ../../glossary/introducing_scopes/histogram_working.rst:85
msgid "In kdenlive 0.7.8 the histogram can be adjusted as follows:"
msgstr "En el Kdenlive 0.7.8 l'histograma es pot ajustar de la manera següent:"

#: ../../glossary/introducing_scopes/histogram_working.rst:87
msgid ""
"Components – They can be enabled individually. For example, you might only "
"want to see the Luma component, or you want to hide the Sum display."
msgstr ""
"Components: es poden activar individualment. Per exemple, és possible que "
"només vulgueu veure el component luma, o que vulgueu ocultar la pantalla Sum."

#: ../../glossary/introducing_scopes/histogram_working.rst:89
msgid ""
"Y or Luma is the best known Histogram. Every digital camera shows it, "
"digikam, GIMP, etc. know it. See below how it is calculated."
msgstr ""
"Y o Luma és l'histograma més conegut. Totes les càmeres digitals el mostren, "
"el digiKam, el GIMP, etc. el coneixen. Vegeu a continuació com es calcula."

#: ../../glossary/introducing_scopes/histogram_working.rst:91
msgid ""
"Sum is basically a quick overview over the individual RGB channels. If it "
"shows e.g. 5 as the minimum value, you know that none of the RGB components "
"goes lower than 5."
msgstr ""
"Sum és bàsicament una visió ràpida dels canals RGB individuals. Si es mostra "
"com a valor mínim, per exemple 5, s'ha de saber que cap dels components RGB "
"és inferior a 5."

#: ../../glossary/introducing_scopes/histogram_working.rst:93
msgid "RGB show the Histogram for the individual channels."
msgstr "RGB mostra l'histograma per als canals individuals."

#: ../../glossary/introducing_scopes/histogram_working.rst:95
msgid ""
"Unscaled (Context menu) – Does not scale the width of the histogram (unless "
"the widget size is smaller). Just a goodie if you want to have it 256 px "
"wide."
msgstr ""
"Sense escalar (menú contextual): no escala l'amplada de l'histograma (tret "
"que la mida del giny sigui més petita). Només és bo si el voleu tenir amb "
"una amplada de 256 px."

#: ../../glossary/introducing_scopes/histogram_working.rst:97
msgid ""
"Luma mode (Context menu) – This option defines how the Luma value of a pixel "
"is calculated. Two options are available:"
msgstr ""
"Mode de luma (menú contextual): aquesta opció defineix com es calcula el "
"valor de la luma d'un píxel. Hi ha dues opcions disponibles:"

# skip-rule: t-acc_obe, t-apo_fin
#: ../../glossary/introducing_scopes/histogram_working.rst:99
msgid "Rec. 601 uses the formula ``Y' = 0.299 R' + 0.587 G' + 0.114 B'``"
msgstr "Rec. 601 empra la fórmula ``Y' = 0,299 R' + 0,587 G' + 0,114 B'``"

# skip-rule: t-acc_obe, t-apo_fin
#: ../../glossary/introducing_scopes/histogram_working.rst:101
msgid "Rec. 709 uses ``Y' = 0.2126 R' + 0.7152 G' + 0.0722 B'``"
msgstr "Rec. 709 empra ``Y' = 0,2126 R' + 0,7152 G' + 0,0722 B'``"

#: ../../glossary/introducing_scopes/histogram_working.rst:103
msgid ""
"Most of the time you will want to use Rec. 709 which is, as far as I know, "
"mostly used in digital video today."
msgstr ""
"La majoria de les vegades voldreu utilitzar la rec. 709 que, segons tinc "
"entès, s'usa majoritàriament en vídeo digital avui en dia."

#: ../../glossary/introducing_scopes/histogram_working.rst:106
msgid "Sample files"
msgstr "Fitxers d'exemple"

#: ../../glossary/introducing_scopes/histogram_working.rst:108
msgid "The sample files used above can be downloaded here:"
msgstr "Els fitxers utilitzats més amunt es poden baixar aquí:"

# skip-rule: t-acc_obe
#: ../../glossary/introducing_scopes/histogram_working.rst:110
msgid ""
"`Histogram-bw.png <http://granjow.net/uploads/kdenlive/samples/Histogram-bw."
"png>`_"
msgstr ""
"`Histogram-bw.png <http://granjow.net/uploads/kdenlive/samples/Histogram-bw."
"png>`_"

# skip-rule: t-acc_obe
#: ../../glossary/introducing_scopes/histogram_working.rst:112
msgid ""
"`Histogram-col.png <http://granjow.net/uploads/kdenlive/samples/Histogram-"
"col.png>`_"
msgstr ""
"`Histogram-col.png <http://granjow.net/uploads/kdenlive/samples/Histogram-"
"col.png>`_"

# skip-rule: t-acc_obe
#: ../../glossary/introducing_scopes/histogram_working.rst:114
msgid ""
"`abc-underexposed.avi <http://granjow.net/uploads/kdenlive/samples/abc-"
"underexposed.avi>`_ (26 MB; 720/24p)"
msgstr ""
"`abc-underexposed.avi <http://granjow.net/uploads/kdenlive/samples/abc-"
"underexposed.avi>`_ (26 MB; 720/24p)"

# skip-rule: t-acc_obe
#: ../../glossary/introducing_scopes/histogram_working.rst:116
msgid ""
"`candlelight.avi <http://granjow.net/uploads/kdenlive/samples/candlelight."
"avi>`_ (14 MB; 720/24p)"
msgstr ""
"`candlelight.avi <http://granjow.net/uploads/kdenlive/samples/candlelight."
"avi>`_ (14 MB; 720/24p)"

#: ../../glossary/introducing_scopes/histogram_working.rst:119
msgid "Summary"
msgstr "Resum"

#: ../../glossary/introducing_scopes/histogram_working.rst:125
msgid ""
"The Histogram is a great tool for exposure correction, together with the "
"Curves and the Levels effects. It helps to avoid clipping (burned out areas) "
"and crushed blacks (the opposite) when applying effects."
msgstr ""
"L'histograma és una gran eina per a la correcció de l'exposició, juntament "
"amb els efectes de corbes i de nivells. Ajuda a evitar la retallada (àrees "
"cremades) i els negres aixafats (el contrari) en aplicar efectes."

#: ../../glossary/introducing_scopes/histogram_working.rst:127
msgid ""
"Thanks for reading! Continue with the :ref:`Waveform and the RGB Parade "
"<waveform_and_RGB_parade>`."
msgstr ""
"Gràcies per llegir-ho! Continueu amb la :ref:`Forma d'ona i l'histograma RGB "
"<waveform_and_RGB_parade>`."

#: ../../glossary/introducing_scopes/histogram_working.rst:129
msgid "Please drop your comments below."
msgstr "Poseu els vostres comentaris a continuació."

#: ../../glossary/introducing_scopes/histogram_working.rst:131
msgid "Simon A. Eugster (Granjow)"
msgstr "Simon A. Eugster (Granjow)"
