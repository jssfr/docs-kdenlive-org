# Spanish translations for docs_kdenlive_org_importing_and_assets_management___capturing.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_importing_and_assets_management___capturing\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2021-11-14 04:34+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../importing_and_assets_management/capturing.rst:22
msgid "Capturing Video"
msgstr "Captura de vídeo"

#: ../../importing_and_assets_management/capturing.rst:27
msgid ""
"At least Firewire and webcam capture were removed in porting to KDE 5 due to "
"lack of manpower."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:30
msgid ""
"**Kdenlive** provides functionality for capturing video from external "
"devices; e.g., Firewire, FFmpeg, Screen Grab and Blackmagic."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:33
msgid ""
"You configure video capturing from :menuselection:`Settings --> Configure "
"Kdenlive --> Capture` (more on this :ref:`configure_kdenlive`)."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:36
msgid ""
"You define the destination location for your captures by using :"
"menuselection:`Settings --> Configure Kdenlive --> Environment --> Default "
"Folders` (more on this :ref:`configure_kdenlive`)."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:39
msgid ""
"To execute a video capture, select the :ref:`monitors` and choose the "
"capture device from the dropdown in the bottom right."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:48
msgid "Firewire"
msgstr "Firewire"

#: ../../importing_and_assets_management/capturing.rst:52
msgid ""
"This option is not available in recent versions of Kdenlive. Use dvgrab "
"directly in a terminal to capture video from firewire."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:55
msgid ""
"This captures video from sources connected via a firewire (also known as -  "
"IEEE 1394 High Speed Serial Bus) card and cable. This functionality uses the "
"`dvgrab <http://linux.die.net/man/1/dvgrab>`_ program and the settings for "
"this can be customized by clicking the spanner icon or choosing  :"
"menuselection:`Settings>Configure Kdenlive`.  See :ref:`configure_kdenlive`."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:58
msgid "To perform a capture:"
msgstr "Para realizar una captura:"

#: ../../importing_and_assets_management/capturing.rst:61
msgid "Plug in your device to the firewire card and turn it on to play mode"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:64
msgid "Click the *Connect Button*"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:70
msgid ""
"Click the Record Button – note it toggles to grey while you are recording"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:73
msgid ""
"Click the Record button again to stop capture. Or click the stop button."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:76
msgid "Once capturing is finished, click the disconnect button"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:83
msgid ""
"In the *Captured Files* dialog, click the import button to have the captured "
"files automatically imported into the project bin."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:93
msgid ""
"If your device does not start playing the source device when you click the "
"record button, you may have to start playback on your device manually and "
"then click record."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:97
msgid "FFmpeg"
msgstr "FFmpeg"

#: ../../importing_and_assets_management/capturing.rst:99
msgid ""
"I believe this captures video from an installed Web Cam using *Video4Linux2*."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:104
msgid "Screen Grab"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:106
msgid "This captures video of the PC screen."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:108
msgid "Open screen grab: :menuselection:`View --> Screen Grab`."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:110
msgid "Start recording: click the “record” button."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:113
msgid "Stop record: click the \"record\" button again."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:116
msgid "The recorded clip will be added in the project bin."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:119
msgid "Settings can be adjusted in :ref:`configure_kdenlive`"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:122
msgid ""
"To check on your linux distro, type ``ffmpeg -version`` in a terminal and "
"look for ``--enable-x11grab`` in the reported configuration info.  [1]_"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:125
msgid ""
"If you are capturing the screen and using the X246 with audio settings and "
"you get a crash as shown in the screen shot…"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:131
msgid ""
"…then consider creating a profile for audio capture where ``-acodec "
"pcm_s16le``  is replaced by ``-acodec libvorbis -b 320k``. See :ref:"
"`configure_kdenlive`."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:135
msgid "Blackmagic"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:139
msgid ""
"This is for capturing from Blackmagics `decklink <http://www.blackmagic-"
"design.com/uk/products/decklink/>`_ video capture cards (AFAIK). Not sure "
"how stable this code is at the moment. Mentioned in legacy Mantis bug "
"tracker ID 2130."
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:143
msgid "Footnotes"
msgstr ""

#: ../../importing_and_assets_management/capturing.rst:147
msgid ""
"There are now two branches of *ffmpeg*: a *Libav* branch and an ffmpeg.org "
"branch. The *ffmpeg* version from the latter branch reports the "
"configuration when you run with ``ffmpeg -version``. The *Libav* version "
"does not. So this method to check for the ``--enable-x11grab`` does not work "
"if you have the *Libav* version of *ffmpeg*."
msgstr ""

#~ msgid "Contents"
#~ msgstr "Contenido"
