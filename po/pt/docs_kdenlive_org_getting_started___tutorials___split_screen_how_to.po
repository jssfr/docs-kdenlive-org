# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-31 00:39+0000\n"
"PO-Revision-Date: 2022-05-22 12:57+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: YouTube ref tracks panandzoom PIP\n"

#: ../../getting_started/tutorials/split_screen_how_to.rst:12
msgid "How To Do Split Screen Effect"
msgstr "Como Fazer um Efeito de Ecrã Dividido"

#: ../../getting_started/tutorials/split_screen_how_to.rst:15
msgid "This effect is also known as picture in picture effect or PIP."
msgstr "Este efeito também é conhecido como 'imagem-na-imagem' ou PIP."

#: ../../getting_started/tutorials/split_screen_how_to.rst:18
msgid ""
"In this example we will see how to have 4 screens playing at once in the "
"video."
msgstr ""
"Neste exemplo iremos ver como ter 4 ecrãs a reproduzir ao mesmo tempo no "
"vídeo."

#: ../../getting_started/tutorials/split_screen_how_to.rst:21
msgid "`Example - 4 screens in one(YouTube) <https://youtu.be/YRs5UDuCVJg>`_"
msgstr "`Exemplo - 4 ecrãs num só(YouTube) <https://youtu.be/YRs5UDuCVJg>`_"

#: ../../getting_started/tutorials/split_screen_how_to.rst:24
msgid ""
"To create this effect you need 4 video tracks on the time line.  Add extra "
"tracks to the time line from the  :ref:`tracks` menu."
msgstr ""
"Para criar este efeito, precisa de 4 faixas de vídeo na linha temporal. "
"Adicione faixas extra na linha temporal com o menu :ref:`tracks`."

#: ../../getting_started/tutorials/split_screen_how_to.rst:27
msgid ""
"You create a project/timeline like that shown in Figure 1 and described in "
"detail below."
msgstr ""
"Irá criar um projecto/linha temporal como o que aparece na Figura 1 e está "
"descrito em detalhe em baixo."

#: ../../getting_started/tutorials/split_screen_how_to.rst:35
msgid "Time Line Setup"
msgstr "Configuração da Linha Temporal"

#: ../../getting_started/tutorials/split_screen_how_to.rst:39
msgid ""
"Video 1 (appears in bottom left in the result) has no effects added to it."
msgstr ""
"O Vídeo 1 (aparece no canto inferior esquerdo no resultado) não tem efeitos "
"adicionados a ele."

#: ../../getting_started/tutorials/split_screen_how_to.rst:47
msgid "Transition 1"
msgstr "Transição 1"

#: ../../getting_started/tutorials/split_screen_how_to.rst:56
msgid "Effect on Video 2"
msgstr "Efeito no Vídeo 2"

#: ../../getting_started/tutorials/split_screen_how_to.rst:58
#, fuzzy
#| msgid ""
#| "Video 2 (appears top left in result) has a :ref:`pan_and_zoom` effect "
#| "added to it (See Figure 3).  This effect has a sizing and positioning "
#| "effect as part of it that causes the Video on Video Track 2 to be scaled "
#| "down 50% and be positioned in the top left corner."
msgid ""
"Video 2 (appears top left in result) has a :ref:`effects-position_and_zoom` "
"effect added to it (See Figure 3).  This effect has a sizing and positioning "
"effect as part of it that causes the Video on Video Track 2 to be scaled "
"down 50% and be positioned in the top left corner."
msgstr ""
"O Vídeo 2 (que aparece no canto superior-esquerdo no resultado) tem um "
"efeito :ref:`pan_and_zoom` adicionado ao mesmo (Veja a Figura 3). Este "
"efeito inclui um efeito de dimensionamento e posicionamento como parte dele, "
"o que faz com que o vídeo na faixa 2 seja reduzido em 50% e fique "
"posicionado no canto superior esquerdo."

#: ../../getting_started/tutorials/split_screen_how_to.rst:67
msgid "Standard composite transition between Video track 2 and Video track 3."
msgstr ""
"Transição normal de composição entre a faixa de vídeo 2 e a faixa de vídeo 3."

#: ../../getting_started/tutorials/split_screen_how_to.rst:76
msgid "Effect on Video 3"
msgstr "Efeito no Vídeo 3"

#: ../../getting_started/tutorials/split_screen_how_to.rst:78
msgid ""
"Video 3 (appears top right in result) has a pan and zoom effect added to it "
"(See Figure 5).  This effect has a sizing and positioning effect as part of "
"it that causes the Video on Video Track 3 to be scaled down 50% and be "
"positioned in the top right corner."
msgstr ""
"O Vídeo 3 (que aparece no canto superior-direito no resultado) tem um efeito "
"de ampliação e deslocamento adicionado ao mesmo (Veja a Figura 5). Este "
"efeito inclui um efeito de dimensionamento e posicionamento como parte dele, "
"o que faz com que o vídeo na faixa 3 seja reduzido em 50% e fique "
"posicionado no canto superior direito."

#: ../../getting_started/tutorials/split_screen_how_to.rst:88
msgid "Standard composite transition between Video track 3 and Video track 4."
msgstr ""
"Transição normal de composição entre a faixa de vídeo 3 e a faixa de vídeo 4."

#: ../../getting_started/tutorials/split_screen_how_to.rst:98
msgid "Effect on Video 4"
msgstr "Efeito no Vídeo 4"

#: ../../getting_started/tutorials/split_screen_how_to.rst:100
msgid ""
"Video 4 (appears bottom right in result) has a pan and zoom effect added to "
"it (See Figure 7).  This effect has a sizing and positioning effect as part "
"of it that causes the Video on Video Track 4 to be scaled down 50% and be "
"positioned in the bottom right corner."
msgstr ""
"O Vídeo 4 (que aparece no canto inferior-direito no resultado) tem um efeito "
"de ampliação e deslocamento adicionado ao mesmo (Veja a Figura 7). Este "
"efeito inclui um efeito de dimensionamento e posicionamento como parte dele, "
"o que faz com que o vídeo na faixa 4 seja reduzido em 50% e fique "
"posicionado no canto inferior direito."

#~ msgid "Contents"
#~ msgstr "Conteúdo"
