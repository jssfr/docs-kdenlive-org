# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-31 00:39+0000\n"
"PO-Revision-Date: 2023-07-31 18:53+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:1
msgid "Project bin in Kdenlive video editor"
msgstr "Контейнер проєкту у редакторі відео Kdenlive"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:1
msgid ""
"KDE, Kdenlive, add clips, animation, color clip, image, title clip, online "
"resources, marker, editing, timeline, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, додавання кліпів, додати кліп, редагування кліпу, анімація, "
"кольоровий кліп, зображення, кліп титрів, інтернет-ресурси, позначка, "
"редагування, монтажний стіл, документація, підручник, відеоредактор, відео, "
"відкритий код, вільний, вивчення, вивчити, просто"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:27
msgid "Clips"
msgstr "Кліпи"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:29
msgid "See also :ref:`clip_menu`."
msgstr "Див. також :ref:`clip_menu`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:35
msgid "Clips (Video, Audio, Images and Animation)"
msgstr "Кліпи (відео, звук, зображення та анімація)"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:37
msgid ""
"The |kdenlive-add-clip| button (or :guilabel:`Add Clip or Folder...` or "
"double click into an empty place in the project bin) brings up the **Add "
"Clip** Dialog where you can choose video, audio, animation (*new in version "
"22.08*) or still image clips to add to :ref:`project_tree`."
msgstr ""
"Натискання кнопки |kdenlive-add-clip| (або пункт :guilabel:`Додати кліп або "
"теку...`, або подвійне клацання на порожньому місці у контейнері проєкту) "
"відкриває вікно **Додавання кліпу**, за допомогою якого ви можете вибрати "
"кліпи відео, звуку, анімацію (*нове у версії 22.08*) або стоп-кадри для "
"додавання на :ref:`панель контейнера проєкту <project_tree>`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:44
msgid ""
"The button |view-preview| labeled 1 toggles File Preview on and off (applies "
"to image files only)."
msgstr ""
"Кнопка |view-preview| із міткою 1 вмикає або вимикає попередній перегляд "
"вмісту файлів (застосовується лише до файлів зображень)."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:46
msgid "The slider labeled 2 adjusts the size of the preview icons."
msgstr "Повзунок із міткою 2 коригує розмір піктограм попереднього перегляду."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:48
msgid ""
"Checkbox :guilabel:`Import image sequence` (labeled 3) enables the import of "
"a series of images that can be used to make a stop motion animation."
msgstr ""
"Пункт :guilabel:`Імпортувати послідовність зображень`, який позначено цифрою "
"3, уможливлює імпортування послідовності зображень, якими можна скористатися "
"для створення анімації з стоп-кадрів."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:52
msgid ""
"Checkbox :guilabel:`Ignore subfolder structure` (labeled 4) enables import "
"video footage or audio recording folders while automatically ignoring any "
"sub-folder structures created by some devices, such as the Sony XDCam, "
"Panasonic P2, Canon camcorders or Zoom audio recorders."
msgstr ""
"Пункт :guilabel:`Ігнорувати структуру підтек` із міткою 4 вмикає "
"імпортування тек відеоматеріалу або звукових записів із автоматичним "
"ігноруванням усієї структури підтек, які створюються деякими пристроями, "
"зокрема Sony XDCam, Panasonic P2, записувачами відео Canon та записувачами "
"звуку Zoom."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:54
msgid ""
"You can add other types of clips by choosing a clip type from the menu "
"brought up from the drop down button next to the |kdenlive-add-clip| button."
msgstr ""
"Ви можете додавати кліпи інших типів, вибравши тип з меню, яке можна "
"відкрити за допомогою кнопки спадного меню кнопки |kdenlive-add-clip|."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:64
msgid "Color clips"
msgstr "Кольорові кліпи"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:66
msgid ""
":guilabel:`Add Color Clip...`: Color clips are images composed of a single "
"color that can be added to the Project Bin. They can be useful to provide a "
"background on which to place titles."
msgstr ""
":guilabel:`Додати кольоровий кліп...`: Кольорові кліпи — зображення, які "
"вкрито одним кольором. Їх можна додати до контейнера проєкту. Такі "
"зображення можуть бути корисними як тло, на якому розташовують титри."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:68
msgid ""
"Add color clips by choosing :menuselection:`Add Color Clip` from the drop "
"down button next to the |kdenlive-add-clip| button."
msgstr ""
"Додати кольоровий кліп можна за допомогою пункту :menuselection:`Додати "
"кольоровий кліп` спадного меню кнопки |kdenlive-add-clip|."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:70
msgid ""
"This brings up the **Color Clip** dialog from which you can choose a color "
"and a duration."
msgstr ""
"У відповідь програма відкриє вікно **Кольоровий кліп**, за допомогою якого "
"ви зможете вибрати колір і тривалість кліпу."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:77
msgid ""
"Clicking :menuselection:`OK` adds the clip to the project bin. The clip can "
"then be dragged to the timeline. The duration of the color clip can be "
"adjusted on the timeline."
msgstr ""
"Натискання кнопки :menuselection:`Гаразд` призводить до додавання кліпу на "
"панель контейнера проєкту. Потім кліп можна буде перетягнути на монтажний "
"стіл. Тривалість кольорового кліпу можна скоригувати на монтажному столі."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:86
msgid "Image Sequence clips"
msgstr "Кліпи з послідовності зображень"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:88
msgid ""
":guilabel:`Add Image Sequence`: Image sequence clips are clips created from "
"a series of still images. The feature can be used to make an animation from "
"a collection of still images or to create a slideshow of still images. To "
"create the former, use a short frame duration; to create the latter, use a "
"long frame duration."
msgstr ""
":guilabel:`Додати послідовність зображень`: Кліпи з послідовності зображень "
"— кліпи створені із послідовності стоп-кадрів. Цією можливістю можна "
"скористатися для створення анімації на основі збірки стоп-кадрів або "
"створення показу слайдів зі статичних зображень. Для створення першого "
"скористайтеся коротким часом показу кадру; для створення другого — довгим."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:95
msgid ""
":guilabel:`MIME Type`: When enabled then the images get imported in "
"ascending order. For example, if the first image is :file:`100_1697.jpg` "
"then the next will be :file:`100_1698.jpg`, etc."
msgstr ""
":guilabel:`Тип MIME`: якщо позначено, зображення буде імпортовано за "
"зростанням. Наприклад, якщо першим зображенням є :file:`100_1697.jpg`, "
"наступним буде :file:`100_1698.jpg`, тощо."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:97
msgid ""
":guilabel:`Filename pattern`:  When enabled you can point to the first image "
"you like to import. The remaining images get imported in ascending order."
msgstr ""
":guilabel:`Взірець назви файла`:  якщо позначено, ви можете вказати перше "
"зображення, яке ви хочете імпортувати. Решту зображень буде імпортовано за "
"зростанням."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:99
msgid ""
":guilabel:`Folder`: Browse to the location of the images which will make up "
"your image sequence and select the first image. The subsequent images that "
"are to be used in the slide show will be selected based on some sort of "
"filename algorithm that predicts what the next image file name should be."
msgstr ""
":guilabel:`Тека`: Перейдіть до теки зображень, з яких складатиметься ваша "
"послідовність, і виберіть перше зображення. Наступні зображення для показу "
"слайдів буде вибрано на основі певного алгоритму визначення назв, який "
"визначатиме назву файла, який має містити наступне зображення."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:101
msgid ""
":guilabel:`Frame Duration`: Select an appropriate frame duration – this "
"defines how long each image be displayed."
msgstr ""
":guilabel:`Тривалість кадру`: Виберіть належну тривалість кадру — це "
"визначає, наскільки довго буде показано кожне із зображень."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:103
msgid ""
":guilabel:`Loop`: When enabled you can lengthen the image sequence clip in "
"the timeline by dragging."
msgstr ""
":guilabel:`Цикл`: якщо позначено, ви можете продовжити послідовність кліпів "
"зображень на монтажному столі перетягуванням."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:105
msgid ""
":guilabel:`Center Crop`: When enabled it automatically fills the output "
"video frame with the images while maintaining their aspect ratio by zooming "
"the image and cropping equal amounts from each edge until can fill the full "
"frame. Without this option, the image will not be zoomed, but black bars "
"will appear when the photo orientation or aspect does not match the video's."
msgstr ""
":guilabel:`Обрізання за центром`: якщо позначено, автоматично заповнити "
"відеокадр результату зображенням, зберігаючи співвідношення розмірів шляхом "
"масштабування зображення і обрізання однакових смуг з кожного краю, аж доки "
"зображення не заповнить кадр. Якщо цей пункт не буде позначено, програма не "
"масштабуватиме зображення, лишаючи чорні смуги там, де орієнтація фотографії "
"або її співвідношення розмірів не збігатимуться із відповідними параметрами "
"відео."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:107
msgid ""
":guilabel:`Dissolve`: When enabled you can adjust the length of the dissolve "
"and choose the type of :guilabel:`Wipe` and adjust it with the :guilabel:"
"`Softness` slider."
msgstr ""
":guilabel:`Розчинити`: якщо позначено, ви можете скоригувати довжину "
"розчинення і вибрати тип :guilabel:`Витирання`, а також скоригувати його за "
"допомогою повзунка :guilabel:`М'якість`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:109
msgid ""
":guilabel:`Animation`: When enabled it adds preset slow smooth pan and zoom "
"effects also known as the Ken Burns Effect. You can choose no animation, "
"pans only, zooms only, or a combination of pans and zooms. Each option also "
"has a low pass filter to reduce the noise in the images that may occur "
"during this operation. Low pass filtering is much slower, so you should "
"preview without it, and then enable it to render."
msgstr ""
":guilabel:`Анімація`: якщо позначено, додає набір повільного плавного "
"панорамування та ефекти масштабування, також відомий як ефект Кена Бернса. "
"Ви можете вибрати варіант без анімації, лише панорамування, лише "
"масштабування або комбінацію панорамувань та змін масштабу. У кожного "
"варіант є фільтр низьких частот для зменшення шумності на зображеннях, яка "
"може виникнути під час виконання дій над зображенням. Фільтрування низьких "
"частот є набагато повільнішим, тому його слід виключити з попереднього "
"перегляду, а потім увімкнути під час остаточної обробки."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:111
msgid ""
"Then hit :guilabel:`OK`.  A video file made up of all the images in the "
"folder from which you selected the first frame file from will be added to "
"the project bin."
msgstr ""
"Потім натисніть кнопку :guilabel:`Гаразд`. На панель контейнера проєкту буде "
"додано відеофайл, який створено з усіх зображень у теці, з якої ви вибрали "
"файл першого кадру."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:113
msgid "You can then drag this video to the timeline."
msgstr "Згодом ви можете перетягнути це відео на монтажний стіл."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:115
msgid ""
"To edit the slideshow parameters: In the project bin either double-click the "
"slideshow or right-click and choose :guilabel:`Clip Properties`."
msgstr ""
"Для редагування параметрів показу слайдів виконайте такі дії: на панелі "
"контейнера проєкту двічі клацніть на пункті показу слайдів або клацніть на "
"ньому правою кнопкою миші і виберіть у контекстному меню пункт :guilabel:"
"`Властивості кліпу`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:119
msgid "Title clips"
msgstr "Кліпи з титрами"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:121
msgid "See :ref:`effects-titles`."
msgstr "Див. :ref:`effects-titles`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:127
msgid "Create Animation"
msgstr "Створити анімацію"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:131
msgid ""
":guilabel:`Create Animation...`: Creates a new animation. Give a name for "
"your new animation and it opens Glaxnimate and adds the newly created "
"animation to the project bin. You can then edit the animation in Glaxnimate "
"by right-click on the animation and choose :guilabel:`Edit Clip` and "
"whenever you save it, it is automatically updated in Kdenlive."
msgstr ""
":guilabel:`Створити анімацію...`: створює анімацію. Надайте новій анімації "
"назву, і її буде відкрито у Glaxnimate і додано до списку анімацій у "
"контейнері проєкту. Редагувати анімацію у Glaxnimate можна клацанням правою "
"кнопкою на пункті анімації з наступним вибором пункту контекстного меню :"
"guilabel:`Редагувати кліп`. Після збереження анімації її буде автоматично "
"оновлено у Kdenlive."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:133
msgid ""
"For editing animation clip in the timeline see :ref:`here <edit_an-"
"animation>`."
msgstr ""
"Редагування кліпу анімації на монтажному столі докладніше описано :ref:`тут "
"<edit_an-animation>`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:135
msgid ""
"Kdenlive supports `Json` (Lottie animations) and `rawr` (Glaxnimate "
"animation) animation."
msgstr ""
"У Kdenlive передбачено підтримку анімацій `Json` (анімацій Lottie) та `rawr` "
"(анімацій Glaxnimate)."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:137
msgid ""
"`Json` and `rawr` files contains an alpha channel so the imported animations "
"have a background which is not visible."
msgstr ""
"У файлах `Json` і `rawr` містяться дані щодо каналу прозорості, отже у "
"імпортованих анімацій буде невидиме тло."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:139
msgid ""
"If Glaxnimate is not installed on your computer following message pops-up:"
msgstr ""
"Якщо у вашій системі не встановлено Glaxnimate, буде показано таке "
"повідомлення:"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:144
msgid "For Glaxnimate installation see :ref:`here <default_apps>`."
msgstr ""
"Опис встановлення Glaxnimate наведено у розділі :ref:`here <default_apps>`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:150
msgid "Add Sequence"
msgstr "Додати послідовність"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:154
msgid ""
":guilabel:`Add Sequence...`: Adds a new sequence to the timeline. A sequence "
"behaves like a clip. Working with sequences see :ref:`here <sequence>`."
msgstr ""
"Пункт :guilabel:`Додати послідовність...`: додає нову послідовність на "
"монтажний стіл. Послідовність поводить себе як кліп. Опис роботи із "
"послідовностями наведено :ref:`тут <sequence>`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:158
msgid "Online Resources"
msgstr "Інтернет-джерела"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:160
msgid "See :ref:`online_resources`."
msgstr "Див. :ref:`online_resources`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:167
msgid "Proxy clips"
msgstr "Проміжні кліпи"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:174
msgid ""
"*Proxy clips* create a lower-quality transcode of the original footage for "
"use in real-time rendering in the project monitor.  This allows for a "
"smoother editing experience even on slower computers with High Definition "
"footage.  When rendering, by default, the original quality footage is used "
"and not the proxy footage. For example, Video decoding of H.264 or H.265 "
"clips, requires a lot of computing power to decode and could cause playback "
"*stutter* when rendering effects in real time."
msgstr ""
"*Проміжні кліпи* — створені програмою низькоякісні перекодування початкового "
"матеріалу для використання у інтерактивній обробці у моніторі проєкту. Так "
"можна забезпечити плавніше редагування навіть на повільних комп'ютерах при "
"використанні матеріалу з високою роздільною здатністю. При обробці, типово, "
"буде використано матеріал початкової якості, а не матеріали проміжних "
"кліпів. Наприклад, декодування відео H.264 або H.265 потребує значних "
"обчислювальних потужностей; це може спричинити *затинання* відтворення при "
"обробці ефектів у реальному часі."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:176
msgid ""
"Proxy clips can be enabled/disabled for the current project in the Project "
"Settings (:menuselection:`Project --> Project Settings... --> Proxy tab --> "
"Enable Proxy Clips option`)."
msgstr ""
"Проміжні кліпи може бути вимкнено або увімкнено для поточного проєкту у "
"параметрах проєкту (:menuselection:`Проєкт --> Параметри проєкту --> вкладка "
"«Проміжні кліпи» --> пункт «Увімкнути проміжні кліпи»`)."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:178
msgid ""
"To enable proxy clips by default for new projects, go to :menuselection:"
"`Settings --> Configure Kdenlive... --> Proxy Clips page --> Enable Proxy "
"Clips option`. See also the :ref:`project_settings` page."
msgstr ""
"Щоб типово увімкнути проміжні кліпи для нових проєктів, скористайтеся "
"пунктом :menuselection:`Параметри --> Налаштувати Kdenlive --> вкладка "
"«Проміжні кліпи» --> «Увімкнути проміжні кліпи»`. Див. також сторінку :ref:"
"`project_settings`"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:186
msgid ""
"As soon as proxy clips are enabled, they can be generated for specific "
"project clips in the Project Bin widget via the context menu :menuselection:"
"`Proxy Clip`. After you select :menuselection:`Proxy Clip` for a clip, a job "
"will start to create the clip. You can view the progress of this job by "
"looking at the little gray progress bar that appears at the bottom of the "
"clip in the Project Bin – see picture. Clicking :menuselection:`Proxy Clip` "
"again disables the proxy for this clip."
msgstr ""
"Щойно буде увімкнено проміжні кліпи, їх можна буде створити для певних "
"кліпів проєкту на віджеті контейнера проєкту за допомогою контекстного меню :"
"menuselection:`Проміжний кліп`. Після вибору пункту :menuselection:"
"`Проміжний кліп` для кліпу програма розпочне виконання завдання зі створення "
"кліпу. Ви можете стежити за поступом цього завдання за маленькою сірою "
"смужкою поступу, яка з'явиться у нижній частині пункту кліпу на панелі "
"контейнера проєкту — див. знімок. Повторний вибір пункту :menuselection:"
"`Проміжний кліп` вимикає проміжний кліп для цього кліпу."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:188
msgid ""
"You can multi-select clips in the Project Bin and select :menuselection:"
"`Proxy Clip` to start a batch proxy clip generation job which will queue up "
"multiple proxy clip generation jobs."
msgstr ""
"Ви можете позначити декілька кліпів на панелі контейнера проєкту і "
"скористатися пунктом меню :menuselection:`Проміжний кліп`, щоб розпочати "
"пакетне створення проміжних кліпів із чергою завдань із декількох проміжних "
"кліпів."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:195
msgid ""
"Once the proxy clip creation has completed, the proxy clip will appear with "
"a **P** icon in the Project Bin."
msgstr ""
"Щойно створення проміжного кліпу буде завершено, проміжний кліп з'явиться із "
"позначкою **P** на панелі контейнера проєкту."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:197
msgid ""
":guilabel:`External proxy clips` When enabled it reads the proxy clips "
"generated by your video camera. More details see: :ref:"
"`using_camcorder_proxy_clips`."
msgstr ""
":guilabel:`Зовнішні проміжні кліпи`: якщо позначено, програма читатиме "
"проміжні кліпи, які створено вашою відеокамерою. Докладніше: :ref:"
"`using_camcorder_proxy_clips`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:199
msgid ""
"When rendering to the final output file, you can choose whether to use the "
"proxy clips as well. It is disabled by default , but for a quick rendering "
"preview it is useful."
msgstr ""
"Під час обробки до остаточного файла результатів ви також можете вибрати, чи "
"слід використовувати проміжні кліпи. Типово, цей пункт вимкнено, але для "
"пришвидшення створення попереднього варіанта він може виявитися корисним."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:205
msgid "Clip Properties"
msgstr "Властивості кліпу"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:207
msgid ""
"You can display and edit clip properties by selecting a clip in the :ref:"
"`project_tree` and choosing :menuselection:`Clip Properties` from the :"
"menuselection:`Project` menu or from the right-click menu. Or by turning on "
"the display of clip properties the :menuselection:`View` and enable :"
"guilabel:`Clip Properties`."
msgstr ""
"Ви можете переглядати і редагувати властивості кліпів: позначте кліп на :ref:"
"`панелі контейнера проєкту <project_tree>` і виберіть пункт :menuselection:"
"`Властивості кліпу` з меню :menuselection:`Проєкт` або контекстного меню. Ви "
"також можете увімкнути показ властивостей кліпу у меню :menuselection:"
"`Перегляд`, пункт :guilabel:`Властивості кліпу`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:211
msgid "File Info"
msgstr "Відомості про файл"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:216
msgid "The :guilabel:`File Info` tab displays information about the file."
msgstr ""
"На вкладці :guilabel:`Відомості щодо файла` буде показано відомості щодо "
"файла."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:220
msgid "Properties"
msgstr "Властивості"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:227
msgid ""
"The :menuselection:`Properties` tab displays advanced properties of the clip "
"where you can select a check box and then force the clip to take the "
"property you specify. For example, you can use :menuselection:`Aspect ratio` "
"to tell a clip that seems to have forgotten it was 16:9 ratio that it really "
"is 16:9 ratio."
msgstr ""
"На вкладці :menuselection:`Властивості` буде показано додаткові властивості "
"кліпу. Там ви можете позначити відповідний пункт і потім примусово "
"встановити значення властивості кліпу. Наприклад, ви можете скористатися "
"пунктом :menuselection:`Співвідношення розмірів`, щоб повідомити програмі, "
"що кліп для якого не встановлено співвідношення розмірів кадру 16:9, слід "
"встановити таке співвідношення."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:231
msgid "Advanced Clip property options are:"
msgstr "Додатковими властивостями кліпу є такі:"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:233
msgid ""
"Duration: Change the clip duration. If the duration is shorter than the clip "
"duration, then the clip is **cropped**. If the duration is bigger than the "
"clip duration, then the last image is repeated until the new duration is "
"over."
msgstr ""
"Тривалість: зміна тривалості відтворення кліпу. Якщо тривалість буде меншою "
"за тривалість відтворення матеріалу кліпу, кліп буде **обрізано**. Якщо "
"тривалість перевищує фактичну тривалість кліпу, останнє зображення кадру "
"буде повторено для доповнення тривалості відтворення до нової."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:235
msgid "Aspect ratio: Change the clip aspect."
msgstr "Співвідношення розмірів: змінити співвідношення розмірів кадру кліпу."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:237
msgid "Proxy clips: Enable a proxy clip for this clip. See :ref:`clips`."
msgstr ""
"Проміжні кліпи: увімкнути проміжний кліп для цього кліпу. Див. :ref:`clips`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:239
msgid ""
"Frame rate: Change the clip frame rate. See `Wikipedia Frame rate <https://"
"en.wikipedia.org/wiki/Frame_rate>`_."
msgstr ""
"Частота кадрів: змінити частоту кадрів кліпу. Див. `«частота кадрів» у "
"Вікіпедії <https://en.wikipedia.org/wiki/Frame_rate>`_."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:241
msgid "Scanning"
msgstr "Сканування"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:243
msgid "Field order"
msgstr "Порядок полів"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:245
msgid "Disable autorotate"
msgstr "Вимкнути автообертання"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:247
msgid "Threads"
msgstr "Потоки"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:249
msgid "Video stream"
msgstr "Відеопотік"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:251
msgid "Audio stream"
msgstr "Потік звукових даних"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:253
msgid "Colorspace"
msgstr "Простір кольорів"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:255
msgid ":ref:`full_luma`"
msgstr ":ref:`full_luma`"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:263
msgid "Audio properties"
msgstr "Властивості звуку"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:270
msgid "For each channel [1]_ you can adjust:"
msgstr "Для кожного каналу [1]_ ви можете скоригувати таке:"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:272
msgid ""
"Choose which audio channel [1]_ should be enabled or disabled. Rename with "
"double click."
msgstr ""
"Виберіть, які звукові канали [1]_ має бути увімкнено або вимкнено. Канали "
"можна перейменувати після подвійного клацання."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:273
msgid "Normalize the channel"
msgstr "Нормалізувати канал"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:274
msgid "Swap the channels"
msgstr "Поміняти місцями канали"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:275
msgid "Copy a channel on the other one"
msgstr "Копіювати канал на інший канал"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:276
msgid "Adjust the volume"
msgstr "Скоригувати гучність"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:278
msgid "for all channels:"
msgstr "для усіх каналів:"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:280
msgid "Adjust the synchronization time related to the video of the clip."
msgstr "Скоригувати синхронізацію часу відносно відео у кліпі."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:285
msgid ""
"There is a difference between audio streams and audio channels. Kdenlive "
"does display the different audio streams with a list. Consumer video camera "
"mostly have only 1 audio stream with i.e. 6 channels, like 5.1 audio. "
"Kdenlive does not allow manipulation of the audio channels."
msgstr ""
"Між звуковими потоками та звуковими каналами існують відмінності. Kdenlive "
"показує різні звукові потоки у форматі списку. У звичайних відеокамер, "
"здебільшого, лише 1 потік звукових даних, тобто 6 каналів, наприклад звук "
"5.1. У Kdenlive не передбачено обробки звукових каналів."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:290
msgid "Markers"
msgstr "Позначки"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:294
msgid ""
"Marker tab removed. Clip markers are shown in :menuselection:`View --> "
"Guides`. More details see :ref:`markers`"
msgstr ""
"Вкладку позначок вилучено. Позначки кліпів буде показано у відповідь на "
"вибір пункту меню :menuselection:`Перегляд --> Напрямні`. Докладніше у "
"розділі щодо :ref:`позначок <markers>`"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:306
msgid "Metadata"
msgstr "Метадані"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:308
msgid ""
"You expect this to show any meta data that is contained in the clip. Does "
"not appear to work."
msgstr ""
"Мало б бути показано усі метадані, які містяться у кліпі. Здається, не "
"працює."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:312
msgid "Analysis"
msgstr "Аналіз"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:318
msgid ""
"You can view and delete motion vector data that is associated with the clip "
"from here. This is data created by :ref:`effects-motion_tracker`"
msgstr ""
"Тут ви можете переглянути і вилучити рухомі векторні дані, які пов'язано із "
"кліпом. Це дані, які створено ефектом :ref:`effects-motion_tracker`"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:320
msgid ""
"Button 1 Will delete the selected analysis data, Button 2 will allow you to "
"export the data (semi colon delimited text file), Button 3 will allow you to "
"import analysis data."
msgstr ""
"Кнопка 1 вилучає позначені дані аналізу, кнопка 2 надає вам змогу "
"експортувати дані (у форматі текстового файла зі значеннями, які "
"відокремлено крапкою з комою), за допомогою кнопки 3 можна імпортувати дані "
"аналізу."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:326
msgid "Generators"
msgstr "Засоби створення"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:329
msgid "Counter"
msgstr "Лічильник"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:336
msgid ""
"This generates a counter timer clip in various formats which you can put "
"onto the timeline."
msgstr ""
"Створює кліп зі зворотним відліком у різних форматах. Цей кліп ви можете "
"розташувати на монтажному столі."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:338
msgid ""
"You can choose to have the clip count up by checking that option, otherwise "
"it will count down by default. The :guilabel:`No Background` option will "
"remove the background from the counter leaving only the grey background "
"without the lines."
msgstr ""
"Позначенням цього пункту ви можете наказати програмі вести відлік за "
"зростанням. Якщо пункт не буде позначено, відлік вестиметься за спаданням. "
"За допомогою пункту :guilabel:`Без тла` можна вилучити тло з лічильника, "
"лишивши лише сірий фон без ліній."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:340
msgid ""
"To change the size and position of the clip, you can add an effect to the "
"clip on the timeline such as the :ref:`effects-position_and_zoom` or the :"
"ref:`effects-transform`."
msgstr ""
"Щоб змінити розміри кадру і розташування кліпу, можете додати до кліпу на "
"монтажному столі ефект, зокрема ефект :ref:`effects-position_and_zoom` або :"
"ref:`effects-transform`."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:346
msgid "White Noise"
msgstr "Білий шум"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:353
msgid ""
"This generates a video noise clip – like the \"snow\" on an out-of-tune "
"analogue TV. In ver 17.04 it generates audio white noise as well as video "
"snow."
msgstr ""
"Створює відеокліп із шумом, подібним до «снігу» на неналаштованому "
"аналоговому телевізорі. Починаючи з версії 17.04, створює звуковий білий шум "
"разом із відеоснігом."

#: ../../importing_and_assets_management/projects_and_files/clips.rst:360
msgid "Color Bars"
msgstr "Кольорові смуги"

#: ../../importing_and_assets_management/projects_and_files/clips.rst:367
msgid ""
"This generator came in to **Kdenlive** around ver 17.04. Generates a color "
"test pattern of various types. Including PAL color bars, BBC color bars, EBU "
"color bars, SMPTE color bars, Philips PM5544, FuBK"
msgstr ""
"Цей засіб створення було реалізовано у версії **Kdenlive** 17.04. Створює "
"кольорові тестові візерунки різних типів. Варіантами є кольорові смуги PAL, "
"кольорові смуги BBC, кольорові смуги EBU, кольорові смуги SMPTE, Philips "
"PM5544, FuBK"

#~ msgid "Stop Motion"
#~ msgstr "Стоп-кадр"

#~ msgid "See :ref:`stop_motion_capture`."
#~ msgstr "Див. :ref:`stop_motion_capture`."

#~ msgid "Contents"
#~ msgstr "Зміст"

#~ msgid ""
#~ "To create an image sequence clip, choose :menuselection:`Add Image "
#~ "Sequence` from the :menuselection:`Add Clip` drop down list."
#~ msgstr ""
#~ "Для створення кліпу з послідовності зображень виберіть пункт :"
#~ "menuselection:`Додати послідовність зображень` зі спадного списку :"
#~ "menuselection:`Додати кліп`."

#~ msgid ""
#~ "From the **Image Sequence** dialog choose :menuselection:`Filename "
#~ "pattern` as **Image selection method**."
#~ msgstr ""
#~ "У діалоговому вікні **Послідовність зображень** виберіть :menuselection:"
#~ "`Шаблон назви файлів` як **Спосіб вибору зображень**."

#~ msgid ""
#~ "You can use the :menuselection:`Markers` tab to add markers for certain "
#~ "points in the source file that are important. However, it is probably "
#~ "easier to add markers to your clips via the  :ref:`monitors` because that "
#~ "allows you to preview the file at the location where you are adding the "
#~ "marker."
#~ msgstr ""
#~ "Ви можете скористатися вкладкою :menuselection:`Позначки`, щоб додати "
#~ "позначки для певних точок у початковому файлі, які є з певних міркувань "
#~ "важливими. Втім, ймовірно, простіше додавати позначки до ваших кліпів за "
#~ "допомогою :ref:`моніторів <monitors>`, оскільки це надає вам змогу "
#~ "попередньо переглядати файл у місці, де було додано позначку."

#~ msgid ""
#~ "Once markers are put in your clip, you can access them in the :ref:"
#~ "`monitors` by right-clicking and selecting :menuselection:`Go To Marker` "
#~ "(see picture.)  Also note how the markers appear as red vertical lines in "
#~ "the **Clip Monitor** (see yellow highlighted regions in the picture.) You "
#~ "can turn on the display of the marker comments in the timeline too (see :"
#~ "ref:`editing`)."
#~ msgstr ""
#~ "Щойно ви розставите позначки на кліпі, ви зможете отримувати до них "
#~ "доступ на :ref:`моніторах <monitors>` клацанням правою кнопкою миші із "
#~ "наступним вибором пункту меню :menuselection:`Перейти до позначки` (див. "
#~ "зображення). Також зауважте, що позначки буде показано як червоні "
#~ "вертикальні лінії на **Моніторі кліпу** (див. підсвічені жовтим області "
#~ "на зображенні). Крім того, ви можете увімкнути показ коментарів до "
#~ "позначок на монтажному столі (див. :ref:`editing`)."

#~ msgid ""
#~ "Markers can also be added to clips on the timeline. :ref:"
#~ "`right_click_menu` the clip and choose :menuselection:`Markers --> Add "
#~ "Marker`.  Markers added this way also appear in the clip in the Project "
#~ "Bin."
#~ msgstr ""
#~ "Крім того, до кліпів на монтажному столі можна додавати позначки. :ref:"
#~ "`Клацніть правою кнопкою миші <right_click_menu>` кліпі і виберіть у "
#~ "контекстному меню пункт :menuselection:`Позначки --> Додати позначку`.  "
#~ "Позначки, які додано у цей спосіб, буде показано на пункті кліпу на "
#~ "панелі контейнера проєкту."

#~ msgid "Create Folder"
#~ msgstr "Створити теку"

#~ msgid "See :ref:`create_folder`."
#~ msgstr "Див. :ref:`create_folder`."
