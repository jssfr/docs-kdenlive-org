# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-17 00:40+0000\n"
"PO-Revision-Date: 2023-07-17 23:01+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:1
msgid "Do your first steps with Kdenlive video editor, using erosion effect"
msgstr ""
"Ваші перші кроки у відеоредакторі Kdenlive, використовуємо ефект ерозії"

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, image adjustment, erosion"
msgstr ""
"KDE, Kdenlive, відеоредактор, довідка, вивчення, просте, ефекти, фільтр, "
"відеоефекти, коригування зображення, ерозія"

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:16
msgid "Erosion"
msgstr "Ерозія"

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:18
msgid ""
"This effect/filter replaces the pixel by the local(3x3) minimum giving the "
"source image an erosion effect."
msgstr ""
"Цей ефект або фільтр замінює піксель локальним (3x3) мінімумом, що додає на "
"початкове зображення ефект ерозії."

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:20
msgid "The effect does not have keyframes."
msgstr "У цьому ефекті не передбачено прив'язування до ключових кадрів."

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:28
msgid "Erosion effect"
msgstr "Ефект ерозії"

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:30
msgid ""
"**1st / 2nd / 3rd / 4th Plane Threshold** - Limits the maximum change for "
"each :term:`plane`, default is 50. If 0, plane will remain unchanged."
msgstr ""
"**Поріг 1-ої / 2-ої / 3-ої / 4-ої площини** — обмежити максимальну зміну для "
"кожної :term:`площини <plane>`. Типовим значенням є 50. Якщо вказано "
"значення 0, площина лишиться незмінною."

#: ../../effects_and_compositions/video_effects/image_adjustment/erosion.rst:32
msgid ""
"**Coordinates** - Flag which specifies the pixel to refer to. Default is "
"255, i.e. all eight pixels are used."
msgstr ""
"**Координати** — прапорець, який вказує пікселі для обробки. Типовим є "
"значення 255, тобто використати вісім пікселів."
