# Spanish translations for docs_kdenlive_org_effects_and_compositions___video_effects___stylize___pixelize.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2023.
# Eloy Cuadra <ecuadra@eloihr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___video_effects___stylize___pixelize\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-26 00:39+0000\n"
"PO-Revision-Date: 2023-07-27 15:21+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:1
msgid "Do your first steps with Kdenlive video editor, using pixelize effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, stylize, pixelize"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:16
msgid "Pixelize"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:18
msgid ""
"This effect/filter pixelizes the input image. It works similar to the :ref:"
"`effects-obscure` effect but allows you to control the block size "
"individually and independently from the image size, but by contrast applies "
"it to the entire frame. In order to apply the **Pixelize** effect only to a "
"certain region use it in combination with the :ref:`effects-"
"alpha_shapes_mask` effect."
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:20
msgid "The effect has keyframes."
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:28
msgid "Pixelize effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:30
msgid "**Block Size X** - Horizontal size of one \"pixel\""
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:32
msgid "**Block Size Y** - Vertical size of one \"pixel\""
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/pixelize.rst:37
msgid ""
"The :guilabel:`Block Size` takes the aspect ratio into account. That means "
"that in a 16:9 project the ratio of the two values has to be 16:9 in order "
"to produce square pixels. Otherwise, identical values will produce "
"rectangles."
msgstr ""
