# Spanish translations for docs_kdenlive_org_effects_and_compositions___video_effects___color_image_correction___bw0r.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2023.
# Eloy Cuadra <ecuadra@eloihr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___video_effects___color_image_correction___bw0r\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-05 00:51+0000\n"
"PO-Revision-Date: 2023-07-03 04:40+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:1
msgid "Do your first steps with Kdenlive video editor, using the bw0r effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, bw0r, black and white "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:1
msgid "- Bernd Jordan "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:1
msgid "Creative Commons License SA 4.0"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:19
msgid "Bw0r"
msgstr "Bw0r"

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:21
msgid ""
"This effect simply turns the clip to black & white. There are no further "
"parameters or keyframes."
msgstr ""
"Este efecto solo convierte el clip a blanco y negro. No hay más parámetros "
"ni fotogramas clave."

#: ../../effects_and_compositions/video_effects/color_image_correction/bw0r.rst:23
msgid "This is the |bw0r| MLT filter."
msgstr "Este es el filtro MLT |bw0r|."
