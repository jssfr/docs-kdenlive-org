# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___misc___dance.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___misc___dance\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-22 00:41+0000\n"
"PO-Revision-Date: 2023-07-08 18:55-0300\n"
"Last-Translator: Gabriel Gazzán <gabcorreo@gmail.com>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.3.2\n"

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:1
msgid "Do your first steps with Kdenlive video editor, using the dance effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, on master, dance"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:17
msgid "Dance"
msgstr "Danza"

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:19
msgid ""
"This effect/filter is an audio visualization effect that zooms, moves and "
"rotates the image proportional to the magnitude of the audio spectrum."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:21
msgid ""
"Using the :guilabel:`Zoom` alone creates a cool pump effect in sync with the "
"beats."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:29
msgid "Dance effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:31
msgid ""
"**Low Frequency** - The low end of the frequency range to be used to "
"influence the image motion"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:33
msgid ""
"**High Frequency** - The high end of the frequency range to be used to "
"influence the image motion"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:35
msgid ""
"**Level Threshold** - The minimum amplitude of sound that must occur within "
"the frequency range to cause the image to move"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:37
msgid ""
"**Oscillation** - Oscillation can be useful to make the image move back and "
"forth during long periods of sound. A value of 0 specifies no oscillation."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:41
msgid ""
"**Initial Zoom** - The amount to zoom the image before any motion occurs. "
"This can be used to avoid black on the sides of the image when it moves."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:43
msgid "100% = no zoom"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:44
msgid "< 100% = zoom out (make the image smaller)"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:45
msgid "> 100% = zoom in (make the image larger)"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:47
msgid "**Zoom** - The amount that the audio affects the zoom of the image."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:49
msgid "< 0% = Image will zoom out (get smaller) with more sound"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:50
msgid "0% = no zoom"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:51
msgid "> 0% = Image will zoom in (get larger) with more sound"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:53
msgid ""
"**Left / Right** - The amount that the audio affects the left / right offset "
"of the image."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:55
#: ../../effects_and_compositions/video_effects/on_master/dance.rst:60
msgid "0% = no offset"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:56
msgid "> 0% = Image will move left / right with more sound"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:58
msgid ""
"**Up / Down** - The amount that the audio affects the up / down offset of "
"the image."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:61
msgid "> 0% = Image will move up / down with more sound"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:63
msgid ""
"**Clockwise / Counter Clockwise** - The amount that the audio affects the "
"rotation of the image."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:65
msgid "0 = no rotation"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:66
msgid "> 0 = Image will rotate with more sound"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:68
msgid ""
"**Window Size** - The number of samples that the FFT\\ [1]_ will be "
"performed on. If :guilabel:`Window Size` is less than the number of samples "
"in a frame, extra samples will be ignored. If :guilabel:`Window Size` is "
"more than the number of samples in a frame, samples will be buffered from "
"previous frames to fill the window. The buffering is performed as a sliding "
"window so that the most recent samples are always transformed."
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:73
msgid "**Notes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/on_master/dance.rst:75
msgid "FFT := Fast Fourier Transform"
msgstr ""

#~ msgid "This effect causes the video frame to dance around the screen."
#~ msgstr ""
#~ "Este efecto hace que la imagen \"dance\" (se desplace) por la pantalla."

#~ msgid ""
#~ "Add this effect to a video and include another video track below it and a "
#~ "composite transition between the two tracks."
#~ msgstr ""
#~ "Agregar este efecto a un clip e incluir otro clip en una pista debajo, "
#~ "con un método de composición entre ambos."

#~ msgid "https://youtu.be/gqxU1nvh6JI"
#~ msgstr "https://youtu.be/gqxU1nvh6JI"

#~ msgid "Contents"
#~ msgstr "Contenido"
