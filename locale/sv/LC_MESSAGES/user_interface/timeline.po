# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2022-09-25 09:35+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../user_interface/timeline.rst:1
msgid "Timeline, central part of Kdenlive video editor"
msgstr ""

#: ../../user_interface/timeline.rst:1
msgid ""
"KDE, Kdenlive, timeline, track, configure, navigation, working, "
"documentation, user manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../user_interface/timeline.rst:24 ../../user_interface/timeline.rst:52
msgid "Timeline"
msgstr "Tidslinje"

#: ../../user_interface/timeline.rst:27
msgid ""
"The timeline is the central part of **Kdenlive**. It is made of four "
"different areas:"
msgstr ""

#: ../../user_interface/timeline.rst:40
#, fuzzy
#| msgid "Timeline"
msgid "Timeline Areas"
msgstr "Tidslinje"

#: ../../user_interface/timeline.rst:42
msgid ""
"1 -  **Timeline**. This is the area where you drop your clips to the various "
"tracks. It shows the clips with or without thumbnails, with or without the "
"audio frequency curve, transitions and compositions, as well as the markers, "
"guides and keyframes (if any)."
msgstr ""

#: ../../user_interface/timeline.rst:44
msgid ""
"2 - **Timeline ruler**. This area shows the time in hh:mm:ss:ff notation. It "
"also shows the current zone (if defined) and the preview render zones and "
"their respective render status. A more detailed description can be found :"
"ref:`further down here <timeline_ruler>`. Left-clicking in the timeline "
"ruler will move the :ref:`timeline` and seek to that position. For the right-"
"click menu see the :ref:`detailed description <timeline_ruler_right-"
"click_menu>`."
msgstr ""

#: ../../user_interface/timeline.rst:46
msgid ""
"3 - **Timeline Toolbar** - This is an important area where the tools for "
"working with the clips in the timeline are easily accessible. Feel free to "
"adjust the timeline toolbar to accommodate your workflow. For details how to "
"do that refer to the section about toolbar configuration."
msgstr ""

#: ../../user_interface/timeline.rst:48
msgid ""
"4 - **Track Header**. This area displays track header data like track type "
"(V for video, A for audio), track number, with your without track effects, "
"hidden or muted, locked or unlocked, and an optional track name. Each track "
"can be adjusted in height individually as well as expanded or collapsed. A "
"more detailed description can be found :ref:`further down here "
"<track_header>`. For the right-click menu see the detailed section."
msgstr ""

#: ../../user_interface/timeline.rst:57
msgid "Zoombars"
msgstr "Zoomrader"

#: ../../user_interface/timeline.rst:67
msgid ""
"Besides the availability of zoombars in the monitor and keyframe scroll "
"bars, zoombars are now available in the timeline as well. You can easily "
"zoom in/out in the timeline by dragging the edges of the timeline scrollbar. "
"(Vertical zoombars coming soon.) Recommend playing this video in full-screen "
"mode."
msgstr ""

#: ../../user_interface/timeline.rst:72
msgid "Key binding information"
msgstr ""

#: ../../user_interface/timeline.rst:82
msgid ""
"Key binding info has been added on the left while context item information "
"has been moved to the right of the :ref:`status_bar`. Recommend playing this "
"video in full-screen mode."
msgstr ""

#: ../../user_interface/timeline.rst:87
msgid "Timeline visuals"
msgstr ""

#: ../../user_interface/timeline.rst:91
msgid ""
"The timeline got a visual overhaul with more and better looking guides/"
"marker colors, the guides have been moved above the timeline ruler while "
"preview and zone bars have been moved below."
msgstr ""

#: ../../user_interface/timeline.rst:96
msgid "Before (above) and after (below)"
msgstr ""

#: ../../user_interface/timeline.rst:103
msgid "Split Audio/Video"
msgstr ""

#: ../../user_interface/timeline.rst:110
msgid ""
"The way timeline tracks work has changed. Each track is now either audio or "
"video, and will only accept audio and video clips respectively. When "
"dragging an AV clip from the project bin to the timeline the clip will be "
"automatically split with the video part going on a video track, and the "
"audio part on an audio track."
msgstr ""

#: ../../user_interface/timeline.rst:112
msgid ""
"The separation of audio/video is important for implementing :ref:`same-track-"
"transitions <same_track_transition>`."
msgstr ""

#: ../../user_interface/timeline.rst:117
msgid "Timeline Cursor/Position Caret/Playhead"
msgstr ""

#: ../../user_interface/timeline.rst:126
msgid ""
"This indicates the position we are displaying in the :ref:`monitors`. You "
"can scroll the position by dragging the Timeline cursor (a.k.a Position "
"Caret or Playhead)."
msgstr ""

#: ../../user_interface/timeline.rst:128
msgid ""
"Beginning with version 0.9.4, dragging the timeline cursor will play the "
"audio of the clip (a.k.a. Audio Scrubbing).  This feature only works if you "
"have checked :menuselection:`Use Open GL for video display` in :ref:"
"`configure_kdenlive`."
msgstr ""

#: ../../user_interface/timeline.rst:135
msgid "Keyboard Navigation"
msgstr ""

#: ../../user_interface/timeline.rst:139
msgid ""
"You now have the possibility to move clips and compositions with your "
"keyboard. To do it, select a clip in the timeline and use the :guilabel:"
"`Grab Current Item` function from the :menuselection:`Menu --> Timeline` "
"menu or use the default shortcut of :kbd:`Shift + G`."
msgstr ""

#: ../../user_interface/timeline.rst:144
msgid "You can then move the item with the arrow keys."
msgstr ""

#: ../../user_interface/timeline.rst:146
msgid ""
"Keyframes can also be moved individually. Just click on a keyframe in the "
"timeline, then move it :kbd:`Left` or :kbd:`Right`, change its value with :"
"kbd:`+` and :kbd:`-`. Use :kbd:`Alt + arrow` to go to another keyframe."
msgstr ""

#: ../../user_interface/timeline.rst:154
msgid "Keyframe handling"
msgstr ""

#: ../../user_interface/timeline.rst:161
msgid "Add a new keyframe by double clicking in timeline."
msgstr ""

#: ../../user_interface/timeline.rst:162
msgid ""
"You can move a keyframe without altering its value by using the vertical "
"line that appears when you are above or below a keyframe."
msgstr ""

#: ../../user_interface/timeline.rst:163
msgid "Remove a keyframe by dragging it far above or below the clip limits."
msgstr ""

#: ../../user_interface/timeline.rst:168
msgid "Disabling individual clips"
msgstr ""

#: ../../user_interface/timeline.rst:178
msgid ""
"Individual clips can be disabled while still in the timeline but with no "
"audio and no video – (works for all clip types). Right-click on the clip and "
"choose :guilabel:`Disable clip` or :guilabel:`Enable clip`."
msgstr ""

#: ../../user_interface/timeline.rst:186
#, fuzzy
#| msgid "Timeline"
msgid "Timeline Ruler"
msgstr "Tidslinje"

#: ../../user_interface/timeline.rst:188
msgid ""
"The timeline ruler shows the timecode information in the notation of hh:mm:"
"ss:ff (default) or in frames. It displays the currently defined timeline "
"zone (1) and the preview render zone (2) and its respective rendering status "
"(red: not yet rendered; yellow: being rendered; green: finished)."
msgstr ""

#: ../../user_interface/timeline.rst:193
msgid ""
"The timeline zone can be moved by dragging the square in the middle, and "
"sized by either dragging the left or right edge or by positioning the "
"playhead in the timeline and pressing :kbd:`I` or :kbd:`O` to set in-point "
"and out-point, respectively. For more detailed information on preview render "
"please refer to this section of the documentation."
msgstr ""

#: ../../user_interface/timeline.rst:200
msgid ""
"Right click into the timeline ruler opens the context menu and allows you to:"
msgstr ""

#: ../../user_interface/timeline.rst:202
msgid ":ref:`Manipulate guides <guides>`"
msgstr ""

#: ../../user_interface/timeline.rst:203
msgid ":ref:`Set Zone In/Out <timeline-preview-rendering>`"
msgstr ""

#: ../../user_interface/timeline.rst:204
msgid ":ref:`Add Project Notes <notes>`"
msgstr ""

#: ../../user_interface/timeline.rst:205
msgid ":ref:`Add Subtitle <effects-subtitles>`"
msgstr ""

#: ../../user_interface/timeline.rst:211
#, fuzzy
#| msgid "Timeline"
msgid "Timeline Toolbar"
msgstr "Tidslinje"

#: ../../user_interface/timeline.rst:213
msgid ""
"The timeline toolbar controls various aspects of the editor. It can be "
"configured to accommodate your workflow (see :ref:`Configuring the Toolbars "
"<configuring_the_toolbars>` for more details)."
msgstr ""

#: ../../user_interface/timeline.rst:218
msgid ""
"This is a quick overview of the main sections of the toolbar. A more "
"detailed description can be found in the :ref:`timeline_toolbar3` section of "
"this documentation."
msgstr ""

#: ../../user_interface/timeline.rst:220
msgid ""
"1 - Enable track compositing and switching split view of audio and video "
"(see :ref:`layout_modes`)"
msgstr ""

#: ../../user_interface/timeline.rst:222
msgid ""
"2 - Selects the editing mode: Normal, Overwrite, Insert (see :ref:"
"`timeline_edit_modes`)"
msgstr ""

#: ../../user_interface/timeline.rst:224
msgid ""
"3 - Edit Tools: Selection Tool, Razor Tool, Spacer Tool, Slip Tool, Ripple "
"Tool (see :ref:`timeline_edit_tools`)"
msgstr ""

#: ../../user_interface/timeline.rst:226
msgid ""
"4 - Clip and Zone Tools: Mix Clips, Insert Clip Zone in Timeline, Overwrite "
"Clip Zone in Timeline, Extract Timeline Zone, Lift Timeline Zone, Expand "
"Clip (see also :ref:`3-point editing <three_point_editing>`)"
msgstr ""

#: ../../user_interface/timeline.rst:228
msgid ""
"5 - Preview Render Tools - Start/Stop Render, Open Preview Render Options "
"Dialog, Add Preview Render Zone, Delete All Preview Render Zones"
msgstr ""

#: ../../user_interface/timeline.rst:230
msgid "6 - Toggle Audio Mixer Widget"
msgstr ""

#: ../../user_interface/timeline.rst:232
msgid ""
"7 - Status Bar icons (in lieu of the status bar which can be switched off in "
"the View menu)*"
msgstr ""

#: ../../user_interface/timeline.rst:234
msgid ""
"\\* Please note this is a customized version of the timeline toolbar and the "
"section #7 is not part of the default toolbar"
msgstr ""

#: ../../user_interface/timeline.rst:239
msgid "Tracks"
msgstr ""

#: ../../user_interface/timeline.rst:241
msgid ""
"The timeline is made of tracks. There are two kinds of tracks: audio and "
"video. The number of tracks is defined when creating a new project in the :"
"ref:`project_settings`. Adding a clip to the timeline can be achieved by "
"dragging it from  :ref:`project_tree` or the :ref:`monitors`."
msgstr ""

#: ../../user_interface/timeline.rst:246
msgid "Track Header"
msgstr ""

#: ../../user_interface/timeline.rst:248
msgid "This area shows some options for a track:"
msgstr ""

#: ../../user_interface/timeline.rst:257
msgid ""
"1 Track collapse and expand icon. Click on it to collapse or expand the "
"track: hold Shift and click to expand or collapse all tracks of the same "
"type."
msgstr ""

#: ../../user_interface/timeline.rst:259
msgid "2 Track name. Click on it to enter or change the name of the track."
msgstr ""

#: ../../user_interface/timeline.rst:261
msgid "3 Track type and track number"
msgstr ""

#: ../../user_interface/timeline.rst:263
msgid "4 Track control icons. Use them to:"
msgstr ""

#: ../../user_interface/timeline.rst:265
msgid ""
"Lock the track |kdenlive-lock| which will prevent adding clips, removing "
"clips, or moving of clips on the timeline;"
msgstr ""

#: ../../user_interface/timeline.rst:266
msgid "Mute the track |kdenlive-hide-audio| (audio tracks only)"
msgstr ""

#: ../../user_interface/timeline.rst:267
msgid "Hide video |kdenlive-hide-video| from this track; and,"
msgstr ""

#: ../../user_interface/timeline.rst:268
msgid ""
"Enable/Disable track effects |tools-wizard| allows you to enable or disable "
"the effects applied to the track."
msgstr ""

#: ../../user_interface/timeline.rst:275
msgid "Adding Tracks"
msgstr ""

#: ../../user_interface/timeline.rst:277
msgid ""
"In order to add a track right-click anywhere in the track header area. In "
"the Add Track dialog window specify what type of track, where and how many "
"you want to insert."
msgstr ""

#: ../../user_interface/timeline.rst:285
msgid "Deleting Tracks"
msgstr ""

#: ../../user_interface/timeline.rst:287
msgid ""
"In order to delete a track right-click anywhere in the track header area. In "
"the Delete Track dialog window select the tracks you want to delete."
msgstr ""

#: ../../user_interface/timeline.rst:295
msgid "Resizing Tracks"
msgstr ""

#: ../../user_interface/timeline.rst:299
msgid ""
"Tracks can be individually resized. Holding down :kbd:`Shift` makes all "
"video or audio tracks change in height simultaneously."
msgstr ""

#: ../../user_interface/timeline.rst:307
msgid "Switching between mixed or split audio"
msgstr ""

#: ../../user_interface/timeline.rst:309
msgid "Switch live between two different layout modes (Mixed or Split)."
msgstr ""

#: ../../user_interface/timeline.rst:317
msgid "Configurable Tracks"
msgstr ""

#: ../../user_interface/timeline.rst:327
msgid "**Video track** - You can choose to display either"
msgstr ""

#: ../../user_interface/timeline.rst:329
msgid ":guilabel:`In Frame`"
msgstr ""

#: ../../user_interface/timeline.rst:331
msgid ":guilabel:`In/Out Frames`"
msgstr ""

#: ../../user_interface/timeline.rst:333
msgid ":guilabel:`All Frames` or"
msgstr ""

#: ../../user_interface/timeline.rst:335
msgid ":guilabel:`No Thumbnails`"
msgstr ""

#: ../../user_interface/timeline.rst:343
msgid "**Audio track** - You can enable:"
msgstr ""

#: ../../user_interface/timeline.rst:345
msgid ""
":guilabel:`Show Record Controls` to record audio direct into the track. More "
"details see :ref:`audio-recording`."
msgstr ""

#: ../../user_interface/timeline.rst:347
msgid ""
":guilabel:`Separate Channels` to see each channel of an audio track (i.e "
"stereo, 5.1)"
msgstr ""

#: ../../user_interface/timeline.rst:349
msgid ""
":guilabel:`Normalize Audio Thumbnails` maximize the audio level peak to -3dB."
msgstr ""

#: ../../user_interface/timeline.rst:358
msgid "Continuously loop playback"
msgstr ""

#: ../../user_interface/timeline.rst:360
msgid ""
"Disable :guilabel:`Pause playback when seeking` in :ref:`configure_timeline` "
"settings (:menuselection:`Settings --> Configure Kdenlive --> Timeline`)."
msgstr ""

#: ../../user_interface/timeline.rst:361
msgid "Make a timeline zone the length you like to loop."
msgstr ""

#: ../../user_interface/timeline.rst:362
msgid "Loop Zone (:kbd:`Ctrl + Shift + Space`)"
msgstr ""

#~ msgid "Contents"
#~ msgstr "Innehåll"
