# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2022-01-09 15:45+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:14
msgid "Analysis and Data - Vectorscope"
msgstr "Analys och data: Vektoroscilloskop"

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:18
msgid ""
"This is the `Frei0r vectorscope <https://www.mltframework.org/plugins/"
"FilterFrei0r-vectorscope/>`_ MLT filter."
msgstr ""
"Det här är MLT-filtret `Frei0r vektoroscilloskop <https://www.mltframework."
"org/plugins/FilterFrei0r-vectorscope/>`_."

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:20
msgid "Displays the vectorscope of the video-data."
msgstr "Visar vektoroscilloskopet för videodata."

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:22
#, fuzzy
#| msgid ""
#| "In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
#| "Effects."
msgid ""
"In ver 17.04 this is found in the :ref:`effects-utility` category of Effects."
msgstr "I version 17.04 finns det i effektkategorin :ref:`analysis_and_data`."

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:24
msgid ""
"It is recommended to use the vectorscope from :ref:`vectorscope`, because "
"the effect *Analysis and Data - Vectorscope* is not correct - it uses a "
"graticule from an analog NTSC vectorscope, but equations from digital video."
msgstr ""
"Det rekommenderas att använda vektoroscilloskopet från :ref:`vectorscope`, "
"eftersom effekten *Analys och data: Vektoroscilloskop* inte är korrekt: den "
"använder referenslinjer från ett analogt NTSC- vektoroscilloskop, men "
"ekvationer från digital video."

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:26
msgid "https://youtu.be/2ybBzDEjdRo"
msgstr "https://youtu.be/2ybBzDEjdRo"

#: ../../effects_and_compositions/video_effects/misc/vectorscope_MLT.rst:28
msgid "https://youtu.be/O1hbS6VZh_s"
msgstr "https://youtu.be/O1hbS6VZh_s"

#~ msgid "Contents"
#~ msgstr "Innehåll"
