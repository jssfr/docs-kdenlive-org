msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-20 00:41+0000\n"
"PO-Revision-Date: 2023-06-10 05:30+0200\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../effects_and_compositions/video_effects/grain_and_noise.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, grain and noise effect "
"category"
msgstr ""

#: ../../effects_and_compositions/video_effects/grain_and_noise.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, grain and noise, grain, noise"
msgstr ""

#: ../../effects_and_compositions/video_effects/grain_and_noise.rst:17
msgid "Grain and Noise"
msgstr ""

#: ../../effects_and_compositions/video_effects/grain_and_noise.rst:19
msgid ""
"The *Grain and Noise* video effects category provides effects and filter for "
"adding or removing noise. A special effect is :ref:`effects-scratchlines` "
"which is most often used with :ref:`effects-oldfilm` and :ref:`effects-dust`."
msgstr ""

#: ../../effects_and_compositions/video_effects/grain_and_noise.rst:21
msgid "The following effects and filters are available:"
msgstr ""
