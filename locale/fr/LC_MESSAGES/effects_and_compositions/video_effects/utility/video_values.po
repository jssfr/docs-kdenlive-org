# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-03 00:39+0000\n"
"PO-Revision-Date: 2022-01-03 13:49+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using video values effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, utility, video values"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:17
msgid "Video Values"
msgstr "Valeurs de vidéos"

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:19
msgid ""
"This effect/filter draws and overlays a window in the clip that shows a "
"zoomed version of the area covered by a small crosshair, and specific values "
"of the covered area of the video stream."
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:21
msgid "The effect has keyframes."
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:29
#, fuzzy
#| msgid "Video Values"
msgid "Video Values effect"
msgstr "Valeurs de vidéos"

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:31
msgid ""
"**Measurement** - Select what will be measured. Options are **RGB** "
"(default), **Y'PbPr - rec.601** and **Y'PbPr - rec.709**"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:33
msgid "**256 scale** - Use the scale 0..256 instead of 0.0..1.0 (default)"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:35
msgid "**Show alpha** - Show the Alpha channel"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:37
msgid "**Big window** - show a bigger version of the video values window"
msgstr ""

#: ../../effects_and_compositions/video_effects/utility/video_values.rst:39
msgid ""
"**X / Y / X size / Y size** - Set the position (X / Y) and the size (X "
"size / Y size) of the crosshair (determines the area covered and analyzed)"
msgstr ""

#~ msgid ""
#~ "This is the `Frei0r pr0be <https://www.mltframework.org/plugins/"
#~ "FilterFrei0r-pr0be/>`_ MLT filter."
#~ msgstr ""
#~ "Ceci est le filtre « Frei0r pr0be <https://www.mltframework.org/plugins/"
#~ "FilterFrei0r-pr0be/> »_ MLT."

#~ msgid "Measures video values."
#~ msgstr "Mesure les valeurs de vidéos."

#~ msgid "Contents"
#~ msgstr "Contenu"
