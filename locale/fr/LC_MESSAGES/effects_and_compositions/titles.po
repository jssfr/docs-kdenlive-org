# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2022-02-01 22:12+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/titles.rst:20
msgid "Titles"
msgstr "Titres"

#: ../../effects_and_compositions/titles.rst:22
msgid ""
"Titles are text elements that can be added to the timeline and appear over "
"the top of other clips. Titles are created in the :ref:`project_tree` and "
"then dragged to the timeline like other video clips."
msgstr ""
"Les titres sont des éléments de texte pouvant être ajoutés à la frise "
"chronologique et apparaissant au-dessus des autres vidéos. Les titres sont "
"créés dans :ref:`project_tree` et ensuite glissés sur la frise chronologique "
"comme les autres séquences de vidéo."

#: ../../effects_and_compositions/titles.rst:24
msgid ""
"If you want titles to overlay other footage, you put title clips above (on "
"video track 2 for example) and have the other footage below (on video track "
"1 for example). You also need to retain the affine transition that is "
"automatically added to the title clips if you want the footage visible "
"underneath."
msgstr ""
"Si vous voulez que les titres recouvrent d'autres sources, vous placez les "
"vidéos de titre au-dessus (sur la piste vidéo 2 par exemple) et les autres "
"sources en dessous (sur la piste vidéo 1 par exemple). Vous devez également "
"conserver la transition affinée, automatiquement ajoutée aux vidéos de titre "
"si vous voulez que la source soit visible en dessous."

#: ../../effects_and_compositions/titles.rst:27
msgid "Create and Edit Title"
msgstr "Créer et modifier un titre"

#: ../../effects_and_compositions/titles.rst:33
msgid ""
"**Create a Title:** Choose :menuselection:`Project --> Add Title Clip` or "
"right-click in an empty area in :ref:`project_tree` and choose :"
"menuselection:`Add Title Clip`."
msgstr ""
"**Créer un titre** : **Sélectionnez :menuselection:`Projet / Ajouter une "
"vidéo de titre` or effectuez un clic droit dans une zone vide de :ref:"
"`project_tree` et sélectionnez :menuselection:`Ajouter une vidéo de titre`."

#: ../../effects_and_compositions/titles.rst:35
msgid ""
"**Edit a Title:** Double click the title clip either in the project bin or "
"in the timeline. Or right-click the title clip in the project bin and "
"select :menuselection:`Edit Clip`."
msgstr ""
"**Modifier un titre : ** effectuez un double clic sur une vidéo de titre, "
"soit dans le dossier du projet, soit dans la frise chronologique. Ou "
"effectuez un clic droit sur la vidéo de titre dans le dossier du projet et "
"sélectionnez :menuselection:`Modifier une vidéo`."

#: ../../effects_and_compositions/titles.rst:37
msgid "**In the title window (1):**"
msgstr "**Dans la fenêtre de titre (1) : **"

#: ../../effects_and_compositions/titles.rst:39
msgid "Select an item by clicking on it."
msgstr "Sélectionner un élément en y cliquant dessus"

#: ../../effects_and_compositions/titles.rst:41
msgid "Edit text by double-clicking."
msgstr "Modifier du texte par un double clic"

#: ../../effects_and_compositions/titles.rst:43
msgid "Once your edit is done click :guilabel:`Update Title` (bottom right)."
msgstr ""
"Une fois votre modification effectuée, cliquez sur :guilabel:`Télécharger un "
"titre` (en bas à gauche)."

#: ../../effects_and_compositions/titles.rst:46
msgid "Add Items"
msgstr "Ajouter des éléments"

#: ../../effects_and_compositions/titles.rst:51
msgid "From the toolbar (green marked area) choose:"
msgstr "Sélectionnez à partir de la barre d'outils (zone marquée en vert) :"

#: ../../effects_and_compositions/titles.rst:53
msgid ":guilabel:`Text` :kbd:`Alt+T` and click into the title window (1)"
msgstr ""
":guilabel:`Texte` :kbd:` « ALT » + « T » ` et cliquez dans le fenêtre de "
"titre (1)"

#: ../../effects_and_compositions/titles.rst:55
msgid ""
":guilabel:`Rectangle` :kbd:`Alt+R` and in the title window (1) drag the "
"mouse to draw a rectangle"
msgstr ""
":guilabel:`Rectangle` :kbd:` « ALT » + « R » ` et dans la fenêtre de titre "
"(1), faites glisser la souris pour dessiner un rectangle."

#: ../../effects_and_compositions/titles.rst:57
msgid ""
":guilabel:`Ellipse` :kbd:`Alt+E` and in the title window (1) drag the mouse "
"to draw a ellipse."
msgstr ""
":guilabel:`Ellipse` :kbd:` « ALT » + « E » ` et dans la fenêtre de titre "
"(1), faites glisser la souris pour dessiner une ellipse."

#: ../../effects_and_compositions/titles.rst:59
msgid ""
":guilabel:`Image` :kbd:`Alt+I` brings up a file chooser where you can choose "
"an image to be inserted into your title (1)."
msgstr ""
":guilabel:`Image` :kbd:` « Alt » +« I » ` fait apparaître un sélecteur de "
"fichiers où vous pouvez choisir une image à insérer dans votre titre (1)."

#: ../../effects_and_compositions/titles.rst:61
msgid "Default selection is :guilabel:`Text` for a new title clip."
msgstr ""
"La sélection par défaut est :guilabel:`Texte` pour une nouvelle vidéo de "
"titre."

#: ../../effects_and_compositions/titles.rst:63
msgid ""
":kbd:`Alt + S` brings you back to :menuselection:`Selection Tool` (icon with "
"the 4 arrows, yellow marked)"
msgstr ""
":kbd:` « ALT » + « S » ` vous ramène à :menuselection:`Outil de sélection` "
"(icône avec 4 flèches, marquée en jaune)"

#: ../../effects_and_compositions/titles.rst:68
msgid ""
"Items can be placed behind each other by selecting them and changing the :"
"guilabel:`Z-index:` (top right corner) to a lower value or with the 4 icons "
"left to the :guilabel:`Z-index`"
msgstr ""
"Les éléments peuvent être placés les uns derrière les autres en les "
"sélectionnant et en modifiant la valeur de :guilabel:`Z-index:` (coin "
"supérieur droit) à une valeur plus faible ou avec les 4 icônes à gauche de :"
"guilabel:`Z-index`."

#: ../../effects_and_compositions/titles.rst:72
msgid "Select Items"
msgstr "Sélectionner des éléments"

#: ../../effects_and_compositions/titles.rst:77
msgid ""
"Yellow marked area. Make sure :guilabel:`Selection Tool` is selected or "
"press :kbd:`Alt + S`."
msgstr ""
"Zone marquée en jaune. Veuillez-vous assurer que :guilabel:`Outil de "
"sélection` est sélectionné ou appuyez sur :kbd:` « Alt » + « S » `."

#: ../../effects_and_compositions/titles.rst:79
msgid "In the title window (1) clicking on an item."
msgstr "Dans la fenêtre de titre (1), en cliquant sur un élément."

#: ../../effects_and_compositions/titles.rst:81
msgid "Holding :kbd:`Shift` to select several items by clicking on it."
msgstr ""
"Le maintien de :kbd:`Maj` pour sélectionner plusieurs éléments en cliquant "
"dessus."

#: ../../effects_and_compositions/titles.rst:83
msgid ":kbd:`Ctrl + A` selects all items."
msgstr ":kbd:` « CTRL » + « A » ` sélectionne tous les éléments."

#: ../../effects_and_compositions/titles.rst:88
msgid ""
"You can select items with one of the 5 selection possibilities (yellow "
"marked area, bottom left), from left to right:"
msgstr ""
"Vous pouvez sélectionner des éléments avec l'une des 5 possibilités de "
"sélection (zone marquée en jaune, en bas à gauche), de gauche à droite :"

#: ../../effects_and_compositions/titles.rst:90
msgid "If you have a selection: Keep only images selected"
msgstr "Si vous avez une sélection : ne conserver que les images sélectionnées"

#: ../../effects_and_compositions/titles.rst:91
msgid "If you have a selection: Keep only text items selected"
msgstr ""
"Si vous avez une sélection : ne conserver que les éléments de texte "
"sélectionnés"

#: ../../effects_and_compositions/titles.rst:92
msgid "Deselect everything"
msgstr "Tout désélectionner"

#: ../../effects_and_compositions/titles.rst:93
msgid "Select All"
msgstr "Tout sélectionner"

#: ../../effects_and_compositions/titles.rst:94
msgid "If you have a selection: Keep only rect items selected"
msgstr ""
"Si vous avez une sélection : ne conserver que les éléments rectangulaires "
"sélectionnées"

#: ../../effects_and_compositions/titles.rst:99
msgid "Adjust Items"
msgstr "Ajuster des éléments"

#: ../../effects_and_compositions/titles.rst:105
msgid ""
"Red marked areas: All items can be adjusted in position, size, level, "
"alignment and properties either with the mouse or by entering values."
msgstr ""
"Zones marquées en rouge : tous les éléments peuvent être ajustés en termes "
"de position, de taille, de niveau, d'alignement et de propriétés, soit avec "
"la souris, soit en saisissant des valeurs."

#: ../../effects_and_compositions/titles.rst:107
msgid "Holding :kbd:`Shift` move selected items vertical only."
msgstr ""
"Le maintien de :kbd:`Maj` permet uniquement le déplacement vertical des "
"éléments sélectionnés."

#: ../../effects_and_compositions/titles.rst:109
msgid "Holding :kbd:`Shift + Alt` move selected items horizontally only."
msgstr ""
"Le maintien de :kbd:` « Maj » + « ALT » ` permet uniquement le déplacement "
"horizontal des éléments sélectionnés."

#: ../../effects_and_compositions/titles.rst:111
msgid ""
"Once done click :guilabel:`Create Title` (bottom right) or :guilabel:`Update "
"Title`."
msgstr ""
"Une fois votre modification effectuée, cliquez sur :guilabel:`Créer un "
"titre` (en bas à droite) ou :guilabel:`Télécharger un titre`."

#: ../../effects_and_compositions/titles.rst:117
msgid "Typewriter"
msgstr "Machine à écrire"

#: ../../effects_and_compositions/titles.rst:124
msgid ""
"The beloved typewriter effect is back. By default it expands text by "
"characters, but other modes like expansion by words and lines are also "
"available."
msgstr ""
"Le célèbre effet de machine à écrire est de retour. Par défaut, il développe "
"le texte par caractères, mais d'autres modes comme le développement par mots "
"et par lignes sont également disponibles."

#: ../../effects_and_compositions/titles.rst:127
msgid ""
"Tells how fast next element will be displayed. The speed is constant over "
"the whole clip and effect period."
msgstr ""
"Indique à quelle vitesse l'élément suivant sera affiché. La vitesse est "
"constante pendant toute la durée de la vidéo et de l'effet."

#: ../../effects_and_compositions/titles.rst:129
msgid "Frame step"
msgstr "Pas de trame"

#: ../../effects_and_compositions/titles.rst:129
msgid "Values: 1-240, default: 25"
msgstr "Valeurs de [0..240]. 25 par défaut"

#: ../../effects_and_compositions/titles.rst:132
msgid "Allows for introducing small fluctuation to the step length."
msgstr "Permet d'introduire une petite fluctuation dans la longueur du pas."

#: ../../effects_and_compositions/titles.rst:134
msgid "Variation / Sigma"
msgstr "Variation / Sigma"

#: ../../effects_and_compositions/titles.rst:134
msgid "Values 0-20, default: 0"
msgstr "Valeurs de [0..20]. 0 par défaut"

#: ../../effects_and_compositions/titles.rst:137
msgid ""
"The random generator for fluctuations is initialized with constant seed to "
"assure predictable pattern. The seed parameter changes the initial value of "
"random generator."
msgstr ""
"Le générateur aléatoire de fluctuations est initialisé avec une valeur "
"initiale constante (« seed ») pour assurer un modèle prévisible. Le "
"paramètre « seed » modifie la valeur initiale du générateur aléatoire."

#: ../../effects_and_compositions/titles.rst:139
msgid "Seed"
msgstr "Départ"

#: ../../effects_and_compositions/titles.rst:139
msgid "Values: 0-100, default 0"
msgstr "Valeurs de [0..100]. 0 par défaut"

#: ../../effects_and_compositions/titles.rst:142
msgid "By character: types character by character"
msgstr "Par caractère : saisissez caractère par caractère"

#: ../../effects_and_compositions/titles.rst:144
msgid "By word: types word by word"
msgstr "Par mot : saisissez mot par mot"

#: ../../effects_and_compositions/titles.rst:146
msgid "By line: types line by line"
msgstr "Par ligne : saisissez ligne par ligne"

#: ../../effects_and_compositions/titles.rst:148
msgid "Custom: custom macros (not implemented)"
msgstr "Personnalisé : macros personnalisées (non implémentées)"

#: ../../effects_and_compositions/titles.rst:149
msgid "Expansion mode"
msgstr "Mode d'expansion"

#: ../../effects_and_compositions/titles.rst:152
msgid "Scroll Title Vertically"
msgstr "Faire défiler un titre verticalement"

#: ../../effects_and_compositions/titles.rst:154
msgid ""
"Put a long title onto the title window. Zoom out so you can see it all. The "
"text should run off the top (or bottom) of the viewable area."
msgstr ""
"Insérez un titre long dans la fenêtre de titre. Faites un zoom arrière pour "
"pouvoir tout voir. Le texte doit déborder en haut (ou en bas) de la zone "
"affichable."

#: ../../effects_and_compositions/titles.rst:156
msgid ""
"Select the :guilabel:`Animation` tab and check the :guilabel:`Edit start "
"viewport` option. Now drag the start rectangle to above the viewable area."
msgstr ""
"Sélectionnez l'onglet :guilabel:`Animation` et cochez l'option :guilabel:"
"`Éditer la fenêtre d'affichage`. Faites maintenant glisser le rectangle de "
"départ au-dessus de la zone affichable."

#: ../../effects_and_compositions/titles.rst:158
msgid ""
"Check the :guilabel:`Edit end viewport` option and drag the end rectangle to "
"below the viewable area."
msgstr ""
"Cochez l'option :guilabel:`Modifier la fenêtre d'affichage de fin` et faites "
"glisser le rectangle de fin en dessous de la zone affichable."

#: ../../effects_and_compositions/titles.rst:160
msgid "Press the :guilabel:`OK` button and preview the scrolled title."
msgstr ""
"Appuyez sur le bouton :guilabel:`Ok` et affichez un aperçu du titre en "
"défilement."

#: ../../effects_and_compositions/titles.rst:165
msgid ""
"The text in the above title scrolls up the screen. It is as if the camera "
"starts on the \"start rectangle\" and then pans down to the \"end rectangle"
"\"."
msgstr ""
"Le texte du titre ci-dessus défile à l'écran. C'est comme si la caméra "
"commençait sur le « rectangle de départ » et faisait un panoramique ensuite "
"jusqu'au « rectangle d'arrivée »."

#: ../../effects_and_compositions/titles.rst:167
msgid ""
"To make the text scroll faster, set the :guilabel:`Duration:` field "
"(highlighted in red in the image above) to a smaller value. To make the text "
"scroll slower, set it to a larger value."
msgstr ""
"Pour accélérer le défilement du texte, définissez le champ :guilabel:`Durée:"
"` (Surligné en rouge dans l'image ci-dessus) sur une valeur inférieure.Pour "
"faire défiler le texte plus lentement, définissez-le sur une valeur plus "
"grande."

#: ../../effects_and_compositions/titles.rst:169
msgid ""
"Note: changing the length of the title clip on the timeline does not change "
"the scrolling speed. If the length of the clip on the timeline is longer "
"than the duration specified in the title editor, the titles will pause on "
"the screen between the time the title's duration expires until the end of "
"the clip."
msgstr ""
"Remarque : la modification de la longueur d'une vidéo de titre sur la frise "
"chronologique ne modifie pas la vitesse de défilement. Si la longueur de la "
"vidéo sur la frise chronologique est supérieure à la durée spécifiée dans "
"l'éditeur de titres, alors, les titres seront en pause à l'écran entre le "
"moment où la durée du titre expire et la fin de la vidéo."

#: ../../effects_and_compositions/titles.rst:171
msgid ""
"If the length of the clip on the timeline is shorter than the duration "
"specified in the title editor, the scrolling will not complete before the "
"title clip finishes."
msgstr ""
"Si la longueur de la vidéo sur la frise chronologique est plus courte que la "
"durée spécifiée dans l'éditeur de titres, le défilement ne se terminera pas "
"avant la fin de la vidéo de titre."

#: ../../effects_and_compositions/titles.rst:173
msgid ""
"Note: the above description of title behaviour with respect to duration only "
"applies to titles that don't get edited after they have been placed on the "
"timeline. If you expand the length of a title clip on the timeline and then "
"edit the title (by double-clicking it in the Project Bin), its apparent "
"duration will become the length that it currently has on the timeline (i.e., "
"the scrolling will not pause at the end anymore) but the duration displayed "
"in the title editor will not have changed."
msgstr ""
"Remarque : la description ci-dessus du comportement des titres concernant la "
"durée ne s'applique qu'aux titres qui n'ont pas été modifiés après avoir été "
"placés sur la frise chronologique. Si vous augmentez la longueur d'une vidéo "
"de titre sur la frise chronologique et que vous éditez ensuite le titre (Par "
"un double clic dessus dans le dossier du projet), sa durée apparente "
"deviendra la longueur qu'il a actuellement sur la frise chronologique (c'est-"
"à-dire que le défilement ne fera plus de pause à la fin) mais la durée "
"affichée dans l'éditeur de titre n'aura pas changé."

#: ../../effects_and_compositions/titles.rst:177
msgid "Scroll Title Horizontally"
msgstr "Faire défiler un titre horizontalement"

#: ../../effects_and_compositions/titles.rst:179
msgid ""
"Use the instructions for vertical scrolling - just put the start and stop "
"rectangles off to the sides of the screen rather than the top and bottom."
msgstr ""
"Utilisez les instructions pour le défilement vertical - placez simplement "
"les rectangles de début et de fin sur les côtés de l'écran plutôt qu'en haut "
"et en bas."

#: ../../effects_and_compositions/titles.rst:183
msgid "Save a Title"
msgstr "Enregistrer un titre"

#: ../../effects_and_compositions/titles.rst:188
msgid "Blue marked area: Press the :guilabel:`Save As` button on the tool bar."
msgstr ""
"Zone marquée en bleu : appuyez sur le bouton :guilabel:`Enregistrer sous` de "
"la barre d'outils."

#: ../../effects_and_compositions/titles.rst:190
msgid ""
"**Title Editor** toolbar when title editor is wide enough for the whole "
"toolbar to display. The toolbar items are: **Selection Tool**, **Add Text**, "
"**Add Rectangle**, **Add Ellipse**, **Add Image**, **Open Document** and "
"**Save As**."
msgstr ""
"La barre d'outils **Éditeur de titres** lorsque l'éditeur de titres est "
"suffisamment large pour que toute la barre d'outils s'affiche. Les éléments "
"de la barre d'outils sont : **Outil de sélection**, **Ajouter du texte**, "
"**Ajouter un rectangle**, **Ajouter une ellipse**, **Ajouter une image**, "
"**Ouvrir un document** et **Enregistrer sous**."

#: ../../effects_and_compositions/titles.rst:192
msgid ""
"or select :guilabel:`Save as` from the toolbar overflow menu which can be "
"found under the :guilabel:`>>` button on the toolbar - see picture."
msgstr ""
"ou sélectionnez :guilabel:`Enregistrer sous` dans le menu déroulant de la "
"barre d'outils, se trouvant sous le bouton :guilabel:`>>` de la barre "
"d'outils - Voir l'image."

#: ../../effects_and_compositions/titles.rst:194
msgid ""
"**Title Editor** toolbar when title editor is not wide enough for the whole "
"toolbar to display. The toolbar items that do not fit can be accessed from "
"the :guilabel:`>>` button that appears at the end of the toolbar."
msgstr ""
"La barre d'outils **Éditeur de titres** lorsque l'éditeur de titres n'est "
"pas assez large pour que toute la barre d'outils s'affiche. Les éléments de "
"la barre d'outils ne s'ajustant pas peuvent être accessibles depuis le "
"bouton :guilabel:`>>` apparaissant à l'extrémité de la barre d'outils."

#: ../../effects_and_compositions/titles.rst:196
msgid "Choose a save location within your project."
msgstr "Choisissez un emplacement de sauvegarde dans votre projet."

#: ../../effects_and_compositions/titles.rst:198
msgid "The titles are saved as :file:`.kdenlivetitle` type documents."
msgstr ""
"Les titres sont enregistrés comme des documents de type :file:`."
"kdenlivetitle`."

#: ../../effects_and_compositions/titles.rst:202
msgid "Load a Title"
msgstr "Charger un titre"

#: ../../effects_and_compositions/titles.rst:207
msgid ""
"Blue marked area: Press the :guilabel:`Open Document` button on the Title "
"Clip editor toolbar or choose :menuselection:`Open document` from the :"
"guilabel:`>>` menu and load up a title saved earlier."
msgstr ""
"Zone marquée en bleu : appuyez sur le bouton :guilabel:`Ouvrir un document` "
"de la barre d'outils « Éditeur de vidéo de titre » ou sélectionnez :"
"menuselection:`Ouvrir un document` dans le menu :guilabel:`>>` et chargez un "
"titre précédemment enregistré."

#: ../../effects_and_compositions/titles.rst:211
msgid "Title Template"
msgstr "Modèle de titre"

#: ../../effects_and_compositions/titles.rst:216
msgid "Built In"
msgstr "Intégré"

#: ../../effects_and_compositions/titles.rst:222
msgid ""
"**Kdenlive** has some built-in title templates that can be accessed from "
"the :guilabel:`Template:` drop-down list found on the bottom of the **Title "
"Clip** window - see below."
msgstr ""
"**Kdenlive** dispose de quelques modèles de titres intégrés, accessibles "
"depuis la liste déroulante :guilabel:`Modèle:` située en bas de la fenêtre "
"**Vidéo de titre** - voir ci-dessous."

#: ../../effects_and_compositions/titles.rst:227
msgid "Download new title template"
msgstr "Télécharger un nouveau modèle de titre"

#: ../../effects_and_compositions/titles.rst:232
msgid ""
"Blue marked area: To install more title templates press the :guilabel:"
"`Download New Title Templates...` icon on the tool bar when you are in the "
"title editor."
msgstr ""
"Zone marquée en bleu : pour installer plus de modèles de titres, appuyez sur "
"l'icône :guilabel:`Télécharger de nouveaux modèles de titres...` de la barre "
"d'outils lorsque vous êtes dans l'éditeur de titres."

#: ../../effects_and_compositions/titles.rst:234
msgid ""
"If you have a good title template, you can post it `here <https://store.kde."
"org/browse/cat/335/>`_ so that other **Kdenlive** users can download it "
"through :menuselection:`Download New Title Templates...` and use it."
msgstr ""
"Si vous avez un bon modèle de titre, vous pouvez le poster « ici <https://"
"store.kde.org/browse/cat/335/> »_ afin que les autres utilisateurs de "
"**Kdenlive** puissent le télécharger via :menuselection:`Télécharger de "
"nouveaux modèles de titres...` et l'utiliser."

#: ../../effects_and_compositions/titles.rst:239
msgid ""
"Once these title templates are installed, they can be accessed via the drop "
"down on the :ref:`template <title_template>` dialog."
msgstr ""
"Une fois ces modèles de titres installés, ils sont accessibles grâce au menu "
"déroulant de la boîte de dialogue :ref:`template <title_template>`."

#: ../../effects_and_compositions/titles.rst:241
msgid ""
"The :file:`.kdenlivetitle` files that supply these templates are installed "
"to:"
msgstr ""
"Les fichiers :file:`.kdenlivetitle` fournissant ces modèles sont installés "
"dans :"

#: ../../effects_and_compositions/titles.rst:243
msgid "**Linux**"
msgstr "**Linux**"

#: ../../effects_and_compositions/titles.rst:245
msgid ":file:`$HOME/.local/share/kdenlive/titles`"
msgstr ":file:`$HOME/.local/share/kdenlive/titles`"

#: ../../effects_and_compositions/titles.rst:247
msgid "**Flatpak**"
msgstr "**Flatpak**"

#: ../../effects_and_compositions/titles.rst:249
msgid ":file:`$HOME/.var/app/org.kde.kdenlive/data/kdenlive/titles`"
msgstr ":file:`$HOME/.var/app/org.kde.kdenlive/data/kdenlive/titles`"

#: ../../effects_and_compositions/titles.rst:251
msgid "**Windows**"
msgstr "**Windows**"

#: ../../effects_and_compositions/titles.rst:253
msgid ":file:`%AppData%/kdenlive/titles`"
msgstr ":file:`%AppData%/kdenlive/titles`"

#: ../../effects_and_compositions/titles.rst:255
msgid ""
"Press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key simultaneously) "
"and copy **%AppData%/kdenlive/**."
msgstr ""
"Appuyez :kbd:` « Win » + « R » ` (Touches :kbd:`Windows` et :kbd:`R` "
"simultanément) et copiez **%AppData%/kdenlive/**."

#: ../../effects_and_compositions/titles.rst:258
msgid "Template Titles - User-Defined"
msgstr "Titres des modèles - Définis par l'utilisateur"

#: ../../effects_and_compositions/titles.rst:260
msgid ""
"*Template Titles* allow you to create a template for other titles in your "
"project. You create the template title with the settings that all the titles "
"in the project should have and then base subsequent titles on the template. "
"If you decide to change the look of your titles, you only need change the "
"template title and the titles based on this template will automatically "
"update to reflect any formatting changes you made to the template title."
msgstr ""
"*Les titres modèles* vous permettent de créer un modèle pour les autres "
"titres de votre projet. Vous créez le titre modèle avec les paramètres que "
"tous les titres du projet doivent avoir, puis vous utilisez les titres "
"suivants sur le modèle. Si vous décidez de modifier l'aspect de vos titres, "
"il vous suffit simplement de modifier le titre modèle et les titres reposant "
"sur ce modèle seront automatiquement mis à jour pour refléter les "
"modifications de mise en forme que vous avez apportées au titre modèle."

#: ../../effects_and_compositions/titles.rst:263
msgid "Create a Template Title"
msgstr "Créer un titre de modèle"

#: ../../effects_and_compositions/titles.rst:265
msgid ""
"Choose the :menuselection:`Add Title Clip` item from the :guilabel:`Add "
"Clip` drop down and create a title with the text *%s* in it and formatted "
"how you desire it. Save this title as described above."
msgstr ""
"Sélectionnez l'élément :menuselection:`Ajouter une vidéo de titre` dans le "
"menu déroulant :guilabel:`Ajouter une vidéo` et créez un titre contenant le "
"texte *%s* et formaté comme vous le souhaitez. Enregistrez le comme décrit "
"ci-dessus."

#: ../../effects_and_compositions/titles.rst:273
msgid "Use the Template Title"
msgstr "Utiliser le titre du modèle"

#: ../../effects_and_compositions/titles.rst:275
msgid ""
"Choose the :menuselection:`Add Template Title` item from the :guilabel:`Add "
"Clip` drop down and choose the title with the *%s* in it that you just saved."
msgstr ""
"Sélectionnez l'élément :menuselection:`Ajouter un titre de modèle` à partir "
"de la liste déroulante :guilabel:`Ajouter une vidéo` et sélectionnez le "
"titre contenant le paramètre « *%s* » que vous venez d'enregistrer."

#: ../../effects_and_compositions/titles.rst:277
msgid ""
"Right-click this clip in the Project Bin and select :menuselection:`Clip "
"Properties` item."
msgstr ""
"Cliquez avec le bouton droit de la souris sur cette vidéo dans le dossier du "
"projet et sélectionnez l'élément :menuselection:`Propriétés vidéo`."

#: ../../effects_and_compositions/titles.rst:284
msgid ""
"Enter the text that this title should display into the text field in the "
"dialog that appears."
msgstr ""
"Saisissez le texte que ce titre doit afficher dans le champ de texte de la "
"boîte de dialogue s'affichant."

#: ../../effects_and_compositions/titles.rst:291
msgid "Drag the title to the timeline."
msgstr "Faire glisser le titre sur la frise chronologique."

#: ../../effects_and_compositions/titles.rst:293
msgid ""
"The *%s* in the template will be replaced with the text that you enter in "
"the :guilabel:`Text:` field."
msgstr ""
"Le paramètre « *%s* » du modèle sera remplacé par le texte que vous "
"saisissez dans le champ :guilabel:`Texte :`."

#: ../../effects_and_compositions/titles.rst:299
msgid ""
"A known issue with template titles is that text centering does not work "
"correctly for text replacing the %s."
msgstr ""
"Un problème connu avec les titres de modèles est que le centrage du texte ne "
"fonctionne pas correctement pour le texte remplaçant le paramètre « %s »."

#: ../../effects_and_compositions/titles.rst:303
msgid "How to fade titles in and/or out"
msgstr ""
"Comment faire apparaître et / ou disparaître les titres en fondu enchaîné"

#: ../../effects_and_compositions/titles.rst:305
msgid ""
"To make titles fade in and out, you modify the transition which gets "
"automatically added between the title and the track below. The modifications "
"consist of adding keyframes into the transition and adjusting the opacity of "
"the transitions at these keyframes. In version 0.9.3 it is an :ref:`affine` "
"transition that is automatically added between the title and the track "
"below. In ver 0.9.2 it is a :ref:`composite`."
msgstr ""
"Pour faire apparaître et disparaître les titres en fondu, vous devez "
"modifier la transition qui s'ajoute automatiquement entre le titre et la "
"piste ci-dessous. Les modifications consistent en l'ajout de trames clé dans "
"la transition et l'ajustement de l'opacité des transitions sur ces trames "
"clé. Dans la version 0.9.3, il s'agit d'une transition :ref:`affine`, "
"automatiquement ajoutée entre le titre et la piste ci-dessous. Dans la "
"version 0.9.2, il s'agit d'une transition :ref:`composite`."

#: ../../effects_and_compositions/titles.rst:307
msgid ""
"In the image below we have four keyframes (labeled 1 to 4). The first "
"keyframe is the one currently displayed and we can see that the opacity on "
"this keyframe is zero. The opacity at keyframes 2 and 3 is 100%. The opacity "
"at the 4th keyframe is zero percent. The overall effect is that the title "
"fades in between keyframe 1 and keyframe 2. And then it fades out between "
"keyframe 3 and keyframe 4 ."
msgstr ""
"Dans l'image ci-dessous, nous avons quatre trames clé (étiquetées de 1 à 4). "
"La première trame clé est celle actuellement affichée et nous pouvons voir "
"que l'opacité sur cette trame clé est de zéro. L'opacité des trames clé 2 et "
"3 est de 100 %. L'opacité de la quatrième trame clé est de 0 %. L'effet "
"global fait que le titre s'affiche en fondu entre les trames clé 1 et 2, "
"puis le fondu s'estompe entre les trames clé 3 et 4."

#: ../../effects_and_compositions/titles.rst:314
msgid "How to fade in more than one title sequentially"
msgstr "Comment faire un fondu enchaîné sur plus d'un titre ?"

#: ../../effects_and_compositions/titles.rst:316
msgid "To create a title sequence like this ..."
msgstr "Pour créer une séquence de titres comme celle-ci..."

#: ../../effects_and_compositions/titles.rst:318
msgid "https://youtu.be/IIV87bFjfo0"
msgstr "https://youtu.be/IIV87bFjfo0"

#: ../../effects_and_compositions/titles.rst:320
msgid ""
"You put three titles on three different tracks but you make all three affine "
"transitions go to the same empty video track (instead of the tracks directly "
"below them, which is the default). See timeline screenshot below."
msgstr ""
"Vous placez trois titres sur trois pistes différentes mais vous faites en "
"sorte que les trois transitions affinées aillent sur la même piste vidéo "
"vide (au lieu des pistes directement en dessous d'elles, ce qui est le cas "
"par défaut). Se référer à la copie d'écran de la frise chronologique ci-"
"dessous."

#: ../../effects_and_compositions/titles.rst:327
msgid "FAQ"
msgstr "Questions les plus fréquentes (FAQ)"

#: ../../effects_and_compositions/titles.rst:329
msgid "Q: How to duplicate a title clip to modify it slightly."
msgstr "Q : comment dupliquer une vidéo de titre pour la modifier légèrement ?"

#: ../../effects_and_compositions/titles.rst:331
#, fuzzy
#| msgid ""
#| "A: You can save a copy of the title (see :ref:`titles`) and then create a "
#| "new title based on that saved version as described :ref:`titles`. Or you "
#| "could use the :ref:`titles` functionality to base the two slightly "
#| "different titles on the one template."
msgid ""
"A: You can save a copy of the title (see :ref:`effects-titles`) and then "
"create a new title based on that saved version as described :ref:`effects-"
"titles`. Or you could use the :ref:`effects-titles` functionality to base "
"the two slightly different titles on the one template."
msgstr ""
"Q : Vous pouvez enregistrer une copie du titre (voir :ref:`titles`), puis "
"créer ensuite un nouveau titre reposant sur cette version enregistrée, comme "
"décrit dans :ref:`titles`. Ou vous pouvez utiliser la fonctionnalité :ref:"
"`titres` pour produire les deux titres légèrement différents à partir du "
"même modèle."

#~ msgid "Contents"
#~ msgstr "Contenu"

#~ msgid "**To create a template title**"
#~ msgstr "**Pour créer un titre de modèle**"

#~ msgid ""
#~ "Open the title in the :ref:`project_tree` by double-clicking it or right-"
#~ "click then choose :menuselection:`Clip Properties`."
#~ msgstr ""
#~ "Ouvrez le titre dans le :ref:`arborescence_projet` par un double clic "
#~ "dessus ou un clic droit puis en sélectionnant :menuselection:`Propriétés "
#~ "vidéo`."

#~ msgid ""
#~ "Choose the :menuselection:`Add Title Clip` item from the :guilabel:`Add "
#~ "Clip` drop down (see picture)."
#~ msgstr ""
#~ "Sélectionnez l'élément :menuselection:`Ajouter une vidéo de titre` à "
#~ "partir de la liste déroulante :guilabel:`Ajouter une vidéo (Voir l'image)."

#~ msgid "How to edit an existing title"
#~ msgstr "Comment modifier un titre existant"

#~ msgid ""
#~ "Right-click the title clip in the Project Bin and select the :"
#~ "menuselection:`Clip properties` item."
#~ msgstr ""
#~ "Faites un clic droit avec la souris sur la vidéo de titre dans le dossier "
#~ "du projet et sélectionnez l'élément :menuselection:`Propriétés vidéo`."

#~ msgid "Template Titles - Built In"
#~ msgstr "Titres de modèles - Intégré"

#~ msgid "Import an Image into the title"
#~ msgstr "Importer une image dans le titre"

#~ msgid "Draw Rectangle Toolbar Item"
#~ msgstr "Élément de la barre d'outils pour dessiner un rectangle"

#~ msgid "Contents:"
#~ msgstr "Contenu :"
