# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2023-06-10 13:48+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:12
msgid "Timeout Indicator"
msgstr "Indicator van tijdslimiet"

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:17
msgid ""
"This is `Frei0r timeout <https://www.mltframework.org/plugins/FilterFrei0r-"
"timeout/>`_ MLT filter by Simon A. Eugster."
msgstr ""
"Dit is MLT-filter `Frei0r timeout <https://www.mltframework.org/plugins/"
"FilterFrei0r-timeout/>`_ door Simon A. Eugster."

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:19
msgid ""
"In ver 17.04 this is found in the :ref:`effects-utility` category of Effects."
msgstr ""
"In versie 17.04 is dit gevonden in de categorie :ref:`effects-utility` van "
"effecten."

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:21
msgid ""
"This adds a little countdown bar to the bottom right of the video and is "
"available in ver. 0.9.5 of **Kdenlive**."
msgstr ""
"Dit voegt een kleine aftelbalk toe rechts onderaan de video en is "
"beschikbaar in ver. 0.9.5 van **Kdenlive**."

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:23
msgid "The settings in this screen shot produced the sample video below."
msgstr ""
"De instellingen in deze schermafdruk produceert de onderstaande "
"voorbeeldvideo."

#: ../../effects_and_compositions/video_effects/misc/timeout_indicator.rst:27
msgid "https://youtu.be/ry3DLZD_bRc"
msgstr "https://youtu.be/ry3DLZD_bRc"

#~ msgid "Contents"
#~ msgstr "Inhoud"
