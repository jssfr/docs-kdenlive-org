# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-11 00:39+0000\n"
"PO-Revision-Date: 2023-07-11 13:54+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:1
msgid "Do your first steps with Kdenlive video editor, using normaliz0r effect"
msgstr ""
"Zet uw eerste stappen met de Kdenlive videobewerker, met gebruik van het "
"effect normaliz0r"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, normaliz0r "
msgstr ""
"KDE, Kdenlive, videobewerker, hulp, leren, gemakkelijk, effecten, filter, "
"video-effecten, kleur- en afbeeldingscorrectie, normaliz0r"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:1
msgid "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "
msgstr "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Creative Commons License SA 4.0"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:19
msgid "Normaliz0r"
msgstr "Normaliz0r"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:21
msgid ""
"This effect/filter normalizes RGB video (aka histogram stretching, contrast "
"stretching)."
msgstr ""
"Di effect/filter normaliseert RGB video (ook bekend als histogram uitrekken, "
"contrast uitrekken)."

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:23
msgid "The effect has keyframes."
msgstr "Dit effect heeft keyframes."

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:31
msgid "Normaliz0r effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:33
msgid ""
"**Smoothing** - The number of previous frames to use for temporal smoothing. "
"The input range of each channel is smoothed using a rolling average over the "
"current frame and the smoothing previous frames. The default is 0 (no "
"temporal smoothing)."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:35
msgid ""
"**Independence** - Controls the ratio of independent (color shifting) "
"channel normalization to linked (color preserving) normalization. 0.0 is "
"fully linked, 1.0 is fully independent. Defaults to 1.0 (fully independent)."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:37
msgid ""
"**Strength** - Overall strength of the filter. 1.0 is full strength. 0.0 is "
"a rather expensive no-op. Defaults to 1.0 (full strength)."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:39
msgid ""
"**BlackPt / WhitePt** - Colors which define the output range. The minimum "
"input value is mapped to the *blackpt*. The maximum input value is mapped to "
"the *whitept*. The defaults are black and white respectively. Specifying "
"white for *blackpt* and black for *whitept* will give color-inverted, "
"normalized video. Shades of grey can be used to reduce the dynamic range "
"(contrast). Specifying saturated colors here can create some interesting "
"effects."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:43
msgid ""
"The Normaliz0r and :ref:`effects-normalize_rgb_video` effects essentially do "
"the same but produce slightly different results."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:45
msgid "**Notes**"
msgstr "**Notities**"

#: ../../effects_and_compositions/video_effects/color_image_correction/normaliz0r.rst:47
msgid ""
"For more information refer to the Wikipedia article about |"
"wikipedia_normalization|."
msgstr ""
