# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-24 07:24+0000\n"
"PO-Revision-Date: 2023-07-24 10:59+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:1
msgid "Do your first steps with Kdenlive video editor, using sop/sat effect"
msgstr ""
"Zet uw eerste stappen met de Kdenlive videobewerker, met gebruik van het "
"effect sop/sat"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, sop/sat, slope offset power saturation "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:1
msgid ""
"- Claus Christensen - Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:"
"Ttguy) - Bushuev (https://userbase.kde.org/User:Bushuev) - Mmaguire (https://"
"userbase.kde.org/User:Mmaguire) - Bernd Jordan (https://discuss.kde.org/u/"
"berndmj) "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Creative Commons License SA 4.0"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:26
msgid "SOP/Sat Effect"
msgstr "SOP/Sat-effect"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:28
msgid ""
"This effect/filter changes Slope, Offset, and Power of the color components, "
"and the overall Saturation, according to the ASC CDL (Color Decision List)\\ "
"[1]_"
msgstr ""
"Dit effect/filter wijzigt de helling, offset en kleurkracht en de "
"kleurcomponenten en algehele verzadiging volgens de ASC CDL (Color Decision "
"List)\\ [1]_."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:30
msgid "The effect has keyframes."
msgstr "Dit effect heeft keyframes."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:32
msgid ""
"Changing the slope means multiplying the pixel value with a constant value. "
"Black pixels will remain black, while brighter ones will be changed. All "
"effects can be observed well when applied on a greyscale gradient and "
"looking at the :ref:`view-rgb_parade`."
msgstr ""
"De helling wijzigen betekent de pixelwaarde met een constante waarde "
"vermenigvuldigen. Zwarte pixels zullen zwart blijven, terwijl helderden "
"gewijzigd zullen worden. Alle effecten kunnen goed bekeken worden bij "
"toepassen op een grijs kleurverloop en te kijken op de :ref:`view-"
"rgb_parade`."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:34
msgid "You can use this effect to achieve proper white balance."
msgstr "U kunt dit effect gebruiken om een juiste witbalans te behalen."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:42
msgid "SAP/Sat effect"
msgstr "SAP/Sat-effect"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:44
msgid ""
"**Slope R / G / B / @** - Slope is the multiplier to the incoming data in "
"the respective color channels. Allowed values are from 0 to 1000, default is "
"50."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:46
msgid ""
"**Offset R / G / B / @** - Offset is a summation to the incoming data in the "
"respective color channels. Allowed values are from 0 to 2048, default is "
"1024."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:48
msgid ""
"**Power R / G / B / @** - Power is a power function (i.e. 2^2) to the "
"incoming data in the respective color channels. Allowed values are from 0 to "
"1000, default is 50."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:50
msgid ""
"**Overall Saturation** - Changes the overall :term:`saturation`. Allowed "
"values are from 0 to 1000, default is 100."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:55
msgid ""
"This filter implements a standard way of color correction proposed by the "
"American Society of Cinematographers: The Color Decision List, also known as "
"the ASC CDL\\ [1]_ with the goal to exchange rudimentary color correction "
"information between post-production tools."
msgstr ""
"Dit filter implementeert een standaard manier van kleurcorrectie voorgesteld "
"door de American Society of Cinematographers: De Kleurbeslissingslijst, ook "
"bekend als de ASC CDL\\ [1]_ met het doel rudimentaire kleurcorrectie- "
"informatie uit te wisselen tussen hulpmiddelen voor nabewerking."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:57
msgid ""
"The ASC CDL is a standard format for basic primary color correction (primary "
"meaning affecting the whole image and not only selected parts)."
msgstr ""
"De ASC CDL is een standaard formaat voor basis primaire kleurcorrectie "
"(primair betekent de gehele afbeelding beïnvloedend en niet alleen "
"geselecteerde delen)."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:59
msgid ""
"Basically there are two stages in the correction: 1. SOP correction for each "
"channel separately 2. Overall saturation correction"
msgstr ""
"In de basis zijn er twee statussen in de correctie: 1. SOP-correctie voor "
"elk kanaal apart 2. Verzadigingscorrectie overal"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:63
msgid ""
"All corrections work on [0,1], so the RGB(A) values need to be transposed "
"from [0,...,255] to [0,1]."
msgstr ""
"Alle correcties werken op [0,1], dus de RGB(A) waarden moet overgezet worden "
"van [0,...,255] naar [0,1]."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:65
msgid "SOP correction"
msgstr "SOP-correctie"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:67
msgid "Slope:   ``out = in * slope;   0 <= slope < ∞``"
msgstr "Helling:   ``uit = in * helling;   0 <= helling < ∞``"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:68
msgid "Offset:  ``out = in + offset;  -∞ < offset < ∞``"
msgstr "Offset:  ``uit = in + offset;  -∞ < offset < ∞``"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:69
msgid "Power:   ``out = in^power;     0 < power < ∞``"
msgstr "Macht:   ``uit = in^macht;     0 < macht < ∞``"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:71
msgid "Saturation"
msgstr "Verzadiging"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:73
msgid "Luma:    ``Y = 0.2126 R + 0.7152 G + 0.0722 B`` (according to Rec. 709)"
msgstr "Luma:    ``Y = 0.2126 R + 0.7152 G + 0.0722 B`` (volgens Rec. 709)"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:74
msgid "For all channels: ``out = luma + sat * (in-luma)``"
msgstr "Voor alle kanalen: ``uit = luma + verz * (in-luma)``"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:76
msgid ""
"As the values may exceed 1 (or 0), they need to be clipped where necessary."
msgstr ""
"Omdat de waarden groter kunnen zijn dan 1 (of 0), moeten ze waar nodig "
"afgekort worden."

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:79
msgid "**Notes**"
msgstr "**Notities**"

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:81
msgid "More details can be found in this article: |cdl_explained|."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/sat.rst:83
msgid ""
"See :ref:`Granjow's blog <waveform_and_RGB_parade>` where he uses the effect "
"to adjust white balance of a clip."
msgstr ""
"Zie :ref:`Granjow's blog <waveform_and_RGB_parade>` waar hij het effect "
"gebruikt om de witbalans van een clip aanpast."

#~ msgid ""
#~ "More information about the ASC CDL can be found on `wikipedia <https://en."
#~ "wikipedia.org/wiki/ASC_CDL>`_."
#~ msgstr ""
#~ "Meer informatie over de ASC CDL is te vinden op `wikipedia <https://en."
#~ "wikipedia.org/wiki/ASC_CDL>`_."

#~ msgid "Contents"
#~ msgstr "Inhoud"
