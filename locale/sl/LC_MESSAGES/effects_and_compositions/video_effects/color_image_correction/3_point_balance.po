# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-02 00:40+0000\n"
"PO-Revision-Date: 2023-07-04 07:27+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using the 3-point balance "
"effect"
msgstr ""
"Opravite svoje prve korake z montažnim programom Kdenlive z uporabo učinka 3-"
"točkovnega ravnovesja"

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, 3-point balance, three point balance "
msgstr ""
"KDE, Kdenlive, montažni program, program za videomontažo, montaža, pomoč, "
"učenje, enostavno, učinki, filter, video učinki, korekcija barv in slike, "
"poprava barv, koloriranje, 3-točkovno ravnovesje, tritočkovno ravnovesje "

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:1
msgid ""
"- Claus Christensen - Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:"
"Ttguy) - Bushuev (https://userbase.kde.org/User:Bushuev) - Mmaguire (https://"
"userbase.kde.org/User:Mmaguire) - Bernd Jordan "
msgstr ""
"- Claus Christensen - Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:"
"Ttguy) - Bushuev (https://userbase.kde.org/User:Bushuev) - Mmaguire (https://"
"userbase.kde.org/User:Mmaguire) - Bernd Jordan "

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Izdano pod dovoljenjem Creative Commons SA 4.0"

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:22
msgid "3 Point Balance"
msgstr "3-točkovno ravnovesje"

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:24
msgid ""
"This effect is a simple implementation of the Bezier Curves effect. With the "
"simplified interface you select the shades of grey using the |pipette| color "
"picker for Black Level, Grey Level and White Level, or use the color "
"selection buttons. See also :ref:`effects-bezier_curves`."
msgstr ""
"Ta učinek je preprosta izvedba učinka Bezierjevih krivulj. V poenostavljenem "
"vmesniku izberete odtenke sive barve s pomočjo izbirnika |pipete| za črno, "
"sivo in belo raven ali pa uporabite gumbe za izbiro barve. Oglejte si tudi :"
"ref:`krivulje Bezier<effects-bezier_curves>`."

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:32
msgid "3-point Balance effect"
msgstr "Učinek 3-točkovno ravnovesje"

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:34
msgid ""
"**Split screen preview** - Select this to have a split screen in the Project "
"Monitor where you can compare the results of the effect/filter with the "
"original clip."
msgstr ""
"**Predogled deljenega zaslona** - izberite to možnost, če želite v Ogledu "
"projekta prikazati razdeljeni zaslon, na katerem lahko rezultate učinka/"
"filtra primerjate z izvirnim posnetkom."

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:36
msgid ""
"**Source image on left side** - If *Split screen preview* is enabled, the "
"original clip is on the left side of the split screen. Uncheck this to have "
"the original on the right-hand side of the split screen."
msgstr ""
"**Izvorna slika na levi strani** - če je omogočen *Predogled deljenega "
"zaslona*, je izvirni posnetek na levi strani deljenega zaslona. Če ne "
"potrdite te možnosti, bo izvirnik na desni strani deljenega zaslona."

#: ../../effects_and_compositions/video_effects/color_image_correction/3_point_balance.rst:38
msgid ""
"**Black / Gray / White color** - Represent the Black, Grey and White levels "
"for the clip."
msgstr ""
"**Črna / siva / bela barva** - predstavlja ravni črne, sive in bele barve za "
"posnetek."
