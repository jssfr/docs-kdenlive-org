# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-31 00:39+0000\n"
"PO-Revision-Date: 2022-08-20 17:33+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using letterb0xed effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, transform, distort, perspective, letterb0xed"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:16
msgid "LetterB0xed"
msgstr "LetterB0xed"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:18
#, fuzzy
#| msgid "Adds Black Borders at top and bottom for Cinema Look."
msgid ""
"This filter/effect adds black borders at the top and bottom for this Cinema "
"look."
msgstr "Doda črne obrobe na vrhu in dnu za filmski videz."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:20
msgid "The effect has keyframes."
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:28
#, fuzzy
#| msgid "LetterB0xed"
msgid "LetterB0xed effect"
msgstr "LetterB0xed"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:30
msgid "**Transparency** - Makes the black borders transparent"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/letterb0xed.rst:32
msgid "**Border Width** - Set the border width in pixel"
msgstr ""

#~ msgid ""
#~ "This is the `Frei0r letterb0xed <https://www.mltframework.org/plugins/"
#~ "FilterFrei0r-letterb0xed/>`_ MLT filter."
#~ msgstr ""
#~ "To je filter MLT `Frei0r letterb0xed <https://www.mltframework.org/"
#~ "plugins/FilterFrei0r-letterb0xed/>`_."

#~ msgid "https://youtu.be/9Ldjt0QZPzs"
#~ msgstr "https://youtu.be/9Ldjt0QZPzs"

#~ msgid "https://youtu.be/JBp8wQW-_Qw"
#~ msgstr "https://youtu.be/JBp8wQW-_Qw"

#~ msgid "Contents"
#~ msgstr "Vsebina"
