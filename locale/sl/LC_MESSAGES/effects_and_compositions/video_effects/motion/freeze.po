# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-20 00:41+0000\n"
"PO-Revision-Date: 2023-07-21 13:24+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:1
msgid "Do your first steps with Kdenlive video editor, using freeze effect"
msgstr ""
"Opravite svoje prve korake z montažnim programom Kdenlive z uporabo učinka "
"zamrznitve slike"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, motion, freeze"
msgstr ""
"KDE, Kdenlive, montažni program, program za videomontažo, montaža "
"videoposnetkov, pomoč, učenje, enostavno, učinki, filter, video učinki, "
"gibanje, zamrzni, zamrznjena slika"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:21
msgid "Freeze"
msgstr "Zamrzni"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:23
msgid "This effect causes the video to freeze."
msgstr "Ta učinek zamrzne (zaustavi) video pri izbrani sličici."

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:25
msgid "The effect does not have keyframes."
msgstr "Ta učinek ne podpira ključnih sličic."

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:33
msgid "Freeze effect"
msgstr "Učinek Zamrzni"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:35
msgid ""
"**Freeze at** - Set the position via the slider or the time code (using the "
"format hh:mm:ss:ff)"
msgstr ""
"**Zamrzni pri** - nastavite položaj z drsnikom ali časovno kodo (v obliki uu:"
"mm:ss:sl)"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:37
msgid ""
"**Freeze Before** - If checked, freezes the video from the start of the clip "
"to the set :guilabel:`Freeze at` position. Default is **off**"
msgstr ""
"**Zamrzni pred** - če je potrjeno, zamrzne sliko oz. videoposnetek od "
"začetka posnetka do nastavljenega položaja :guilabel:`Zamrzni pri`. Privzeto "
"je **izklopljeno**"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:39
msgid ""
"**Freeze After** - If checked, freezes the video from set :guilabel:`Freeze "
"at` position to the end of the clip. Default is **off**"
msgstr ""
"**Zamrzni po** - če je potrjeno, se video zamrzne od nastavljenega položaja :"
"guilabel:`Zamrzni pri` do konca posnetka. Privzeto je **izklopljeno**"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:44
msgid ""
"By default, the clip will be frozen for its entire length when the effect is "
"added to the clip. To change this, check either the :guilabel:`Freeze "
"Before` or :guilabel:`Freeze After` option and move the :guilabel:`Freeze "
"at` slider to the time where you want the freeze to start or end. The audio "
"in the video plays for the entire length, i.e. the **Freeze** effect does "
"not affect the audio."
msgstr ""
"Privzeto bo posnetek zamrznjen v celotni dolžini, ko mu bo dodan učinek. Če "
"želite to spremeniti, potrdite možnost :guilabel:`Zamrzni pred` ali :"
"guilabel:`Zamrzni po` in premaknite drsnik :guilabel:`Zamrzni pri` na čas, "
"ko želite, da se zamrznitev začne ali konča. Zvok v videoposnetku se "
"predvaja v celotni dolžini, kar pomeni, da učinek **Zamrzni** ne vpliva na "
"zvok."
