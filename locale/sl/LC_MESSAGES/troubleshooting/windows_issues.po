# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2023-01-23 14:22+0100\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.2.2\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../troubleshooting/windows_issues.rst:1
msgid "The Kdenlive User Manual"
msgstr "Uporabniški priročnik Kdenlive"

#: ../../troubleshooting/windows_issues.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn, Windows workaround, problem solving"
msgstr ""
"KDE, Kdenlive, dokumentacija, uporabniški priročnik, montažni program, "
"program za montažo, video urejevalnik, odprta koda, brezplačno, prosto, "
"pomoč, učenje, Windows, rešitev, reševanje težav"

#: ../../troubleshooting/windows_issues.rst:19
msgid "Windows issues"
msgstr "Težave na sistemih Windows"

#: ../../troubleshooting/windows_issues.rst:22
msgid ""
"The current **Kdenlive** on Windows® (April 2022, version 21.12.3) has a few "
"issues that have workarounds. The purpose of this wiki page is to document "
"these issues and their workarounds."
msgstr ""
"Trenutni **Kdenlive** za operacijski sistem Windows® (april 2022, različica "
"21.12.3) ima nekaj težav, ki se jim je možno izogniti. Namen te wiki strani "
"je dokumentiranje teh vprašanj in rešitev zanje."

#: ../../troubleshooting/windows_issues.rst:27
msgid "Title tool, display real background not working in \"DirectX\" backend"
msgstr ""
"Orodje za napise, prikaz realnega ozadja ne deluje z zaledjem »DirectX«"

#: ../../troubleshooting/windows_issues.rst:29
#, fuzzy
msgid ""
"Something with the settings went wrong. Go to: :menuselection:`Help --> "
"Reset Configuration` and try again."
msgstr ""
"Nekaj z nastavitvami je šlo narobe. Odprite: :menuselection:'Help --> "
"Ponastavi konfiguracijo' in poskusite znova."

#: ../../troubleshooting/windows_issues.rst:33
msgid "Render problems"
msgstr "Težave z izrisovanjem"

#: ../../troubleshooting/windows_issues.rst:35
#, fuzzy
msgid ""
"After rendering you get de-synced audio or wrong effects or black frames at "
"end of the last clip: download version 20.08.1 or higher from the `download "
"page <https://kdenlive.org/en/download>`_. If you still experience problems "
"see :ref:`windows_issues`."
msgstr ""
"Po udobju boste na koncu zadnjega posnetka dobili de-sinhronizirane zvočne "
"ali napačne učinke ali črne okvirje: prenos različice 20.08.1 ali novejšo iz "
"\"strani za prenos <https://kdenlive.org/en/download>\"_. Če še vedno imate "
"težave glejte :ref:'windows_issues'."

#: ../../troubleshooting/windows_issues.rst:38
msgid "Scopes doesn't show anything"
msgstr "Merilniki ne kažejo ničesar"

#: ../../troubleshooting/windows_issues.rst:40
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"

#: ../../troubleshooting/windows_issues.rst:44
msgid "Audio crackling while playback"
msgstr "Zvočno pokanje med predvajanjem"

#: ../../troubleshooting/windows_issues.rst:45
#, fuzzy
msgid ""
":kbd:`CRL + SHIFT +,` (comma), select :menuselection:`Playback --> audio "
"backend` and play around with :menuselection:`WinMM` (Win7), :menuselection:"
"`Wasapi` (Win10), :menuselection:`DirectSound` to see what give the best "
"result. Restart Kdenlive after each switch."
msgstr ""
":kbd:'CRL + SHIFT +,' (vejca), izberite :menuselection:'Predvajanje --> "
"audio backend' in predvajanje naokoli z :menuselection:'WinMM' (Win7), :"
"menuselection:'Wasapi' (Win10), :menuselection:'DirectSound' da vidite, kaj "
"daje najboljši rezultat. Po vsakem stikalom znova zaženite Kdenlive."

#: ../../troubleshooting/windows_issues.rst:49
msgid ""
"This application failed to start because no Qt platform plugin could be "
"initialized"
msgstr ""
"Program se ni uspel zagnati, ker ni bilo mogoče inicializirati vtičnika "
"platforme Qt"

#: ../../troubleshooting/windows_issues.rst:51
#: ../../troubleshooting/windows_issues.rst:166
#, fuzzy
msgid ""
"Hit :kbd:`CTRL + SHIFT + ,` (comma) > :menuselection:`environment` > make "
"sure the paths point to the same path as \"MLT profiles folder\"."
msgstr ""
"Hit :kbd:'CTRL + SHIFT + ,' (vejico) > :menuselection:'environment' > "
"poskrbite, da bodo poti kazale na isto pot kot \"MAPA PROFILOV MLT\"."

#: ../../troubleshooting/windows_issues.rst:53
#, fuzzy
msgid ""
"Download: :download:`qt.conf </files/qt.conf>`. Put the file :file:`qt.conf` "
"into the \"bin\" folder (the folder where :file:`kdenlive.exe` is)"
msgstr ""
"Download: :d ownload:'qt.conf </files/qt.conf>'. Dajte datoteko :file:'qt."
"conf' v mapo \"bin\" (mapa, kjer :file:'kdenlive.exe' je)"

#: ../../troubleshooting/windows_issues.rst:57
msgid "First time use of Kdenlive"
msgstr "Prva uporaba programa Kdenlive"

#: ../../troubleshooting/windows_issues.rst:59
#, fuzzy
msgid ""
"This issue should be solved with Windows version 19.04.2-6. That :file:"
"`kdenliverc` is correct set up please start Kdenlive twice (start -> close -"
"> start). Then start your work."
msgstr ""
"To težavo je treba rešiti z operacijskim sistemom Windows 19.04.2-6. To :"
"file:'kdenliverc' je pravilno nastavljeno prosim začnite Kdenlive dvakrat "
"(start -> close -> start). Potem začnite z delom."

#: ../../troubleshooting/windows_issues.rst:63
msgid "Intel graphic card driver"
msgstr "Gonilnik grafične kartice Intel"

#: ../../troubleshooting/windows_issues.rst:65
msgid "Updated Intel graphic driver versions lead to a corrupted Kdenlive GUI."
msgstr ""
"Posodobljene različice grafičnega gonilnika Intel vodijo do poškodovanega "
"vmesnika Kdenlive."

#: ../../troubleshooting/windows_issues.rst:67
msgid ""
"**Solution 1:** Open Kdenlive. Move the mouse to the top. The menus are "
"showing up. Try to reach :menuselection:`Settings` -> :menuselection:`openGL "
"backend` -> enable :menuselection:`OpenGLES`/:menuselection:`DirectX`. "
"Restart Kdenlive. This should solve your Intel graphic driver issue."
msgstr ""
"**Rešitev 1:** Odprite Kdenlive. Premaknite miško na vrh. Meniji se "
"prikažejo. Poskusite doseči :menuselection:`Nastavitve` -> :menuselection:"
"`Zaledje openGL` -> omogočite :menuselection:`OpenGLES`/:menuselection:"
"`DirectX`. Znova zaženite Kdenlive. To bi moralo rešiti težavo z grafičnim "
"gonilnikom Intel."

#: ../../troubleshooting/windows_issues.rst:71
msgid ""
"Maybe this statement helps (forum user \"Windows User\"): I would like to "
"confirm that this issue seems to be mostly fixed. When I use the latest "
"daily build of Kdenlive on Windows 10 with the latest Intel graphics "
"drivers, I still get see a corrupted GUI after opening Kdenlive. The only "
"way to resolve this is to choose Settings > OpenGL Backend > OpenGLES from "
"the menu. I can't see the menu when the GUI is corrupt but I can click where "
"the menu should be. A quick test of Kdenlive after doing this seems fixed."
msgstr ""
"Morda ta izjava pomaga (uporabnik foruma »Windows User«): Rad bi potrdil, da "
"se zdi, da je ta težava večinoma odpravljena. Ko uporabljam najnovejšo "
"dnevno gradnjo Kdenlive na Windows 10 z najnovejšim grafičnimi gonilniki "
"Intel, še vedno vidim poškodovan up. vmesnik po odprtju Kdenlive. To lahko "
"rešite le tako, da v meniju izberete Nastavitve > Zaledje OpenGL > OpenGLES. "
"Menija ne vidim, ko je up. vmesnik poškodovan, lahko pa kliknem, kje bi "
"moral biti meni. Po hitrem preizkusu Kdenlive po tem se zdi težava s tem "
"rešena."

#: ../../troubleshooting/windows_issues.rst:74
msgid ""
"**Solution 2:** Press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key "
"simultaneously) and type **appdata**. Go to :file:`local` and within it "
"open :file:`kdenliverc` with an editor. Search for ``[misc]`` and delete "
"``[misc]`` and the following entry. . Restart Kdenlive."
msgstr ""
"**Rešitev 2:** Pritisnite :kbd:`Win + R` (tipki :kbd:`Windows` in :kbd:`R` "
"hkrati) in vnesite **appdata**. Pojdite na :file:`local` in znotraj tega "
"odprite :file:`kdenliverc` z urejevalnikom. Poiščite »[misc]« in izbrišite "
"»[misc]« in naslednji vnos. Znova zaženite Kdenlive."

#: ../../troubleshooting/windows_issues.rst:81
#, fuzzy
msgid ""
"Timeline: right-click menu close immediately after releasing mouse button"
msgstr ""
"Časovnica: z desno tipko miške kliknite meni zaprite takoj po izpustu miške"

#: ../../troubleshooting/windows_issues.rst:83
msgid "Don't use the style :menuselection:`Fusion`."
msgstr "Ne uporabljajte sloga :menuselection:`Fusion`."

#: ../../troubleshooting/windows_issues.rst:86
#, fuzzy
msgid ""
"Go to: :menuselection:`Settings` -> :menuselection:`Style` and choose :"
"menuselection:`Default` or :menuselection:`Windows`."
msgstr ""
"Pojdite na: :menuselection:'Settings' -> :menuselection:'Style' in izberite :"
"menuselection:'Default' ali :menuselection:'Windows'."

#: ../../troubleshooting/windows_issues.rst:91
msgid "Icons are missing"
msgstr "Ikone manjkajo"

#: ../../troubleshooting/windows_issues.rst:93
#, fuzzy
msgid ""
"Go to: :menuselection:`settings` -> untick :menuselection:`force breeze icon "
"theme`. Kdenlive restarts and you should see the icons."
msgstr ""
"Pojdite na: :menuselection:'settings' -> :menuselection:'force breeze icon "
"theme'. Kdenlive se ponovno zažene in morate videti ikone."

#: ../../troubleshooting/windows_issues.rst:97
#, fuzzy
msgid ""
"Cannot open projects made with previous version, timeline snaps back, cannot "
"import clip"
msgstr ""
"Ni mogoče odpreti projektov, narejenih s prejšnjo različico, časovne premike "
"nazaj, izrezka ni mogoče uvoziti"

#: ../../troubleshooting/windows_issues.rst:99
msgid "Go to: :menuselection:`Help --> Reset configuration`."
msgstr "Pojdite na: :menuselection:`Pomoč --> Ponastavi prilagoditev`."

#: ../../troubleshooting/windows_issues.rst:102
#, fuzzy
msgid ""
"If this is not solving the problem: Press :kbd:`Win + R` (:kbd:`Windows` key "
"and :kbd:`R` key simultaneously) and type **appdata**. Go to :file:`local` "
"and within it rename :file:`kdenliverc` to :file:`kdenliverc.old`. Start "
"Kdenlive -> do nothing -> close Kdenlive -> and restart Kdenlive again."
msgstr ""
"Če to ne rešuje težave: Pritisnite :kbd:'Win + R' (:kbd:'Windows' ključ in :"
"kbd:'R' ključ hkrati) in vnesite **appdata**. Pojdite na :file:'local' in "
"znotraj tega preimenujte :file:'kdenliverc' v :file:'kdenliverc.old'. "
"Začnite Kdenlive -> -> zaprite Kdenlive -> in znova zaženite Kdenlive."

#: ../../troubleshooting/windows_issues.rst:105
#, fuzzy
msgid ""
"If you have still problems delete proxy clips and other cached data by going "
"to :menuselection:`Project` menu > :menuselection:`Project Setting` > :"
"menuselection:`Cache Data` tab > there you can delete cached data."
msgstr ""
"Če imate še vedno težave, izbrišite izrezke proxyja in druge predpomnjene "
"podatke tako, da odprete :menuselection:'Project' meni > :"
"menuselection:'Project Setting' > :menuselection:'Cache Data' tab > there "
"can delete cached data."

#: ../../troubleshooting/windows_issues.rst:108
#, fuzzy
msgid "If you have still problems try :ref:`windows_issues`."
msgstr "Če imate še vedno težave poskusite :ref:'windows_issues'."

#: ../../troubleshooting/windows_issues.rst:112
#, fuzzy
msgid "Windows 10: timeline stuttering or Kdenlive hangs."
msgstr "Windows 10: jecljanje s časovnico ali Kdenlive visi."

#: ../../troubleshooting/windows_issues.rst:114
#, fuzzy
msgid ""
"Most probably you got a major Win10 update (i.e 1809). If so you have to "
"update all drivers for audio and video."
msgstr ""
"Najverjetneje ste dobili veliko Win10 posodobitev (to je 1809). Če je tako, "
"morate posodobiti vse gonilnike za zvok in video."

#: ../../troubleshooting/windows_issues.rst:116
#, fuzzy
msgid ""
"Intel driver can be updated with this updater: `Intel updater <https://"
"downloadcenter.intel.com/en/download/28425/Intel-Driver-Support-Assistant>`_."
msgstr ""
"Intelov gonilnik je mogoče posodobiti s tem posodobiteljem: 'Intel updater "
"<https://downloadcenter.intel.com/en/download/28425/Intel-Driver-Support-"
"Assistant>'_."

#: ../../troubleshooting/windows_issues.rst:120
msgid "\"Clip is invalid, will be removed\""
msgstr "»Posnetek ni veljaven, zato bo odstranjen iz projekta.«"

#: ../../troubleshooting/windows_issues.rst:122
#, fuzzy
msgid ""
"This bug can appear if you do a clean reinstall of **Kdenlive** (see above). "
"Simply close and open **Kdenlive** once, and it should be fixed."
msgstr ""
"Ta napaka se lahko pojavi, če naredite čisto ponovno namestitev **Kdenlive** "
"(glejte zgoraj). Preprosto zaprite in odprite **Kdenlive** enkrat, in ga je "
"treba popraviti."

#: ../../troubleshooting/windows_issues.rst:125
#, fuzzy
msgid ""
"Additionally this can be a problem either with the :file:`kdenliverc` file "
"(see here :ref:`windows_issues`) or you have some mismatch in the \"local\" "
"folder (see here :ref:`windows_issues`)."
msgstr ""
"Poleg tega je to lahko problem bodisi z :file:'kdenliverc' datoteko (glej "
"tukaj :ref:'windows_issues') ali pa imate nekaj neskladja v \"lokalni\" mapi "
"(glej tukaj :ref:'windows_issues')."

#: ../../troubleshooting/windows_issues.rst:129
#, fuzzy
msgid "Any critical bug"
msgstr "Vsak kritični hrošč"

#: ../../troubleshooting/windows_issues.rst:131
msgid "This describes the process of doing a clean install on Windows®."
msgstr "To opisuje postopek čiste namestitve na sistemih Windows®."

#: ../../troubleshooting/windows_issues.rst:134
msgid ""
"Firstly, delete your normal **Kdenlive** folder (containing the application)"
msgstr "Najprej izbrišite običajno mapo **Kdenlive** (ki vsebuje program)"

#: ../../troubleshooting/windows_issues.rst:137
#, fuzzy
msgid ""
"Access the **Appdata** folder (:kbd:`Win + R` and then type **APPDATA** in "
"full caps). Go to :file:`local` and search for folder :file:`kdenlive`."
msgstr ""
"Dostopajte do mape **Appdata** (:kbd:'Win + R' in nato vnesite **APPDATA** v "
"polne pokrovčke). Pojdite na :file:'local' in poiščite mapo :file:'kdenlive'."

#: ../../troubleshooting/windows_issues.rst:142
#, fuzzy
msgid ""
"If you have any saved effects or clips stored in your library, make a backup "
"of the library folder."
msgstr ""
"Če imate shranjene učinke ali izrezke shranjene v knjižnici, naredite "
"varnostno kopijo mape knjižnice."

#: ../../troubleshooting/windows_issues.rst:145
#, fuzzy
msgid ""
"Then once you have backup up your library folder, delete the :file:"
"`kdenlive` folder."
msgstr ""
"Ko imate varnostno kopijo mape knjižnice, izbrišite mapo :file:'kdenlive'."

#: ../../troubleshooting/windows_issues.rst:148
#, fuzzy
msgid ""
"Reinstall the latest version of **Kdenlive** from the `download page "
"<https://kdenlive.org/en/download>`_"
msgstr ""
"Znova namestite najnovejšo različico programa **Kdenlive** s strani za "
"prenos <https://kdenlive.org/en/download>'_"

#: ../../troubleshooting/windows_issues.rst:152
#, fuzzy
msgid "JPG files appear as white picture after rendering"
msgstr "JPG datoteke se prikažejo kot bela slika po ukinitvi"

#: ../../troubleshooting/windows_issues.rst:154
#, fuzzy
msgid ""
"This issue should be solved with Windows version 19.04.0. If not convert the "
"JPG to PNG and it renders correctly."
msgstr ""
"To težavo je treba rešiti z operacijskim sistemom Windows 19.04.0. Če ne "
"pretvorite JPG v PNG in pravilno renders."

#: ../../troubleshooting/windows_issues.rst:158
msgid "Play/Pause Issue"
msgstr "Težava s predvajanjem/premorom"

#: ../../troubleshooting/windows_issues.rst:160
#, fuzzy
msgid ""
"This issue is solved with Windows version 18.08.2 (30. Oct 2018). Get the "
"current version from the `download page <https://kdenlive.org/en/download>`_."
msgstr ""
"Ta težava je rešena s sistemom Windows različica 18.08.2 (30. okt 2018). "
"Prenesite trenutno različico s strani za prenos <https://kdenlive.org/en/"
"download>'_."

#: ../../troubleshooting/windows_issues.rst:164
msgid "Qt rendering crash"
msgstr "Sesutje izrisovanja Qt"

#: ../../troubleshooting/windows_issues.rst:169
#, fuzzy
msgid ""
"When switching from kdenlive for windows 17.12 > 18.04/18.08, a Qt rendering "
"crash appears. To make sure this doesn't happen, you need to edit the :file:"
"`kdenliverc` file in the :file:`appdata/local` folder. To access your "
"appdata, press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key "
"simultaneously) and type **appdata**. Go to :file:`local` and within it "
"rename :file:`kdenliverc` to :file:`kdenliverc.old`."
msgstr ""
"Ko preklopite iz Kdenlive za okna 17.12 > 18.04/18.08, se prikaže Qt "
"ugasnjenje crash. Če želite, da se to ne zgodi, morate urediti datoteko :"
"file:'kdenliverc' v mapi :file:'appdata/local'. Če želite dostopati do "
"appdata, pritisnite :kbd:'Win + R' (:kbd:'Windows' ključ in :kbd:'R' ključ "
"hkrati) in vnesite **appdata**. Pojdite na :file:'local' in znotraj tega "
"preimenujte :file:'kdenliverc' v :file:'kdenliverc.old'."

#: ../../troubleshooting/windows_issues.rst:173
msgid "Kdenlive cannot be deleted, running process on exit"
msgstr "Kdenlive ni mogoče izbrisati, proces teče ob izhodu"

#: ../../troubleshooting/windows_issues.rst:175
#, fuzzy
msgid ""
"This issue is solved with Windows version 18.12.1. Get the current version "
"from the `download page <https://kdenlive.org/en/download>`_."
msgstr ""
"Ta težava je rešena s sistemom Windows različica 18.12.1. Prenesite trenutno "
"različico s strani za prenos <https://kdenlive.org/en/download>'_."

#: ../../troubleshooting/windows_issues.rst:178
#, fuzzy
msgid ""
"If you want to reinstall **Kdenlive** or re-run **Kdenlive**, it may tell "
"you \"The file or folder is open in another program\". Windows® then won't "
"let you delete or re-run **Kdenlive**."
msgstr ""
"Če želite znova namestiti **Kdenlive** ali ponovno zagnati **Kdenlive**, vam "
"lahko pove \"Datoteka ali mapa je odprta v drugem programu\". Windows vam® "
"nato ne dovoli izbrisati ali ponovno zagnati **Kdenlive**."

#: ../../troubleshooting/windows_issues.rst:181
#, fuzzy
msgid ""
"To fix this you have to kill the running process: press and hold :kbd:`Ctrl "
"+ Shift + Esc` &  expand the task manager by clicking :menuselection:`all "
"details`. Then find :file:`kdenlive.exe` &  :file:`dbus-daemon.exe`, and "
"click :menuselection:`End task` for both of them."
msgstr ""
"Če želite to popraviti, morate ubiti tekoči postopek: pritisnite in držite :"
"kbd:'Ctrl + Shift + Esc' & razširite upravljalnik opravil s klikom :"
"menuselection:'all details'. Nato poiščite :file:'kdenlive.exe' & :"
"file:'dbus-daemon.exe', in kliknite :menuselection:'End task' za oba."

#: ../../troubleshooting/windows_issues.rst:184
#, fuzzy
msgid ""
"Or download the: :download:`Kdenlive-kill.zip </files/Kdenlive-kill.zip>`. "
"Unpack it and just double-click the batch file which kills all running "
"**Kdenlive** processes."
msgstr ""
"Ali prenesite: :d nastavite:'Kdenlive-kill.zip </files/Kdenlive-kill.zip>'. "
"Razpakijte in dvokliknite paketno datoteko, ki ubije vse tekoče **Kdenlive** "
"procese."

#: ../../troubleshooting/windows_issues.rst:188
#, fuzzy
#| msgid "Kdenlive cannot be uninstalled"
msgid "Kdenlive crash at start up, Kdenlive cannot be uninstalled"
msgstr "Kdenlive ni mogoče odstraniti"

#: ../../troubleshooting/windows_issues.rst:190
msgid ""
"If Kdenlive crash at startup or if the uninstaller doesn't work delete the "
"entire folder: :file:`C:/Program Files/kdenlive`."
msgstr ""
"Če se Kdenlive zruši ob zagonu ali če odstranjevalnik ne deluje, izbrišite "
"celotno mapo: :file:`C:/Program Files/kdenlive`."

#: ../../troubleshooting/windows_issues.rst:192
msgid "Re-install Kdenlive"
msgstr "Ponovno namestite Kdenlive"

#: ../../troubleshooting/windows_issues.rst:194
msgid "You have to manually delete in the start menu the Kdenlive folder."
msgstr "V začetnem meniju morate ročno izbrisati mapo Kdenlive."

#: ../../troubleshooting/windows_issues.rst:198
#, fuzzy
msgid "Kdenlive crash or green Monitor"
msgstr "Sesutje Kdenlive ali zeleni monitor"

#: ../../troubleshooting/windows_issues.rst:200
#, fuzzy
msgid ""
"Get all newest Windows® updates. Afterwards, update your graphic card driver "
"and your sound card driver and your printer driver. Some crashes could occur "
"of incompatibility of the graphics card and sound card with the newest "
"Windows®10 updates (18.09 update). After you have updated the drivers re-"
"start the computer and try again by starting :file:`kdenlive.exe`."
msgstr ""
"Prenesi vse najnovejše posodobitve sistema Windows®. Nato posodobite "
"gonilnik grafične kartice, gonilnik zvočne kartice in gonilnik tiskalnika. "
"Do nekaterih strmoglavitev lahko pride zaradi nezdružljivosti grafične "
"kartice in zvočne kartice z najnovejšimi posodobitvami sistema Windows®10 "
"(posodobitev 18.09). Ko posodobite gonilnike, znova zaženite računalnik in "
"poskusite znova z zagonom :file:'kdenlive.exe'."

#: ../../troubleshooting/windows_issues.rst:205
#, fuzzy
msgid ""
"If this is not solving the problem switch your standard printer to "
"“Microsoft XPS Document Writer” and try again to start Kdenlive."
msgstr ""
"Če to ne odpravlja težave, preklopite standardni tiskalnik na »Microsoft XPS "
"Document Writer« in poskusite znova zagnati Kdenlive."

#: ../../troubleshooting/windows_issues.rst:208
#, fuzzy
msgid ""
"Delete the :file:`kdenliverc` file as descript here under :ref:"
"`windows_issues`."
msgstr ""
"Izbrišite datoteko :file:'kdenliverc' kot opis tukaj pod :"
"ref:'windows_issues'."

#: ../../troubleshooting/windows_issues.rst:211
#, fuzzy
msgid ""
"Make sure you set processing thread to 1: :kbd:`Ctrl + Shift + ,` (comma) > :"
"menuselection:`Environment` > :menuselection:`Processing thread` > set to 1"
msgstr ""
"Prepričajte se, da nastavite nit obdelave na 1: :kbd:'Ctrl + Shift "
"+ ,' (vejca) > :menuselection:'Okolje' > :menuselection:'Obdelava nit' > "
"nastavljena na 1"

#: ../../troubleshooting/windows_issues.rst:215
msgid "General Issues"
msgstr "Splošne težave"

#: ../../troubleshooting/windows_issues.rst:217
#, fuzzy
msgid ""
"The current **Kdenlive** version (November 2018, version 18.08.3) has a few "
"issues that have workarounds."
msgstr ""
"Trenutna **Kdenlive** različica (november 2018, različica 18.08.3) ima nekaj "
"težav, ki imajo zaodložja."

#: ../../troubleshooting/windows_issues.rst:221
#, fuzzy
msgid "Audio Pops and Ticks in Render"
msgstr "Presket zvoka v izdelani videodatoteki"

#: ../../troubleshooting/windows_issues.rst:223
msgid "If this problem appears make sure the audio file is: 16-bit PCM WAV."
msgstr ""
"Če se pojavi ta težava, preverite, ali je zvočna datoteka v zapisu: 16-bitni "
"PCM WAV."

#~ msgid "Contents"
#~ msgstr "Vsebina"
