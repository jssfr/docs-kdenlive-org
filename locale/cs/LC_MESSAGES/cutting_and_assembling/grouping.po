# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2022-01-17 14:47+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../cutting_and_assembling/grouping.rst:16
msgid "Grouping"
msgstr "Seskupování"

#: ../../cutting_and_assembling/grouping.rst:19
msgid ""
"Grouping clips allows you to lock clips together so that you can move them "
"as a group and still retain their positions relative to each element in the "
"group."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:24
msgid "How to Group Clips"
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:26
msgid ""
"You can select multiple clips in preparation for grouping them by holding "
"shift and clicking the mouse and dragging in the timeline."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:34
msgid ""
"To group the selected clips select :menuselection:`Timeline --> Group Clips` "
"or right-click the selected clips and choose :menuselection:`Group Clips`."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:39
msgid "Cutting Grouped Clips"
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:41
msgid ""
"Grouping is also useful if you have separate audio and video tracks and need "
"to cut and splice both tracks at exactly the same point (e.g. for audio sync "
"reasons)."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:44
msgid ""
"If you cut the video clip using the :ref:`editing` when there is an audio "
"clip grouped to it, then **Kdenlive** cuts the audio clip at the same point "
"automatically."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:65
msgid "Removing Clip Grouping"
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:67
msgid ""
"To remove the grouping on clips, select the group of clips and choose :"
"menuselection:`Timeline --> Ungroup Clips`."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:71
msgid "FAQ"
msgstr "Často kladené dotazy"

#: ../../cutting_and_assembling/grouping.rst:73
msgid "Q: How to delete sound track only?"
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:75
msgid ""
"A: Right-click on the clip and choose :menuselection:`Split Audio`. The "
"audio will move to an audio track but be grouped with the video track."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:81
msgid "Right-click again and choose :menuselection:`Ungroup Clips`."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:83
msgid "Then you can delete just the audio track."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:85
msgid ""
"Alternatively you can keep the audio in the clip and use the :menuselection:"
"`Audio Correction --> Mute` effect to just mute the soundtrack on the clip."
msgstr ""

#: ../../cutting_and_assembling/grouping.rst:87
msgid ""
"Yet another method is to select :menuselection:`Video only` from the :ref:"
"`clip_menu`."
msgstr ""
