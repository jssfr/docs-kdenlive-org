# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-11 00:39+0000\n"
"PO-Revision-Date: 2022-04-06 12:02+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using white balance (LMS) "
"effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, white balance (LMS) "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:1
msgid ""
"- Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:Ttguy) - Mmaguire "
"(https://userbase.kde.org/User:Mmaguire) - Bernd Jordan (https://discuss.kde."
"org/u/berndmj) "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:1
msgid "Creative Commons License SA 4.0"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:17
msgid "White Balance (LMS)"
msgstr "Vyvážení bílé (LMS)"

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:19
msgid ""
"This effect/filter does simple color correction, in a physically meaningful "
"way. For more detailed about white balance see :ref:`tutorial-"
"white_balance_lms` in the :ref:`useful_information` section of the "
"documentation."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:21
msgid "The effect has keyframes."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:29
msgid "White Balance (LMS space) effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:31
msgid ""
"**Neutral Color** - Choose the color from the source image that should be "
"white"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:33
msgid ""
"**Color Temperature** - Choose an output color temperature, if different "
"from 6,500K"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/white_balance_lms.rst:38
msgid ""
"Color temperature is measured in degrees Kelvin. Lower values correct for "
"\"warmer\" lighting, higher values correct for \"cool\" lighting. the "
"default value of +6,500K is unity."
msgstr ""
