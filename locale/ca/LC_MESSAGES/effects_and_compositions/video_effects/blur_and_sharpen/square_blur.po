# Translation of docs_kdenlive_org_effects_and_compositions___video_effects___blur_and_sharpen___square_blur.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-05 00:51+0000\n"
"PO-Revision-Date: 2023-07-06 18:56+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using square blur effect"
msgstr ""
"Feu les primeres passes amb l'editor de vídeo Kdenlive, fent servir l'efecte "
"de difuminat quadrat"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, blur and sharpen, square blur  "
msgstr ""
"KDE, Kdenlive, editor de vídeo, ajuda, aprendre, fàcil, efectes, filtre, "
"efectes de vídeo, difumina i aguditza, difuminat quadrat  "

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:1
msgid ""
"- Claus Christensen - Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:"
"Ttguy) - Bushuev (https://userbase.kde.org/User:Bushuev) - Roger (https://"
"userbase.kde.org/User:Roger) - Bernd Jordan (https://discuss.kde.org/u/"
"berndmj) "
msgstr ""
"- Claus Christensen - Yuri Chornoivan - Ttguy (https://userbase.kde.org/User:"
"Ttguy) - Bushuev (https://userbase.kde.org/User:Bushuev) - Roger (https://"
"userbase.kde.org/User:Roger) - Bernd Jordan (https://discuss.kde.org/u/"
"berndmj) "

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Llicència Creative Commons SA 4.0"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:19
msgid "Square Blur"
msgstr "Difuminat quadrat"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:21
msgid ""
"This effect blurs the entire clip. **Kernel size** determines the blurriness."
msgstr ""
"Aquest efecte difumina tot el clip. **Mida del nucli** determina el "
"difuminat."

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:23
msgid "This effect has keyframes."
msgstr "Aquest efecte té fotogrames clau."

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/square_blur.rst:31
msgid "Squareblur effect"
msgstr "Efecte de difuminat quadrat"
