# Translation of docs_kdenlive_org_effects_and_compositions___video_effects___color_image_correction___gamma_keyframe.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-07 00:43+0000\n"
"PO-Revision-Date: 2023-07-07 20:21+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:1
msgid ""
"Do your first steps with Kdenlive video editor, using gamma effect with "
"keyframes"
msgstr ""
"Feu les primeres passes amb l'editor de vídeo Kdenlive, fent servir l'efecte "
"gamma amb fotogrames clau"

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, gamma with keyframes "
msgstr ""
"KDE, Kdenlive, editor de vídeo, ajuda, aprendre, fàcil, efectes, filtre, "
"efectes de vídeo, correcció del color i la imatge, gamma amb fotogrames clau "

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:1
msgid "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "
msgstr "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Llicència Creative Commons SA 4.0"

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:14
msgid "Gamma (keyframable)"
msgstr "Gamma (amb fotogrames clau)"

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:16
msgid "This effect/filter adjusts the :term:`gamma` value of a source image."
msgstr ""
"Aquest efecte/filtre ajusta el valor de la :term:`gamma` d'una imatge font."

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:18
msgid ""
"The effect has keyframes. Use :ref:`effects-gamma` if you do not need "
"keyframes."
msgstr ""
"L'efecte té fotogrames clau. Useu :ref:`effects-gamma` si no necessiteu "
"fotogrames clau."

#: ../../effects_and_compositions/video_effects/color_image_correction/gamma_keyframe.rst:26
msgid "Gamma (keyframable) effect"
msgstr "Efecte gamma (amb fotogrames clau)"
