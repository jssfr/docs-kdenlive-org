# Translation of docs_kdenlive_org_troubleshooting___installation_troubleshooting.po to Catalan
# Copyright (C) 2022-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-27 00:53+0000\n"
"PO-Revision-Date: 2023-02-27 09:17+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../troubleshooting/installation_troubleshooting.rst:10
msgid "Installation Troubleshooting"
msgstr "Solució de problemes a la instal·lació"

#: ../../troubleshooting/installation_troubleshooting.rst:12
msgid ""
"Many problems encountered in Kdenlive are caused by installation problems "
"(missing or mismatching packages). Kdenlive uses the MLT framework to "
"process all video operations, and MLT uses many other libraries like FFmpeg, "
"Frei0r, …"
msgstr ""
"Molts problemes que es troben en el Kdenlive són provocats per problemes "
"d'instal·lació (manquen paquets o no coincideixen). El Kdenlive usa el marc "
"de treball MLT per a processar totes les operacions de vídeo, i el MLT usa "
"moltes altres biblioteques com la FFmpeg, Frei0r,…"

#: ../../troubleshooting/installation_troubleshooting.rst:14
msgid ""
"Here are some tips to understand what might be wrong for you, depending on "
"the error message you get on startup. If this does not help you, check the "
"forums."
msgstr ""
"Aquí hi ha alguns consells per entendre el que podria estar malament per a "
"vós, depenent del missatge d'error que rebeu a l'inici. Si això no ajuda, "
"reviseu els fòrums."

#: ../../troubleshooting/installation_troubleshooting.rst:17
msgid "Your MLT installation cannot be found or Cannot start MLT backend"
msgstr ""
"No s'ha pogut trobar la vostra instal·lació del MLT o no s'ha pogut iniciar "
"el dorsal MLT"

#: ../../troubleshooting/installation_troubleshooting.rst:19
msgid ""
"There is obviously something wrong with your MLT installation. Either it is "
"not installed or not in a standard location. You can test your MLT "
"installation from a terminal, type: `melt color:red` This should bring up a "
"red window (press :kbd:`q` to close it)."
msgstr ""
"Evidentment, hi ha alguna cosa incorrecta en la instal·lació del MLT. O bé "
"no està instal·lat o no en una ubicació estàndard. Podeu provar la vostra "
"instal·lació del MLT des d'un terminal, teclegeu: `melt color:red`. Això "
"hauria de fer aparèixer una finestra vermella (premeu :kbd:`q` per tancar-"
"la)."

#: ../../troubleshooting/installation_troubleshooting.rst:22
msgid ""
"If you see an error message, try reinstalling MLT or check that you don't "
"have several versions installed on the system."
msgstr ""
"Si veieu un missatge d'error, intenteu reinstal·lar el MLT o comproveu que "
"no hi ha instal·lades diverses versions en el sistema."

# skip-rule: t-acc_obe
#: ../../troubleshooting/installation_troubleshooting.rst:24
msgid ""
"If you see the red window, check where your MLT is installed: `which melt`. "
"Then delete Kdenlive's config file (`$HOME/.config/kdenliverc`) and restart "
"Kdenlive."
msgstr ""
"Si veieu la finestra vermella, comproveu on està instal·lat el MLT: `which "
"melt`. Després suprimiu el fitxer de configuració del Kdenlive (`$HOME/."
"config/kdenliverc`) i reinicieu el Kdenlive."

#: ../../troubleshooting/installation_troubleshooting.rst:27
msgid "Missing package"
msgstr "Manca de paquets"

#: ../../troubleshooting/installation_troubleshooting.rst:29
msgid "A dependency is missing and it is recommended to install it."
msgstr "Manca una dependència i es recomana instal·lar-la."

#: ../../troubleshooting/installation_troubleshooting.rst:32
msgid "Frei0r"
msgstr "Frei0r"

#: ../../troubleshooting/installation_troubleshooting.rst:34
msgid ""
"This package provides many effects and transitions. Without it, Kdenlive's "
"features will be reduced. You can simply install frei0r-plugins from your "
"package manager."
msgstr ""
"Aquest paquet proporciona molts efectes i transicions. Sense ell, es "
"reduiran les característiques del Kdenlive. Simplement podeu instal·lar "
"«frei0r-plugins» des del gestor de paquets."

#: ../../troubleshooting/installation_troubleshooting.rst:37
msgid "Breeze icons"
msgstr "Icones Brisa"

#: ../../troubleshooting/installation_troubleshooting.rst:39
msgid ""
"Many icons used by Kdenlive come from the Breeze Icons package. Without it, "
"many parts of the UI will not appear correctly. You can simply install "
"breeze-icon-theme or breeze-icons from your package manager."
msgstr ""
"Moltes icones utilitzades pel Kdenlive provenen del paquet Breeze Icons. "
"Sense ell, moltes parts de la IU no apareixeran correctament. Simplement "
"podeu instal·lar breeze-icon-theme o breeze-icons del vostre gestor de "
"paquets."

#: ../../troubleshooting/installation_troubleshooting.rst:42
msgid "MediaInfo"
msgstr "MediaInfo"

# skip-rule: t-acc_obe
#: ../../troubleshooting/installation_troubleshooting.rst:44
msgid ""
"Download and install MediaInfo from `here <https://mediaarea.net/MediaInfo/"
"Download>`_"
msgstr ""
"Baixeu i instal·leu el MediaInfo des d'`aquí <https://mediaarea.net/"
"MediaInfo/Download>`_"

#: ../../troubleshooting/installation_troubleshooting.rst:47
msgid "Missing MLT module"
msgstr "Manca el mòdul MLT"

#: ../../troubleshooting/installation_troubleshooting.rst:49
msgid "An MLT dependency is missing and it is required to install it."
msgstr "Manca una dependència del MLT i cal instal·lar-la."

# skip-rule: t-acc_obe
#: ../../troubleshooting/installation_troubleshooting.rst:51
msgid ""
"**SDL** is used to output audio. Install `libsdl 1.x` from your package "
"manager."
msgstr ""
"**SDL** s'utilitza a la sortida d'àudio. Instal·leu `libsdl 1.x` des del "
"gestor de paquets."

#: ../../troubleshooting/installation_troubleshooting.rst:53
msgid ""
"**Avformat** is the FFmpeg module. Make sure you have ffmpeg installed on "
"your system."
msgstr ""
"**Avformat** és el mòdul FFmpeg. Assegureu-vos que teniu instal·lat el "
"«ffmpeg» al vostre sistema."

#: ../../troubleshooting/installation_troubleshooting.rst:56
msgid "The following codecs were not found on your system…"
msgstr "No s'han trobat els còdecs següents al vostre sistema…"

# skip-rule: t-acc_obe
#: ../../troubleshooting/installation_troubleshooting.rst:58
msgid ""
"Some audio / video codecs are not installed by default. Installing a package "
"called `libavcodec-extra` might solve the problem."
msgstr ""
"Alguns còdecs d'àudio/vídeo no estan instal·lats de manera predeterminada. "
"Instal·lar un paquet anomenat `libavcodec-extra` podria resoldre el problema."

# skip-rule: t-acc_obe
#: ../../troubleshooting/installation_troubleshooting.rst:60
msgid ""
"On openSuse, you need to add the `packman repository <https://www.opensuse-"
"community.org/>`_, then enable `replace vendor package <https://en.opensuse."
"org/SDB:Vendor_change_update#Full_repository_Vendor_change>`_ on the packman "
"repository."
msgstr ""
"A openSuse, cal afegir el `repositori packman <https://www.opensuse-"
"community.org/>`_, i després habilitar el `paquet de reemplaçament del "
"venedor <https://en.opensuse.org/SDB:"
"Vendorchangeupdate#FullrepositoryVendorchange>`_ al repositori «packman»."
