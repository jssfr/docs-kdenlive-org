# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2022.
# Paolo Zamponi <feus73@gmail.com>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2023-06-09 21:17+0200\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#: ../../effects_and_compositions/transitions/screen.rst:13
msgid "Transitions - Screen"
msgstr "Transizioni - Schermo"

#: ../../effects_and_compositions/transitions/screen.rst:17
msgid ""
"This is very much like :ref:`effects-chroma_key_basic` but it works with "
"footage filmed against a black background."
msgstr ""
"È molto simile a :ref:`effects-chroma_key_basic`, ma funziona con filmati "
"girati su uno sfondo nero."

#: ../../effects_and_compositions/transitions/screen.rst:19
msgid "Timeline showing how to apply the \"Screen\" transition."
msgstr "La Linea temporale che mostra come applicare la transizione «Schermo»."

#: ../../effects_and_compositions/transitions/screen.rst:25
msgid ""
"This video composites a video of fire filmed on a black background into "
"another bit of footage using the *Screen* transition."
msgstr ""
"Questo video combina un video di fuoco girato su uno sfondo nero in un altro "
"pezzo di filmato usando la transizione *Schermo*."

#: ../../effects_and_compositions/transitions/screen.rst:27
msgid "https://youtu.be/GkFdHcf9jbY"
msgstr "https://youtu.be/GkFdHcf9jbY"

#~ msgid "Contents"
#~ msgstr "Contenuti"
