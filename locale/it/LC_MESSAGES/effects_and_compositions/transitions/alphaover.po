# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2022-10-17 17:29+0200\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.2\n"

#: ../../effects_and_compositions/transitions/alphaover.rst:10
msgid "alphaover transition"
msgstr "transizione alphaover"

#: ../../effects_and_compositions/transitions/alphaover.rst:14
msgid ""
"This is the `Frei0r alphaover <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaover/>`_ MLT transition."
msgstr ""
"È la transizione MLT `Frei0r alphaover <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaover/>`_."

#: ../../effects_and_compositions/transitions/alphaover.rst:16
msgid "The alpha OVER operation."
msgstr "L'operazione alfa OVER."

#: ../../effects_and_compositions/transitions/alphaover.rst:18
msgid "Yellow clip has a triangle alpha shape with min = 0 and max =618."
msgstr "La clip gialla ha una forma alfa triangolare con min = 0 e max =618."

#: ../../effects_and_compositions/transitions/alphaover.rst:20
msgid "Green clip has rectangle alpha shape with min=0 and max =1000."
msgstr "La clip verde ha una forma alfa rettangolare con min=0 e max=1000."

#: ../../effects_and_compositions/transitions/alphaover.rst:22
msgid "alphaover is the transition in between."
msgstr "alphaover è la transizione nel mezzo."

#~ msgid "Contents"
#~ msgstr "Contenuto"
