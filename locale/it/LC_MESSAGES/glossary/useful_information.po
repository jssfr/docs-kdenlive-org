# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <feus73@gmail.com>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2023-01-02 16:22+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.0\n"

#: ../../glossary/useful_information.rst:1
msgid "Editing in Kdenlive video editor"
msgstr "Montaggio nell'editor video Kdenlive"

#: ../../glossary/useful_information.rst:1
msgid ""
"KDE, Kdenlive, useful information, editing, timeline, documentation, user "
"manual, video editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, informazioni utili, montaggio, linea temporale, "
"documentazione, Manuale utente, editor video, open source, libero, impara, "
"facile"

#: ../../glossary/useful_information.rst:22
msgid "Useful Information"
msgstr "Informazioni utili"

#~ msgid "Contents:"
#~ msgstr "Contenuto:"

#~ msgid ""
#~ "`HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum.kde."
#~ "org/viewtopic.php?f=272&t=124869#p329129>`_"
#~ msgstr ""
#~ "`Come produrre video 4k e 2K compatibili con YouTube <https://forum.kde."
#~ "org/viewtopic.php?f=272&t=124869#p329129>`_"
