# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-26 00:39+0000\n"
"PO-Revision-Date: 2023-06-11 01:42+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clip pixelizada ref edit mode\n"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:14
msgid "Obscure"
msgstr "Obscuro"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:16
#, fuzzy
#| msgid ""
#| "Hide a region of the clip by pixelizing it (obscure). The effect's "
#| "pixelization cannot be adjusted and may therefore not work satisfactorily "
#| "with all source material. In that case try the :ref:`Pixelate <effects-"
#| "pixelate>` effect."
msgid ""
"Hide a region of the clip by pixelizing it (obscure). The effect's "
"pixelization cannot be adjusted and may therefore not work satisfactorily "
"with all source material. Compare the :ref:`effects-pixelize` effect."
msgstr ""
"Esconde uma região do 'clip', tornando-a pixelizada (obscura). A pixelização "
"do efeito não pode ser ajustada e poderá não funcionar de forma satisfatória "
"com todo o material original. Nesse caso, experimente o efeito de :ref:"
"`Pixelização <effects-pixelate>`."

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:26
msgid "Obscure effect panel and results (default settings)"
msgstr "Painel do efeito Obscuro e os resultados (configuração predefinida)"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:28
#, fuzzy
#| msgid ""
#| "Use the handles on the red rectangle [1]_ in the Project Monitor to "
#| "change the size: use the center handle to move the rectangle around."
msgid ""
"Use the handles on the red rectangle\\ [1]_ in the Project Monitor to change "
"the size: use the center handle to move the rectangle around."
msgstr ""
"Use as pegas no rectângulo vermelho [1]_ no Monitor do Projecto para mudar o "
"tamanho: use a pega do centro para mover o rectângulo ao redor."

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:33
msgid "**Notes**"
msgstr "**Notas**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/obscure.rst:35
msgid ""
"If you do not see a red rectangle in the Project Monitor, enable Edit Mode "
"by clicking on the |edit-mode| icon in the Project Monitor toolbar"
msgstr ""
"Se não vir um rectângulo vermelho no Monitor do Projecto, active o Modo de "
"Edição, carregando no ícone do |edit-mode| na barra do Monitor do Projecto"
