# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-09 00:40+0000\n"
"PO-Revision-Date: 2023-06-10 14:53+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref quickstart clips thumbs titles kdenlivetitle\n"
"X-POFile-SpellExtra: Kdenlive effects\n"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:23
msgid "File Structure"
msgstr "Estrutura de Ficheiros"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:25
msgid ""
"As already pointed out in the :ref:`quickstart`, we suggest using a "
"different project folder for each project. **Kdenlive** will generate the "
"following folders for caching in the project folder:"
msgstr ""
"Como já foi referido no :ref:`quickstart`, sugerimos que use uma pasta de "
"projecto diferente para cada projecto. O **Kdenlive** irá gerar as seguintes "
"pastas para fazer 'cache' da pasta do projecto:"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:28
msgid ":file:`/proxy/` for the :ref:`clips` that have been generated"
msgstr ":file:`/proxy/` para os :ref:`clips` que foram gerados"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:30
msgid ":file:`/thumbs/` for thumbnails to all used clips"
msgstr ":file:`/thumbs/` para as miniaturas de todos os 'clips' usados"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:32
msgid ""
":file:`/titles/` default location for the :ref:`effects-titles` saved "
"outside the project file"
msgstr ""
":file:`/titles/` a localização por omissão do :ref:`effects-titles`, gravada "
"fora do ficheiro do projecto"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:34
msgid ""
":file:`/.backup/` for your project's automatic :ref:`backup` These "
"directories can be deleted if not required anymore (for example for saving "
"space on the harddrive). **Kdenlive** will create them again when you load "
"the project the next time."
msgstr ""
":file:`/.backup/` para :ref:`backup` automática do seu projecto. Estas "
"pastas podem ser apagadas se não forem mais necessárias (por exemplo, para "
"poupar espaço no disco rígido). O **Kdenlive** irá criá-las de novo quando "
"carregar o projecto da próxima vez."

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:39
msgid ""
"The :file:`/titles/` directory is the default directory for saved :file:`."
"kdenlivetitle` title files. Make sure that you did not save any important "
"files in there before deleting it."
msgstr ""
"A pasta :file:`/titles/` é a pasta por omissão para os ficheiros de títulos :"
"file:`.kdenlivetitle` guardados. Certifique-se que não gravou nenhuns "
"ficheiros importantes aí, antes de a remover."

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:41
msgid ""
"Source clips can be located anywhere. Still, here are some thoughts about "
"their location:"
msgstr ""
"Os 'clips' de origem podem-se encontrar em qualquer lugar. De qualquer "
"forma, ainda existem algumas considerações sobre a sua localização:"

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:44
msgid ""
"Material (images, clips, audio) that is used for one project only can be put "
"into a subdirectory of the project folder as well. This keeps all important "
"files together, and searching for the files takes less time."
msgstr ""
"O material (imagens, 'clips', áudio) que é usado apenas para um projecto "
"pode também ser colocado numa sub-pasta da pasta do projecto. Isto mantém "
"todos os ficheiros importantes em conjunto, e assim a pesquisa pelos itens "
"demora menos tempo."

#: ../../importing_and_assets_management/projects_and_files/file_structure.rst:47
msgid ""
"Material that is used by multiple projects is convenient when kept together. "
"I’ve got a video collection the same way that I have a photo collection."
msgstr ""
"O material que é usado por vários projectos é conveniente tê-lo junto. Tenho "
"uma colecção de vídeos da mesma forma que tenho uma colecção de fotografias."
