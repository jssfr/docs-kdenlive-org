# Lithuanian translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-07 00:43+0000\n"
"PO-Revision-Date: 2021-11-18 00:21+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:1
msgid "Do your first steps with Kdenlive video editor, using hue shift effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, hue shift "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:1
msgid "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:1
msgid "Creative Commons License SA 4.0"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:14
msgid "Hue Shift"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:16
msgid "This effect/filter shifts the :term:`hue` of a source image."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:18
msgid "The effect has keyframes."
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:26
msgid "Hue Shift effect"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/hue_shift.rst:28
msgid "**Hue** - Sets the hue of the entire image"
msgstr ""
