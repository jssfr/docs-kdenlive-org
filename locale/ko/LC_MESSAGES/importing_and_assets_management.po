# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-26 00:42+0000\n"
"PO-Revision-Date: 2022-05-08 16:55+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../importing_and_assets_management.rst:1
msgid "Load files and start video editing with Kdenlive"
msgstr "Kdenlive에 파일을 불러오고 동영상 편집 시작하기"

#: ../../importing_and_assets_management.rst:1
msgid ""
"KDE, Kdenlive, file loading, start video editing, first steps, "
"documentation, user manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../importing_and_assets_management.rst:15
msgid "Importing and assets management"
msgstr ""

#~ msgid "Contents:"
#~ msgstr "목차:"
