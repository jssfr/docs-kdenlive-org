# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-17 00:21+0000\n"
"PO-Revision-Date: 2022-05-08 00:50+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../glossary/useful_information/default_transition_duration.rst:13
msgid "Configuring the Default Transition Duration"
msgstr "기본 전환 시간 설정"

#: ../../glossary/useful_information/default_transition_duration.rst:17
msgid ""
"You can now configure the **default duration for all newly created "
"transitions**. Before this, all new transitions were always 65 frames long – "
"and this translated to varying default durations, depending on a project’s "
"frame rate. This new configuration option should appeal all those users who "
"work a lot with transitions."
msgstr ""

#: ../../glossary/useful_information/default_transition_duration.rst:25
msgid ""
"Go to :menuselection:`Settings --> Configure Kdenlive --> Misc` category. "
"Under the heading Default Durations you’ll now find the new fifth option to "
"configure the default duration for newly created transitions. Enter a "
"duration in the usual format hh:mm:ss:ff."
msgstr ""

#: ../../glossary/useful_information/default_transition_duration.rst:27
msgid ""
"Please note that the frames (:ff) field will be interpreted on the basis of "
"the current project’s framerate. In contrast, the other fields hh:mm:ss are "
"independent of the framerate."
msgstr ""
