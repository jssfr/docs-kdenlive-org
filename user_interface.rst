.. meta::
   :description: Introduction to Kdenlive's window system and widgets
   :keywords: KDE, Kdenlive, user interface, documentation, user manual, video editor, open source, free, learn, easy, timeline, toolbar

.. metadata-placeholder

   :authors: - Eugen Mohr
             - Maris Stalte (https://userbase.kde.org/User:limerick)
             
   :license: Creative Commons License SA 4.0

.. _user_interface:

##############
User interface
##############

After starting Kdenlive the Kdenlive window should look something similar to the image below; as Kdenlive’s user interface is consistent across all platforms.

Kdenlive’s interface is separated into four main parts:

The :ref:`menubar` contains the :ref:`Menu` and :ref:`workspace_layouts` at the very top.

:ref:`Toolbars` at the top and above the timeline

:ref:`Window <view_menu>` in the middle.

:ref:`status_bar` at the bottom.

.. figure:: /images/interface_window_system_editing_screen.png
   :width: 650px
   :alt: interface_window-system_editing-screen

   Kdenlive’s default Screen Layout (example editing view). Topbar (blue), Toolbars (yellow), Window (green) and Status Bar (red).

This page introduces the Kdenlive user interface (UI), explaining where to find each group of features, and how the highly focused and tightly integrated Media, Edit, Color, Fairlight, and Deliver pages work together to let you pursue nearly any post-production workflow you can imagine.


.. toctree::
   :hidden:
   :glob:

   user_interface/*